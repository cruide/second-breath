# Second-Breath PHP5 framework.

**This is a small and fast PHP 5.3+ framework**

[Second-Breath](http://second-breath.alex-tisch.ru/) is an elegant, open source, based on namespace, and object oriented HMVC framework built using PHP 5.3 or later, by a Tishchenko Alexander. It aims to be swift, secure, and small.

Released under a [AS IS](http://second-breath.alex-tisch.ru/LICENSE.md), Second-Breath PHP5 framework can be used legally for any open source, commercial, or personal project.

Initiator: Tishchenko Alexander

Date of inception: 08/12/2014


