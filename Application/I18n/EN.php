<?php return array (
    '#LANGUAGE' => 'English',
    '#CODE'     => 'EN',

    'yes'             => 'yes',
    'no'              => 'no',
    'not'             => 'not',
    'exit'            => 'exit',
    'cancel'          => 'cancel',
    'enter'           => 'enter',
    'login'           => 'login',
    'password'        => 'password',
    'uthorization'    => 'authorization',
    'first_page'      => 'first',
    'last_page'       => 'last',
    'next_page'       => 'next',
    'previous_page'   => 'previous',

    'name'            => 'name',
    'you'             => 'you',
    'your'            => 'your',
    'your_name'       => 'your name',
    'message'         => 'message',
    'messages'        => 'messages',
    'send'            => 'send',
    'feedback'        => 'feedback',
    
    'root'            => 'root',
    'administrator'   => 'administrator',
    'administrators'  => 'administrators',
    'moderator'       => 'moderator',
    'moderators'      => 'moderators',
    'advanced_user'   => 'advanced user',
    'advanced_users'  => 'advanced users',
    'user'            => 'user',
    'users'           => 'users',
    'noob'            => 'noob',
    'noobs'           => 'noobs',
    
	'sw_framework'    => 'This is a small and fast PHP 5.3.x framework',
    
    'mail_send_error' => 'Error while sending the message.<br />Try again in a few minutes.',
    'auth_error'      => 'authorization error',
    'access_denied'   => 'access denied',
    'forbidden'       => 'forbidden',
    
    'welcome'         => 'welcome'
);