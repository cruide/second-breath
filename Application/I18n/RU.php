<?php return array (
    '#LANGUAGE' => 'Русский',
    '#CODE'     => 'RU',

    'yes'             => 'да',
    'no'              => 'нет',
    'not'             => 'не',
    'exit'            => 'выход',
    'cancel'          => 'отменить',
    'enter'           => 'войти',
    'login'           => 'логин',
    'password'        => 'пароль',
    'uthorization'    => 'авторизация',
    'first_page'      => 'первая',
    'last_page'       => 'последняя',
    'next_page'       => 'следующая',
    'previous_page'   => 'предыдущая',

    'name'            => 'имя',
    'you'             => 'вы',
    'your'            => 'ваш',
    'your_name'       => 'ваше имя',
    'message'         => 'сообщение',
    'messages'        => 'сообщения',
    'send'            => 'отправить',
    'feedback'        => 'обратная связь',
    
    'root'            => 'root',
    'administrator'   => 'администратор',
    'administrators'  => 'администраторы',
    'moderator'       => 'модератор',
    'moderators'      => 'модераторы',
    'advanced_user'   => 'продвинутый пользователь',
    'advanced_users'  => 'продвинутые пользователи',
    'user'            => 'пользователь',
    'users'           => 'пользователи',
    'noob'            => 'новичек',
    'noobs'           => 'новички',
    
	'sw_framework'    => 'Это маленький и быстрый PHP 5.3.x framework',
    
    'mail_send_error' => 'Ошибка отправки сообщения.<br />Попробуйте повторить через несколько минут.',
    'auth_error'      => 'ошибка авторизации',
    'access_denied'   => 'доступ запрещен',
    'forbidden'       => 'запрещено',
    
    'welcome'         => 'добро пожаловать'
);