<?php namespace Application\Plugins\Captcha\Library;
/**
* @author        Alexander Tishchenko
* @copyright     Copyright (c) 2013 Tishchenko A.
* @package       Second-Breath PHP5 framework
* @filesource    Captcha.php
* @name          Application\Plugins\Captcha\Library\Captcha
*/

// -------------------------------------------------------------------------------------
  final class Captcha extends \SB\Abstracts\Library 
  {
      public $width = 80, $height = 28, $size = 12;
      public $font;
      protected static $_instance;
// -------------------------------------------------------------------------------------
      public function __construct()
      {
		  $this->set_font( __DIR__ . DIR_SEP . 'Ttf' . DIR_SEP . 'lsansd.ttf' );
      }
// -------------------------------------------------------------------------------------
      public static function Instance()
      {
          if( null === self::$_instance ) {
              self::$_instance = new self();
          }
 
          return self::$_instance;
      }
// -------------------------------------------------------------------------------------
      public function set_font($font_file)
      {
		  if( is_file($font_file) ) {
			  $this->font = $font_file;
		  }
      }
// -------------------------------------------------------------------------------------
	  private function makeimg($number) 
      {
          if( empty($number) or !is_numeric($number) ) {
              $number = rand(10000, 99999);
          }
        
          $image = imagecreate($this->width, $this->height);
		  $bg    = imagecolorallocate($image, 255, 255, 255);
		  $fg    = imagecolorallocate($image, 0, 0, 0);
        
          $colors = array (
              imagecolorallocate($image, 255, 0, 0),
              imagecolorallocate($image, 0, 0, 255),
              imagecolorallocate($image, 110, 110, 110),
              imagecolorallocate($image, 0, 0, 110),
              imagecolorallocate($image, 110, 0, 0),
              imagecolorallocate($image, 0, 110, 0),
              imagecolorallocate($image, 0, 0, 0),
              imagecolorallocate($image, 110, 110, 0),
              imagecolorallocate($image, 0, 110, 110),
              imagecolorallocate($image, 110, 0, 110),
              imagecolorallocate($image, 180, 180, 0),
              imagecolorallocate($image, 170, 170, 170),
          );
        
		  imagecolortransparent($image, $bg);
		  imageinterlace($image, 1);
        
          $y_rnd_max = ($this->height / 100) * 35;
          $x_rnd_max = ($this->width / 100) * 35;

          $y_rnd_min = $this->height / 35;
          $x_rnd_min = $this->width / 28;
          
          
          $x   = rand( (int)$x_rnd_min, (int)$x_rnd_max ); 
          $y   = rand( (int)$y_rnd_min, (int)$y_rnd_max);
          $str = str_split($number);

          foreach($str as $key=>$val) {
          	  if( !empty($this->font) ) {
				  imagettftext($image, $this->size, 0, ($x <= 0) ? 2 : $x, $y + 11, $colors[ rand(0, count($colors)-1) ], $this->font, $val);
          	  } else {
				  imagestring($image, 5, $x, $y, $val, $colors[ rand(0, count($colors)-1) ]);
          	  }
              
              $x += 10;
          }
        
		  imagearc($image, rand(0, $this->width ), rand(0, $this->height ), rand($this->width / 2, $this->width), rand($this->height / 2, $this->height), 0, 360, imagecolorallocate($image, rand(0, 255), rand(0, 255), rand(0, 255)));
		  imagearc($image, rand(0, $this->width ), rand(0, $this->height ), rand($this->width / 2, $this->width), rand($this->height / 2, $this->height), 0, 360, imagecolorallocate($image, rand(0, 255), rand(0, 255), rand(0, 255)));
		  $dots = $this->width * $this->height / 10;
		  for ($i=0; $i < $dots; $i++) {
              imagesetpixel($image, rand(0, $this->width), rand(0, $this->height), imagecolorallocate($image, rand(0, 255), rand(0, 255), rand(0, 255)));
		  }
        
		  imagejpeg($image, null, 60);
		  imagedestroy($image);
	  }
// -------------------------------------------------------------------------------------
      public function show()
      {
          $number = rand(10000, 99999);

          \SB\Session::Instance()->set('Captcha', $number);

          if( !headers_sent() ) {
              header('Content-type: image/jpeg');
              header('Last-Modified: ' . gmdate( 'D, d M Y H:i:s', time() - 60 ) . ' GMT');
              header('Cache-Control: no-cache, no-store, max-age=0, must-revalidate');
              header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
              header('Pragma: no-cache');
              
              $this->makeimg( $number );
          }
      }
  }  

// -------------------------------------------------------------------------------------
/*  End of captcha.php  */