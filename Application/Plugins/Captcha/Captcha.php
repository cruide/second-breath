<?php namespace Application\Plugins\Captcha {
/**
* @author        Alexander Tishchenko
* @copyright     Copyright (c) 2013 Tishchenko A.
* @package       Second-Breath PHP5 framework
* @filesource    Router.php
* @name          Application\Plugins\Captcha\Captcha
*/
 
// -------------------------------------------------------------------------------------
  class Captcha extends \SB\Abstracts\Plugin 
  {
// -------------------------------------------------------------------------------------
      public function _before()
      {
          $show = $this->Input->get('show');
          if( $show == 1 ) {
              \Application\Plugins\Captcha\Library\Captcha::Instance()->show();
              exit();
          }
      }

      public function content()
      {
          return $this->Ui->fetch('captcha');
      }

  }  
}