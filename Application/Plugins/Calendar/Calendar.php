<?php namespace Application\Plugins\Calendar {
/**
* @author        Alexander Tishchenko
* @copyright     Copyright (c) 2013 Liks-Card-Service
* @package       Second-Breath PHP5 framework
* @filesource    Router.php
* @name          Application\Plugins\Calendar\Calendar
*/

// -------------------------------------------------------------------------------------
  class Calendar extends \SB\Abstracts\Plugin 
  {
      private $holidays;
      
      public function _before()
      {
          if( is_file(PLUGINS_DIR . DIR_SEP . $this->Name . DIR_SEP . 'Library' . DIR_SEP . 'holidays.php') ) {
              $this->holidays = include(PLUGINS_DIR . DIR_SEP . $this->Name . DIR_SEP . 'Library' . DIR_SEP . 'holidays.php');
          }
      }
      
      public function content()
      {
          $month = (isset($this->Params['m']) && is_numeric($this->Params['m'])) ? (int)$this->Params['m'] : null;
          $year  = (isset($this->Params['y']) && is_numeric($this->Params['y'])) ? (int)$this->Params['y'] : null;
          
          $this->Ui->assign('calendar', $this->_make_calendar($month, $year));
          
          return $this->Ui->fetch('calendar');
      }
      
      private function _make_calendar($month = null, $year = null)
      {
          if( $month == null || !is_numeric($month) ) {
              $month = date('m');
          } else {
              $month = intval($month);
          }
          
          if( $year == null || !is_numeric($year) ) {
              $year = date('Y');
          } else {
              $year = intval($year);
          }
          
          $datetime   = strtotime("{$year}-{$month}-1 0:00:00");
          $dayofmonth = date('t', $datetime);
          $day_count  = 1;
          $num        = 0;
          $_          = '';
          $holidays   = (array_count($this->holidays) > 0) ? true : false;

          for($i = 0; $i < 7; $i++) {
              $dayofweek = date('w', mktime(0, 0, 0, date('m', $datetime), $day_count, date('Y', $datetime)));
              $dayofweek = $dayofweek - 1;

              if( $dayofweek == -1 ) {
                  $dayofweek = 6;
              }

              if( $dayofweek == $i ) {
                  $week[ $num ][ $i ] = $day_count;
                  $day_count++;
              } else {
                  $week[ $num ][ $i ] = '';
              }
          }

          while( true ) {
              $num++;

              for($i = 0; $i < 7; $i++) {
                  $week[ $num ][ $i ] = $day_count;
                  $day_count++;

                  if( $day_count > $dayofmonth ) break;
              }

              if( $day_count > $dayofmonth ) break;
          }

          $_ = '<table class="sb-calendar">';

          for($i = 0; $i < count($week); $i++) {
              $_ .= '<tr>';

              for($j = 0; $j < 7; $j++) {
                  if( !empty($week[ $i ][ $j ]) ) {
                      $day = $week[ $i ][ $j ];

                      if($j == 5 || $j == 6) {
                          $_ .= '<td class="sb-calendar-day-off';
                          
                          if( $holidays == true && !empty($this->holidays[$month][$day]) ) {
                              $title = implode(', ', $this->holidays[$month][$day]);
                              $_    .= " sb-calendar-holiday\" title=\"{$title}\">{$day}</td>";
                          } else {
                              $_ .= '">' . $day . '</td>';
                          }
                      } else {
                          $_     .= '<td';
                          
                          if( $holidays == true && !empty($this->holidays[$month][$day]) ) {
                              $title = implode(', ', $this->holidays[$month][$day]);
                              $_    .= " class\"sb-calendar-holiday\" title=\"{$title}\">{$day}</td>";
                          } else {
                              $_ .= '>' . $day . '</td>';
                          }
                      }
                  } else {
                      $_ .= '<td>&nbsp;</td>';
                  }
              }

              $_ .= '</tr>';
          } 

          return $_ . '</table>';
      }

  }  
}