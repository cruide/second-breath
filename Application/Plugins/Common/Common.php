<?php namespace Application\Plugins\Common {
/**
* @author        Alexander Tishchenko
* @copyright     Copyright (c) 2013 Alexander Tishchenko
* @package       Second-Breath PHP5 framework
* @filesource    Common.php
* @name          Application\Plugins\Common\Common
*/

// -------------------------------------------------------------------------------------
  class Common extends \SB\Abstracts\Plugin 
  {
      public function headers()
      {
          header('Content-type: application/javascript; charset=utf-8');
      }
// -------------------------------------------------------------------------------------
      public function content()
      {
          $tmp = array();

          foreach(\SB\Cloud::Instance()->modules as $key=>$val) {
              $tmp[] = sb_strtolower($val) . ": { vars: {}, fn: {} }";
          }

          $modules = implode(', ', $tmp);
          $tmp     = array();

          foreach(\SB\Cloud::Instance()->plugins as $key=>$val) {
              $tmp[] = sb_strtolower($val) . ": { vars: {}, fn: {} }";
          }

          $plugins = implode(', ', $tmp);
          
          $this->Ui->assign('modules_lst', $modules);
          $this->Ui->assign('plugins_lst', $plugins);
          
          if( $this->direct_request ) {
              return js_minify( $this->Ui->fetch('common') );
          }
          
          return '';
      }
  }  
}