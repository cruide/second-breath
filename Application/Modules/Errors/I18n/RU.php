<?php return array (
    '#LANGUAGE' => 'Русский',
    '#CODE'     => 'RU',

    'access_denied'         => 'У Вас недостаточно прав для просмотра данной страницы',
    'access_denied_title'   => '403, Доступ заперщён',

    'unknown_request'       => 'По Вашему запросу ничего не найдено',
    'unknown_request_title' => '404, Неизвестный запрос',
);