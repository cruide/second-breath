<?php return array (
    '#LANGUAGE' => 'English',
    '#CODE'     => 'EN',

    'access_denied'         => 'You are not authorized to view this link',
    'access_denied_title'   => '403, access denied',

    'unknown_request'       => 'In your search returned no results. Try to set the correct address',
    'unknown_request_title' => '404, unknown request',
);