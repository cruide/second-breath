<?php namespace Application\Modules\Errors\Controllers {

  class Index extends \SB\Abstracts\Controller
  {
      public function _before()
      {
          if( is_ajax() ) {
              $this->Layout->disable();
          }
      }
      
      public function default_Action()
      {
          return $this->show404_Action();
      }
      
	  public function show404_Action()
	  {
  		  set_http_status(404);
		  return $this->Ui->fetch('error_404');
	  }
      
      public function show500_Action()
      {
          set_http_status(500);
          return $this->Ui->fetch('error_500');
      }

      public function forbidden_Action()
      {
          set_http_status(403);
          return $this->Ui->fetch('forbidden');
      }

      public function notallowed_Action()
      {
          set_http_status(405);
          return $this->Ui->fetch('notallowed');
      }

      public function unauthorized_Action()
      {
          set_http_status(401);
          return $this->Ui->fetch('unauthorized');
      }
  }

}