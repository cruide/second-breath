<?php namespace Application\Modules\Index\Controllers {    
/**
* @author       Tishchenko Alexander
* @copyright    Copyright (c) 2014 All rights to Tishchenko A.
* @package      Second-Breath PHP5 framework
* @name         Index controller of default module
* @filesource   /Application/Modules/Index/Controllers/Index.php
*/

use SB\Theme;

  class Index extends \SB\Abstracts\Controller
  {
      protected $cache;
// ------------------------------------------------------------------------------
      /**
      * Этот метод будет запущен перед выполнением
      * любого экшена
      */
      public function _before()
      {
          $this->cache = \SB\Cache::Instance();
          $this->Layout->use_theme_css('forms.css');

      }
// ------------------------------------------------------------------------------
      /**
      * Метод-экшен по умолчанию
      * 
      */
  	  public function default_Action()
  	  {
/*
          if( !empty($this->cache) && $this->cache->enabled() ) {
              if( isset($this->cache->indexPage) ) {
                  return str_base64_decrypt( $this->cache->indexPage );
              }
              
              $page = $this->Ui->fetch('index');
              $this->cache->set('indexPage', str_base64_encrypt($page));
              
              return $page;
          }
*/          
          return $this->Ui->fetch('index');
	  }
  }
}