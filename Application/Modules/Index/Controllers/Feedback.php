<?php namespace Application\Modules\Index\Controllers {
/**
* @author       Tishchenko Alexander
* @copyright    Copyright (c) 2014 All rights to Tishchenko A.
* @package      Second-Breath PHP5 framework
* @name         Feedback controller of Index module
* @filesource   /Application/Modules/Index/Controllers/Feedback.php
*/

    class Feedback extends \SB\Abstracts\Controller 
    {
        public function _before()
        {
            $this->Layout->assign('page_title', sb_ucfirst($this->I18n->feedback));
        }
        
        public function default_Action()
        {
            $confirm = $this->Input->post('confirm');
            $auth    = \SB\Authorization::Instance()->is_auth();

            if( $auth ) {
                $form = new \Application\Modules\Index\Forms\Messager();    
            } else {
                $form = new \Application\Modules\Index\Forms\Feedback();
            }

            if( is_sb_ajax() && empty($confirm) ) {
                return $form->fetch();
            }
            
            if( $confirm == 'ok' && $form->check_all() ) {
                $data     = $form->get_values_as_array();
                $cfg      = load_ini('phpmailer');
                $title    = $cfg->from_name . ' | Обратная связь';
                $sendmail = new \SB\Phpmail();

                $sendmail->assign('message', $data['message'])
                         ->assign('title', $title);

                $sendmail->AddAddress( ((isset($cfg->email_from)) ? $cfg->email_from : 'info@' . SELF_DOMAIN) );
                $sendmail->SetFrom( $data['email'], ((!empty($data['name'])) ? $data['name'] : '') );
                $sendmail->Subject( $title );

                if( $sendmail->Send('feedback_mail') ) {
                    exit('success');
                }
                
                $form->assign('send_error', true);
            }
            
            return $form->fetch();
        }
    }
}

