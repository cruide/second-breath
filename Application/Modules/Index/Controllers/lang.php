<?php namespace Application\Modules\Index\Controllers {
/**
* @author       Tishchenko Alexander
* @copyright    Copyright (c) 2014 All rights to Tishchenko A.
* @package      Second-Breath PHP5 framework
* @name         Set language controller of default module
* @filesource   /Application/Modules/Index/Controllers/Lang.php
*/

    class Lang extends \SB\Abstracts\Controller
    {
	    public function default_Action()
	    {
		    $id = strtoupper( $this->Input->get('id') );

		    if( !empty($id) && strlen($id) == 2 && $this->I18n->func_is_locale_exists($id) ) {
			    $this->I18n->func_set_locale($id);
		    }

		    redirect();
	    }
    }
}