<?php namespace Application\Modules\Index\Forms {

    class Messager extends \SB\Abstracts\Form
    {
        public function _init()
        {
            $this->set_action('/index/feedback')
                 ->set_field('message', 'plaintext', 1, 255, true);
        }
    }
}