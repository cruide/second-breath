<?php namespace Application\Modules\Index\Forms {

    class Feedback extends \SB\Abstracts\Form
    {
        public function _init()
        {
            $this->set_action('/index/feedback')
                 ->set_field('name', 'name', 2, 100, true)
                 ->set_field('email', 'email', 6, 255, true)
                 ->set_field('captcha', 'integer', 5, 6, true)
                 ->set_field('message', 'plaintext', 1, 255, true);
        }
        
        public function check_all()
        {
            if( !parent::check_all() ) {
                return false;
            }
            
            if( !empty($this->Session) ) {
                $_ = $this->Session->get('Captcha');
            } else {
                $_ = $this->Cookie->Captcha;
            }

            if( empty($_) || !is_numeric($_) ) {
                $_ = -1;
            }

            if( $_ != $this->captcha->value ) {
                $this->captcha->message = 'Invalid code from the picture';
                $this->captcha->value   = '';
                return false;
            }
              
            return true;
        }
    }
}