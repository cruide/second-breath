<?php namespace Application\Modules\Acp\Library;

class Bootstrap extends \SB\Abstracts\Bootstrap
{
	public function run()
	{
        $auth = \SB\Authorization::Instance();
        if( !$auth->is_auth() ) {
            redirect('auth/index');
        }
        
        $user = $auth->get_auth_user();
        if( $user['group_id'] > 1 ) {
            redirect();
        }
        
		\SB\Theme::Instance()->set_theme('Acp');
	}
}