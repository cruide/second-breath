<?php namespace Application\Modules\Acp\Controllers;

  class Uploads extends \SB\Abstracts\Controller
  {
      public function _before()
      {
          $this->Layout->add_css(INTERFACE_COMMON_URL . '/Js/plupload/jquery.ui.plupload/css/jquery.ui.plupload.css');
      }
      
	  public function default_Action()
	  {
		  return $this->Ui->fetch('uploads.default');
	  }
      
      public function upload_Action()
      {
          $this->Layout->disable();
          
          $uploadfile = $this->Input->files('file');
          $images_dir = CONTENT_DIR . DIR_SEP . 'Images';
          $images_url = CONTENT_URL . '/Images';
          $upload_dir = dechex( (int)date('Ym') ) . DIR_SEP .
                        dechex( (int)date('d') );
          
          if( !is_dir($images_dir . DIR_SEP . $upload_dir) ) {
              try {
                  mkdir($images_dir . DIR_SEP . $upload_dir, 0755, true);
              } catch( \Exception $e ) {
                  system_exception($e);
              }
          }
          
          if( array_count($uploadfile) > 0 && is_image($uploadfile['tmp_name']) ) {
              $extension = get_image_extension($uploadfile['tmp_name']);
              $filename  = generate_file_name(".{$extension}");
              $filedest  = $images_dir . DIR_SEP . $upload_dir . DIR_SEP . $filename;
//              if( function_exists('DebugBreak') ) DebugBreak();
              if( move_uploaded_file($uploadfile['tmp_name'], $filedest) ) {
//                  $img = new \SB\Base\tObject('tab_cms_uploads');
//                  $img->timestamp = time();
//                  $img->filename  = $filename;
//                  $img->ipaddr    = get_ip_address();
//                  $img->save();
                  
                  return 'success';
              }
          }
          
          set_http_status(400);
          
          return 'upload fail';
      }
  }

