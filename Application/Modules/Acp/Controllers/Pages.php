<?php namespace Application\Modules\Acp\Controllers;

  class Pages extends \SB\Abstracts\Controller
  {
	  public function default_Action()
	  {
          $pages_model = new \Application\Modules\Acp\Models\Pages();
          $pages       = $pages_model->get_pages();
          
          if( array_count($pages) > 0 ) {
              return $this->Ui
                          ->assign('pages', $pages)
                          ->fetch('pages.default');
          }
          
		  return $this->Ui->fetch('pages.default');
	  }
// -------------------------------------------------------------------------------------
      public function new_Action()
      {
          return $this->Ui->fetch('pages.new');
      }
// -------------------------------------------------------------------------------------
      public function edit_Action()
      {
          $id = $this->Input->get('p');
          
          if( \SB\Match::is_integer($id) ) {
              $page = new \SB\Base\tObject('tab_cms_pages', array('id' => $id));

              if( !empty($page->created) ) {
                  $this->Ui->assign('page_content', $page->content);
              } else {
                  $this->Ui->assign('page_error', $this->I18n->Modules->Acp->page_not_found);
              }
          } else {
              $this->Ui->assign('page_error', $this->I18n->Modules->Acp->page_not_found);
          }
          
          return $this->Ui->fetch('pages.edit');          
      }
// -------------------------------------------------------------------------------------
      public function getthemecss_Action()
      {
          $config_theme = $this->Config->interface['theme'];
          $css_dir      = INTERFACE_DIR . DIR_SEP . $config_theme . DIR_SEP . 'Css';
          $css          = get_files_list( $css_dir, array('css') );
          $_            = '';
          
          
          if( array_count($css) > 0 ) {
              foreach($css as $key=>$val) {
                  $_ .= file_get_contents( $css_dir . DIR_SEP . $key );
              }
          }
          
          $this->Layout
               ->disable()
               ->set_header('Content-type: text/css; charset=utf-8');
          
          return $_;
      }
// -------------------------------------------------------------------------------------
      public function getimages_Action()
      {
          $images_dir = CONTENT_DIR . DIR_SEP . 'Images';
          $images_url = CONTENT_URL . '/Images';
          
          if( !is_dir($images_dir) ) {
              try {
                  mkdir($images_dir, 0755);
              } catch( \Exception $e ) {
                  system_exception($e);
              }
          }
          
          $this->Layout->disable();
          $this->Layout->set_header(
              'Content-type: application/javascript; charset=utf-8'
          );
          
          $images = get_files_list($images_dir, array('jpg','jpeg','gif','png','bmp'));

          if( array_count($images) > 0 ) {
              $images_bank = array();
              
              foreach($images as $key=>$val) {
                  $images_bank[] = "    [\"{$key}\", \"{$images_url}/{$key}\"]";
              }
              
              return "var tinyMCEImageList = new Array (\n" . implode(",\n", $images_bank) . ');';
          }
          
          return 'var tinyMCEImageList = new Array ();';
      }
  }

