<?php namespace Application\Modules\Acp\Models {

    class Pages extends \SB\Abstracts\Model
    {
        private $limit;
        
// -------------------------------------------------------------------------------------
        public function set_limit( $limit )
        {
            if( is_numeric($limit) ) {
                $this->limit = (int)$limit;
            }
            
            return $this;
        }
// -------------------------------------------------------------------------------------
        public function get_pages()
        {
            return $this->db->select('tab_cms_pages', array(
                'limit' => $this->limit,
            ));
        }
    }

}