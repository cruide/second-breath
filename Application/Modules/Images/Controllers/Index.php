<?php namespace Application\Modules\Images\Controllers {
/**
* @author       Tishchenko Alexander
* @copyright    Copyright (c) 2014 All rights to Tishchenko A.
* @package      Second-Breath PHP5 framework
* @name         Set language controller of default module
* @filesource   /Application/Modules/Index/Controllers/Lang.php
*/

    class Index extends \SB\Abstracts\Controller
    {
	    public function default_Action()
	    {
            $get  = $this->Input->get();
            $path = CONTENT_DIR . DIR_SEP . 'Images' . DIR_SEP . 
                    str_replace('-', DIR_SEP, $get['path']) . 
                    DIR_SEP . $get['file'];
            
            if( is_file($path) && ($image = getimagesize($path)) != false ) {
                header('Content-type: ' . $image['mime']);
                header('Last-Modified: ' . gmdate( 'D, d M Y H:i:s', time() - 60 ) . ' GMT');
                header('Cache-Control: no-cache, no-store, max-age=0, must-revalidate');
                header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
                header('Pragma: no-cache');
                
                exit( file_get_contents($path) );
            }
            
            show_error_404();
	    }
    }
}