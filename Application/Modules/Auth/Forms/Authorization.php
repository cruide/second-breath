<?php namespace Application\Modules\Auth\Forms {

	class Authorization extends \SB\Abstracts\Form
	{

		protected function _init()
		{
			$this->set_action('/auth/index/login')
			     ->set_field('email', 'email', 6, 255, true)
			     ->set_field('password', 'alphanum', 6, 16, true);

		}
	}
}