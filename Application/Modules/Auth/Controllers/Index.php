<?php namespace Application\Modules\Auth\Controllers {

    class Index extends \SB\Abstracts\Controller
    {
        private $auth;
// ------------------------------------------------------------------------------
        /**
        * Этот метод будет запущен перед выполнением
        * любого экшена
        */
        public function _before()
        {
            $this->auth = \SB\Authorization::Instance();
            $this->Layout->use_theme_css('forms.css');
        }
// ------------------------------------------------------------------------------
        public function default_Action()
        {
            if( $this->auth->is_auth() ) {
                redirect();
            }
            
            \SB\Native::assign_global('current_uri', 'login');
            
            $form = new \Application\Modules\Auth\Forms\Authorization();
            
            return $this->Ui
                        ->assign('authorization_form', $form->fetch())
                        ->fetch('auth.default.phtml');
        }
// ------------------------------------------------------------------------------
        public function login_Action()
        {
            $confirm = $this->Input->post('confirm');
            $form    = new \Application\Modules\Auth\Forms\Authorization();

            if( !is_sb_ajax() || empty($confirm) || $confirm != 'ok' ) {
                return $form->fetch();
            }
            
            if( !$form->check_all() ) {
                return $form->fetch();
            }
            
            $input = $form->get_values_as_array();
            $user  = \SB\Users::Instance()->get_by_email( $input['email'] );
            
            if( empty($user['email']) ) {
                return $form->assign('auth_error', true)->fetch();
            }

            $pwd = mkpass($input['password'], $user['salt']);
       
            if( $user['password'] == mkpass($input['password'], $user['salt']) 
             && \SB\Authorization::Instance()->signin($user['email']) == true ) 
            {
                redirect();
            }
            
            return $form->assign('auth_error', true)->fetch();
        }
// ------------------------------------------------------------------------------
        public function logout_Action()
        {
            if( !$this->auth->is_auth() ) {
                redirect();
            }
            
            $this->auth->signout();
            
            redirect();
        }
// ------------------------------------------------------------------------------
        public function makeroot_Action()
        {
            $root = new \SB\Base\tObject('tab_core_users', array(
                'email' => 'root@sb.local'
            ));
            
            if( empty($root->id) ) {
                $salt  = salt_generation();
                $today = time();
                
                $root->email     = 'root@sb.cms';
                $root->password  = mkpass('1234567890', $salt);
                $root->salt      = $salt;
                $root->group_id  = 0;
                $root->registred = $today;
                $root->activated = $today;
                $root->hash      = md5( "{$root->email}|{$root->password}|{$salt}" );
                $root->save();

                $profile = new \SB\Base\tObject('tab_core_users_profiles');
                $profile->id          = $root->id;
                $profile->nicname     = 'Superuser';
                $profile->birthday    = date('Y-m-d');
                $profile->gender      = 'male';
                $profile->avatar      = INTERFACE_URL . '/Common/Images/root.png';
                $profile->save();
            }
        }
    }

}