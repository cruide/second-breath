<?php
/**
* @author		Alexander Tishchenko
* @copyright	Copyright (c) 2013 Tishchenko A.
* @package		Second-Breath PHP5 framework
* @filesource	Index.php
*/
  
  /** 
  * Set default timezone to Europe/Moscow
  * ini_set may be denied by the administrator
  * there were precedents
  */
  @ini_set('date.timezone', 'Europe/Moscow');

  /* Check PHP version */
  if( floatval(PHP_VERSION) < 5.4 ) { 
      trigger_error('Version PHP requires 5.4 or more', E_USER_ERROR);
  }  

  /* Add include path */
  set_include_path( __DIR__ . ';' . get_include_path() );
  
  define('DOCUMENT_ROOT', $_SERVER['DOCUMENT_ROOT']);
  define('ROOT'         , __DIR__);
  define('SELF_PATH'    , pathinfo(__FILE__, PATHINFO_BASENAME));
  define('PROTOCOL'     , 'http' . ( (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') ? 's' : '') );
  define('SELF_DOMAIN'  , $_SERVER['HTTP_HOST']);
  define('BASE_URL'     , PROTOCOL . '://' . SELF_DOMAIN);

  require_once('Core/Bootstrap.php');
