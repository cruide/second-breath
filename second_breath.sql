-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия сервера:               5.6.17 - MySQL Community Server (GPL)
-- ОС Сервера:                   Win32
-- HeidiSQL Версия:              9.1.0.4867
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Дамп структуры для таблица second_breath.sb_acp_menu
DROP TABLE IF EXISTS `sb_acp_menu`;
CREATE TABLE IF NOT EXISTS `sb_acp_menu` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `parent_id` bigint(20) NOT NULL DEFAULT '0',
  `uri` varchar(100) NOT NULL DEFAULT '',
  `title` varchar(50) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  `sortorder` smallint(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `parent_id` (`parent_id`),
  KEY `sortorder` (`sortorder`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Дамп данных таблицы second_breath.sb_acp_menu: 0 rows
DELETE FROM `sb_acp_menu`;
/*!40000 ALTER TABLE `sb_acp_menu` DISABLE KEYS */;
/*!40000 ALTER TABLE `sb_acp_menu` ENABLE KEYS */;


-- Дамп структуры для таблица second_breath.sb_cms_menu
DROP TABLE IF EXISTS `sb_cms_menu`;
CREATE TABLE IF NOT EXISTS `sb_cms_menu` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `parent_id` bigint(20) NOT NULL DEFAULT '0',
  `uri` varchar(100) NOT NULL DEFAULT '',
  `title` varchar(50) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  `auth_need` tinyint(4) NOT NULL DEFAULT '0',
  `auth_show` tinyint(4) NOT NULL DEFAULT '1',
  `sortorder` smallint(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `sortorder` (`sortorder`),
  KEY `parent_id` (`parent_id`),
  KEY `auth_need` (`auth_need`),
  KEY `auth_show` (`auth_show`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Дамп данных таблицы second_breath.sb_cms_menu: 0 rows
DELETE FROM `sb_cms_menu`;
/*!40000 ALTER TABLE `sb_cms_menu` DISABLE KEYS */;
/*!40000 ALTER TABLE `sb_cms_menu` ENABLE KEYS */;


-- Дамп структуры для таблица second_breath.sb_cms_pages
DROP TABLE IF EXISTS `sb_cms_pages`;
CREATE TABLE IF NOT EXISTS `sb_cms_pages` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `alias` varchar(80) NOT NULL DEFAULT '0',
  `created` bigint(20) NOT NULL DEFAULT '0',
  `updated` bigint(20) NOT NULL DEFAULT '0',
  `approve` tinyint(4) NOT NULL DEFAULT '0',
  `default` tinyint(4) NOT NULL DEFAULT '0',
  `title` varchar(150) NOT NULL DEFAULT '',
  `keywords` varchar(255) NOT NULL DEFAULT '',
  `content` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `alias` (`alias`),
  KEY `created` (`created`),
  KEY `updated` (`updated`),
  KEY `approve` (`approve`),
  KEY `default` (`default`),
  FULLTEXT KEY `title` (`title`),
  FULLTEXT KEY `keywords` (`keywords`),
  FULLTEXT KEY `content` (`content`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы second_breath.sb_cms_pages: 1 rows
DELETE FROM `sb_cms_pages`;
/*!40000 ALTER TABLE `sb_cms_pages` DISABLE KEYS */;
INSERT INTO `sb_cms_pages` (`id`, `alias`, `created`, `updated`, `approve`, `default`, `title`, `keywords`, `content`) VALUES
	(1, 'index', 1415161651, 0, 1, 1, 'Главная страница', 'главная, индекс', '    <h2 class="header">&lt;?=\'Second-Breath PHP5 framework\'?&gt;</h2>\r\n    <p> \r\n        Second-Breath это:\r\n        <ul>\r\n            <li><a>сравнительно небольшой,</a></li>\r\n            <li><a>достаточно быстрый,</a></li>\r\n            <li><a>с открытым кодом,</a></li>\r\n            <li><a>объектно-ориентированный,</a></li>\r\n            <li><a>использующий пространство имен,</a></li>\r\n        </ul>\r\n        \r\n        HMVC framework для использования на PHP 5.3 или выше.<br />\r\n        Распространяется на условиях "Как есть".\r\n    </p>\r\n    <p>\r\n        Second-Breath PHP5 framework может быть использован для:\r\n        <ul>\r\n            <li><a>бесплатных веб-приложений;</a></li>\r\n            <li><a>коммерческих сайтов;</a></li>\r\n            <li><a>персональных проектов;</a></li>\r\n        </ul>\r\n    </p>\r\n    <p>&nbsp;</p>\r\n    <p>\r\n        <div style="margin: 0 auto; width: 310px;">\r\n            <script type="text/javascript">(function() {\r\n              if (window.pluso)if (typeof window.pluso.start == "function") return;\r\n              if (window.ifpluso==undefined) { window.ifpluso = 1;\r\n                var d = document, s = d.createElement(\'script\'), g = \'getElementsByTagName\';\r\n                s.type = \'text/javascript\'; s.charset=\'UTF-8\'; s.async = true;\r\n                s.src = (\'https:\' == window.location.protocol ? \'https\' : \'http\')  + \'://share.pluso.ru/pluso-like.js\';\r\n                var h=d[g](\'body\')[0];\r\n                h.appendChild(s);\r\n              }})();</script>\r\n            <div class="pluso" data-background="transparent" data-options="medium,square,line,horizontal,counter,theme=02" data-services="vkontakte,odnoklassniki,facebook,twitter,google,email,print" data-url="http://second-breath.alex-tisch.ru/" data-description="HMVC framework для использования на PHP 5.3 или выше. Распространяется на условиях &quot;Как есть&quot;." data-title="Second-Breath PHP5 framework"></div>    \r\n        </div>\r\n    </p>\r\n    <p>&nbsp;</p>\r\n    \r\n    <h2 class="header">&lt;?=\'История создания\'?&gt;</h2>\r\n    <p>\r\n     &nbsp;&nbsp;&nbsp;Всё началось с маленького безобидного эксперимента по созданию простенького микро-фреймворка для небольших проектов используя PHP 5.3+... что-то вроде \r\n     <a href="http://www.slimframework.com/" target="_blank">Slim PHP micro framework</a> или <a href="http://flightphp.com/" target="_blank">Flight micro-framework for PHP</a>.\r\n     В итоге через месяц родилась идея создания небольшого полноценного <a href="http://ru.wikipedia.org/wiki/HMVC" target="_blank">HMVC</a> мини-фреймворка для быстрого написания небольших и средних проектов.\r\n     Ещё через месяц родился самодостаточный фреймворк для проектов любой сложности.\r\n     Ввиду того, что это уже мой 2й фреймворк, этот был назван "Второе дыханье".\r\n    </p>\r\n    <p>\r\n     &nbsp;&nbsp;&nbsp;В итоге я получил не плохой PHP фреймворк с ядром, которое может располагаться где угодно на сервере и использоваться сразу несколькими проектами:<br />\r\n     <i>- "Одно ядро и несколько приложений";</i><br />\r\n     &nbsp;&nbsp;&nbsp;Знаю многие скажут, что это далеко не лучшая идея и подобный подход не допустим для серьёзных веб-студий... Мол если появится ошибка/уязвимость в ядре, то это повлияет на все проекты...<br />\r\n     &nbsp;&nbsp;&nbsp;Я приведу другой пример... Вы используете один фреймворк и пихаете его в каждый свой проект (как я часто это видел в веб-приложениях на Kohana или CodeIgniter), но вот незадача... вдруг, совершенно случайно, \r\n     обнаружилась критическая ошибка... и вы начинаете переливать файлик/файлики во все... скажем 30 сайтов клиентов...<br />\r\n     &nbsp;&nbsp;&nbsp;Я решил подобную проблему по своему. Одно ядро, несколько сайтов клиентов на одном сервере. На мой взгляд это практичней и удобней... и где то безопасней.\r\n    </p>\r\n\r\n    <h2 class="header">&lt;?=\'Структура\'?&gt;</h2>\r\n    <p>\r\n        &nbsp;&nbsp;&nbsp;Я постарался придержаться, на мой взгляд удобной и практичной <a href="http://ru.wikipedia.org/wiki/HMVC" target="_blank">HMVC</a> структуры, т.к. я и мои знакомые привыкли к подобному подходу представления приложений. \r\n        К тому же структура <a href="http://ru.wikipedia.org/wiki/HMVC" target="_blank">HMVC</a> делает веб-приложения достаточно гибкими и масштабируемыми.<br />\r\n        &nbsp;&nbsp;&nbsp;Что касается внутренней структуры фреймворка, то тут я пошел по самому простому пути... "Неймспейсы повторяют структуру директорий".<br /><br />\r\n        То есть:<br />\r\n        <i style="color: teal;">Namespace Application\\Modules\\Index\\Controllers</i><br />\r\n        Равнозначно пути:<br />\r\n        <i style="color: teal;">DOCUMENT_ROOT . \'/Application/Modules/Index/Controllers\'</i><br /><br />\r\n        А класс:<br />\r\n        <i style="color: teal;">$myClass = new \\Application\\Library\\<u>myClass</u>();</i><br />\r\n        Будет расположен в файле:<br />\r\n        <i style="color: teal;">DOCUMENT_ROOT . \'/Application/Library/<u>myClass</u>.php\'</i><br />\r\n        С именем класса внутри:<br />\r\n        <i style="color: teal;">\r\n        &lt;?php<br /> \r\n        namespace Application\\Library;<br />\r\n        <br />\r\n        class <u>myClass</u><br />\r\n        {<br /> \r\n        <br />\r\n        }<br /><br />\r\n        </i>\r\n        Вы всегда знаете где находится класс и подгружать предварительно ничего не надо... этим занимается "автолоадер" и как следствие "Загружаем только то, что используем".\r\n    </p>\r\n    <p>\r\n        Общение посетителя с вашим сайтом происходит с помощью REST запросов...<br />То есть:<br />\r\n        <i style="color: teal; font-size: 8pt;">http://mysite.com/модуль/контроллер/метод/id/103/page/2.html</i><br /><br />\r\n        Если мы просто обращаемся к сайту http://mysite.com без указания модуля, то будут выполнены "дефолтовые" значения:<br />\r\n        <i style="color: teal; font-size: 8pt;">http://mysite.com/ = http://mysite.com/index/index/default</i><br /><br />\r\n        И по аналогии если не указать контроллер или метод.\r\n    </p>\r\n    <p>\r\n        Всё это делает мой фпеймворк достаточно читаемым для стороннего программиста. Любой, кто будет смотреть и править код, будет понимать, что и где находится.\r\n    </p>\r\n    \r\n    <h2 class="header">&lt;?=\'Форматирование кода\'?&gt;</h2>\r\n    <p>\r\n        &nbsp;&nbsp;&nbsp;Весь код отформатирован по стандарту PSR-2. Естественно у меня есть привычки, которые так въелись, \r\n        что искоренить их уже практически не возможно. Ввиду этого факта в коде имеются некоторые уходы от стандарта PSR-2... \r\n        за что и прошу простить меня :)\r\n    </p>\r\n        \r\n    <h2 class="header">&lt;?=\'Разработка\'?&gt;</h2>\r\n    <p>\r\n        &nbsp;&nbsp;&nbsp;В данный момент ядро находится на стадии "Бета" и продолжает дорабатываться. Я готов поделится им для заинтересованных лиц.<br /><br />\r\n        Сам проект находится на <a href="https://bitbucket.org/cruide/second-breath" target="_blank">Bitbucket.org</a><br />\r\n        GIT ссылка: https://bitbucket.org/cruide/second-breath.git<br /><br />\r\n        Фреймворк распространяется "как есть".\r\n    </p>\r\n\r\n    <h2 class="header">&lt;?=\'Документация\'?&gt;</h2>\r\n    <p>\r\n        &nbsp;&nbsp;&nbsp;Как таковой документации нет, но я работаю над этим. Так же стараюсь делать описание функций, методов и классов. Конечно то, что есть трудно назвать документированным кодом... но это пока всё, что есть в данный момент.<br />\r\n        &nbsp;&nbsp;&nbsp;Если же моё "произведение" будет востребованным, то я займусь созданием более подробной документации.\r\n    </p>\r\n    <p>&nbsp;</p>\r\n\r\n    <h2 class="header">&lt;?=\'Контакты\'?&gt;</h2>\r\n    <p>\r\n        Для связи со мной Вы можете воспользоваться <a style="cursor: pointer; color: blue;" onclick="feedback_dialog()">формой обратной связи</a>\r\n    </p>\r\n    <p>&nbsp;</p>\r\n    \r\n    <p>\r\n        <div style="width: 110px; margin: 0 auto;">\r\n            <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">\r\n                <input type="hidden" name="cmd" value="_s-xclick" />\r\n                <input type="hidden" name="hosted_button_id" value="9NLMTUZLZWW5N" />\r\n                <input type="image" src="https://www.paypalobjects.com/ru_RU/RU/i/btn/btn_donateCC_LG.gif" border="0" name="submit" alt="PayPal — более безопасный и легкий способ оплаты через Интернет!" style="width: 100px; height: 40px;" />\r\n                <img alt="" border="0" src="https://www.paypalobjects.com/ru_RU/i/scr/pixel.gif" width="1" height="1" />\r\n            </form>\r\n        </div>\r\n    </p>\r\n    \r\n<script type="text/javascript">\r\nvar feedback_dialog = function() {\r\n    $(\'#feedback-dialog\').remove();\r\n    $(\'body\').append(\'<div id="feedback-dialog" style="display: none;"></div>\');\r\n    $(\'#feedback-dialog\').dialog({\r\n        close: function(){ $(\'#feedback-dialog\').remove(); },\r\n        modal: true,\r\n        resizable: false,\r\n        title: "Форма обратной связи",\r\n        width: 400,\r\n        height: 335,\r\n        buttons: {\r\n            \'Отмена\': function() { $(this).dialog("close");},\r\n            \'Отправить\': function() {\r\n                var form_data = $(\'#feedback-real-form\').serialize();\r\n                \r\n                jQuery.ajax({\r\n                    type: \'POST\',\r\n                    url: \'/index/feedback/default\',\r\n                    data: form_data,\r\n                    dataType: \'html\',\r\n                    success: function(data) {\r\n                        if( data == \'success\' ) {\r\n                            $(\'#feedback-dialog\').dialog("close");\r\n                            $sb.message(\'Благодарю Вас за интерес к моему проекту.<br />В скором времение я свяжусь с Вами.\', \'Ваше сообщение отправлено!\');\r\n                        } else if( data == \'fail\' ) {\r\n                            $(\'#feedback-dialog\').dialog("close");\r\n                        } else {                            \r\n                            $(\'#feedback-dialog\').html(data);\r\n                        }\r\n                    }\r\n                });\r\n            }\r\n        },\r\n        open: function(event, ui) {\r\n            jQuery.ajax({\r\n                type: \'POST\',\r\n                url: \'/index/feedback/default\',\r\n                data: {\'timer\': $sb.pre.stamp()},\r\n                dataType: \'html\',\r\n                success: function(data) {\r\n                    $(\'#feedback-dialog\').html(data);\r\n                }\r\n            });\r\n        }\r\n    });\r\n}\r\n</script>   \r\n');
/*!40000 ALTER TABLE `sb_cms_pages` ENABLE KEYS */;


-- Дамп структуры для таблица second_breath.sb_cms_uploads
DROP TABLE IF EXISTS `sb_cms_uploads`;
CREATE TABLE IF NOT EXISTS `sb_cms_uploads` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `timestamp` bigint(20) NOT NULL DEFAULT '0',
  `filename` varchar(35) NOT NULL DEFAULT '',
  `ipaddr` varchar(15) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `timestamp` (`timestamp`),
  KEY `filename` (`filename`),
  KEY `ipaddr` (`ipaddr`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы second_breath.sb_cms_uploads: ~0 rows (приблизительно)
DELETE FROM `sb_cms_uploads`;
/*!40000 ALTER TABLE `sb_cms_uploads` DISABLE KEYS */;
INSERT INTO `sb_cms_uploads` (`id`, `timestamp`, `filename`, `ipaddr`) VALUES
	(1, 1419521491, '1419521491d960647d1.', '127.0.0.1');
/*!40000 ALTER TABLE `sb_cms_uploads` ENABLE KEYS */;


-- Дамп структуры для таблица second_breath.sb_core_access
DROP TABLE IF EXISTS `sb_core_access`;
CREATE TABLE IF NOT EXISTS `sb_core_access` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module` varchar(20) NOT NULL DEFAULT '' COMMENT 'Имя модуля',
  `controller` varchar(20) NOT NULL DEFAULT '' COMMENT 'Имя контроллера',
  `method` varchar(25) NOT NULL DEFAULT '' COMMENT 'Имя метода',
  `group` tinyint(4) NOT NULL DEFAULT '6' COMMENT 'Уровень доступа',
  PRIMARY KEY (`id`),
  KEY `module` (`module`),
  KEY `controller` (`controller`),
  KEY `method` (`method`),
  KEY `group` (`group`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы second_breath.sb_core_access: 1 rows
DELETE FROM `sb_core_access`;
/*!40000 ALTER TABLE `sb_core_access` DISABLE KEYS */;
INSERT INTO `sb_core_access` (`id`, `module`, `controller`, `method`, `group`) VALUES
	(1, 'Index', 'Index', 'default', 6);
/*!40000 ALTER TABLE `sb_core_access` ENABLE KEYS */;


-- Дамп структуры для таблица second_breath.sb_core_sessions
DROP TABLE IF EXISTS `sb_core_sessions`;
CREATE TABLE IF NOT EXISTS `sb_core_sessions` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `sid` varchar(36) NOT NULL,
  `ipaddr` varchar(15) NOT NULL,
  `timestamp` bigint(20) NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `sid` (`sid`),
  KEY `timestamp` (`timestamp`),
  KEY `ipaddr` (`ipaddr`),
  KEY `sid_ipaddr` (`sid`,`ipaddr`)
) ENGINE=MyISAM AUTO_INCREMENT=39 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы second_breath.sb_core_sessions: 1 rows
DELETE FROM `sb_core_sessions`;
/*!40000 ALTER TABLE `sb_core_sessions` DISABLE KEYS */;
INSERT INTO `sb_core_sessions` (`id`, `sid`, `ipaddr`, `timestamp`, `data`) VALUES
	(38, 'aac8e037-fb86-4623-a692-19990654c814', '127.0.0.1', 1424774988, 'a:0:{}');
/*!40000 ALTER TABLE `sb_core_sessions` ENABLE KEYS */;


-- Дамп структуры для таблица second_breath.sb_core_users
DROP TABLE IF EXISTS `sb_core_users`;
CREATE TABLE IF NOT EXISTS `sb_core_users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `email` varchar(254) NOT NULL DEFAULT '',
  `password` varchar(50) NOT NULL DEFAULT '',
  `salt` varchar(36) NOT NULL DEFAULT '',
  `group_id` tinyint(4) NOT NULL DEFAULT '0',
  `registred` bigint(20) NOT NULL DEFAULT '0',
  `activated` bigint(20) NOT NULL DEFAULT '0',
  `suspended` bigint(20) NOT NULL DEFAULT '0',
  `ipaddr` varchar(15) NOT NULL DEFAULT '',
  `session` varchar(36) NOT NULL DEFAULT '',
  `timestamp` bigint(20) NOT NULL DEFAULT '0',
  `hash` varchar(32) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `email` (`email`),
  KEY `password` (`password`),
  KEY `regstamp` (`registred`),
  KEY `activated` (`activated`),
  KEY `suspended` (`suspended`),
  KEY `ipaddr` (`ipaddr`),
  KEY `session` (`session`),
  KEY `hash` (`hash`),
  KEY `email_password` (`email`,`password`),
  KEY `ipaddr_session` (`ipaddr`,`session`),
  KEY `session_ipaddr` (`session`,`ipaddr`),
  KEY `timestamp` (`timestamp`),
  KEY `group_id` (`group_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='Пользователи системы';

-- Дамп данных таблицы second_breath.sb_core_users: 1 rows
DELETE FROM `sb_core_users`;
/*!40000 ALTER TABLE `sb_core_users` DISABLE KEYS */;
INSERT INTO `sb_core_users` (`id`, `email`, `password`, `salt`, `group_id`, `registred`, `activated`, `suspended`, `ipaddr`, `session`, `timestamp`, `hash`) VALUES
	(1, 'root@sb.cms', '$1$7e4819ac$yJ54YRPJL3yRRTp0sw9fF0', '7e4819ac4deafa87aa', 0, 1418037342, 1418037342, 0, '127.0.0.1', 'aac8e037-fb86-4623-a692-19990654c814', 1424774198, 'e79c1b2249e1975a9b17d279f1db66ee');
/*!40000 ALTER TABLE `sb_core_users` ENABLE KEYS */;


-- Дамп структуры для таблица second_breath.sb_core_users_profiles
DROP TABLE IF EXISTS `sb_core_users_profiles`;
CREATE TABLE IF NOT EXISTS `sb_core_users_profiles` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `nicname` varchar(32) NOT NULL DEFAULT '',
  `first_name` varchar(80) NOT NULL DEFAULT '',
  `middle_name` varchar(80) NOT NULL DEFAULT '',
  `last_name` varchar(80) NOT NULL DEFAULT '',
  `birthday` date DEFAULT NULL,
  `gender` enum('male','female','other') NOT NULL DEFAULT 'male',
  `phone` varchar(20) NOT NULL DEFAULT '',
  `avatar` varchar(150) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `birthday` (`birthday`),
  KEY `gender` (`gender`),
  KEY `phone` (`phone`),
  KEY `nicname` (`nicname`),
  KEY `first_name` (`first_name`),
  KEY `middle_name` (`middle_name`),
  KEY `last_name` (`last_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Профили пользователей';

-- Дамп данных таблицы second_breath.sb_core_users_profiles: 1 rows
DELETE FROM `sb_core_users_profiles`;
/*!40000 ALTER TABLE `sb_core_users_profiles` DISABLE KEYS */;
INSERT INTO `sb_core_users_profiles` (`id`, `nicname`, `first_name`, `middle_name`, `last_name`, `birthday`, `gender`, `phone`, `avatar`) VALUES
	(1, 'Superuser', '', '', '', '2014-12-08', 'male', '', 'http://default.local/Interface/Common/Images/root.png');
/*!40000 ALTER TABLE `sb_core_users_profiles` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
