<?php
/**
* @author       Tishchenko Alexander
* @copyright    Copyright (c) 2014 All rights to Tishchenko A.
* @package		Second-Breath PHP5 framework
* @filesource	Bootstrap.php
*/

require_once 'Constants.php';

/**
* Автолоадер для классов пространства имен
* 
* @param string $className
*/
function SB_Loader($className)
{       
	$c_name = (string)str_replace( '\\', '|', $className );

	if( preg_match("#^\|?SB\|#", $c_name) ) {
		$file_path = SB_PATH . DIR_SEP . $c_name . '.php';
	} else {
		$file_path = ROOT . DIR_SEP . $c_name . '.php';
	}

	$file_path = str_replace('|', DIR_SEP, $file_path);
	$file_path = str_replace(DIR_SEP . DIR_SEP, DIR_SEP, $file_path);

	if( is_file($file_path) ) {
		require($file_path);
		return true;
	}
}

set_include_path( __DIR__ . ';' . get_include_path() );
define('SB_PATH', __DIR__);

spl_autoload_register('SB_Loader');
require_once(__DIR__ . DIR_SEP . 'vendor' . DIR_SEP . 'autoload.php');

if( !headers_sent() ) header('X-Powered-By: ' . FRAMEWORK);

\SB\Profiler::start();

require_once 'Functions.php';

\SB\Exception\SB_Exception::setExceptionHandler();
$OLD_ERROR_HANDLER = set_error_handler('system_error_handler');

if( !is_dir(TEMP_DIR) )    sb_mkdir(TEMP_DIR, 0777);
if( !is_dir(CONTENT_DIR) ) sb_mkdir(CONTENT_DIR, 0777);
if( !is_dir(LOGS_DIR) )    sb_mkdir(LOGS_DIR, 0777);

$_ = get_disabled_functions();

if( !in_array('ini_set', $_) ) {
    $php_cfg  = load_ini('config')->php;

    if( array_count($php_cfg) > 0 ) {
        foreach($php_cfg as $key=>$val) {
            $key = str_replace( '__', '.', $key);
            @ini_set($key, $val);
        }
        
        unset($key, $val, $_);
    }
}

/**
* Получаем списки модулей и плагинов приложения
*/
\SB\Cloud::Instance()->modules = get_directory_list(MODULES_DIR);
\SB\Cloud::Instance()->plugins = get_directory_list(PLUGINS_DIR);

/**
* Подгружаем роуты для модулей
*/
if( array_count(\SB\Cloud::Instance()->modules) > 0 ) {
	foreach(\SB\Cloud::Instance()->modules as $key=>$val) {
		if( is_file(MODULES_DIR . DIR_SEP . $val . DIR_SEP . 'Library' . DIR_SEP . 'Routes.php') ) {
			require_once MODULES_DIR . DIR_SEP . $val . DIR_SEP . 'Library' . DIR_SEP . 'Routes.php';
		}
	}
	unset($key, $val);
}

//use Illuminate\Database\Capsule\Manager as DB;
class_alias('\\Illuminate\\Database\\Capsule\\Manager', '\\DB');
class_alias('\\Illuminate\\Database\\Eloquent\\Model' , '\\Model');
class_alias('\\Illuminate\\Database\\Schema\\Builder' , '\\Schema');

$capsule = new \DB();
$config  = load_ini('dbase');

foreach($config as $key=>$val) {
    $capsule->addConnection([
        'driver'    => 'mysql',
        'host'      => $val->host,
        'database'  => $val->database,
        'username'  => $val->username,
        'password'  => $val->password,
        'charset'   => 'utf8',
        'collation' => 'utf8_general_ci',
        'prefix'    => $val->prefix,
    ], strtolower($key));
}

$capsule->setAsGlobal();
$capsule->bootEloquent();
$capsule->setEventDispatcher(
    new \Illuminate\Events\Dispatcher(
        new \Illuminate\Container\Container()
    )
);

\SB\Session::Instance();

/**
* Запускаем наше приложение
*/
\SB\Kernel::Instance()->Execute();
