<?php
/**
* @author       Tishchenko Alexander
* @copyright    Copyright (c) 2014 All rights to Tishchenko A.
* @package		Second-Breath PHP5 framework
* @filesource	Functions.php
*/

if( !defined('__FUNCTIONS__') ) {
    define('__FUNCTIONS__', true);

    
  function http_referer( $clear = true )
  {
      global $_SERVER;
      
      if( !isset($_SERVER['HTTP_REFERER']) ) {
          return false;
      }

      if( $clear === false ) {
          return $_SERVER['HTTP_REFERER'];
      }
      
      $_ = preg_replace('%^https?://[a-z0-9\.\-]+/?%iu', '', $_SERVER['HTTP_REFERER']) ;
      
      return (!empty($_)) ? trim($_) : false;
  }  
// ------------------------------------------------------------------------------
  /**
  * Corretion for directory path
  * 
  * @param string $dir
  * @return string
  */
  function correct_directory_path( $dir, $end_slash = false )
  {
      if( empty($dir) ) return $dir;
      
      $dir = str_replace(':', '||', $dir);
      $dir = str_replace('\\\\', '::', $dir);
      $dir = str_replace('\\', '::', $dir);
      $dir = str_replace('//', '::', $dir);
      $dir = str_replace('/', '::', $dir);
      $dir = str_replace('::', '/', $dir);
      $dir = str_replace('||', ':', $dir);
      
      if( $end_slash && !preg_match("#(.*)\/$#is" , $dir, $tmp) ) {
          $dir = $dir . '/';
      }
      
      return $dir;
  }

// ------------------------------------------------------------------------------

  function sb_mkdir($path, $mode = 0755)
  {
      if( !empty($path) && is_string($path) && !is_dir($path) ) {
          $path = correct_directory_path($path);
          
          if( !mkdir($path, $mode, true) ) {
              system_error(
                  'Can not create directory: ' . $path,
                  500
              );
          }
          
          @chmod($path, $mode);
      }
  }
  
// ------------------------------------------------------------------------------
  /**
  * Return IP adddress of client
  * 
  */
  function get_ip_address() 
  {
      global $_SERVER;
       
      if( !empty($_SERVER['REMOTE_ADDR']) && !empty($_SERVER['HTTP_CLIENT_IP']) ) {
          $ipaddr = $_SERVER['HTTP_CLIENT_IP'];
      } else if( !empty($_SERVER['HTTP_CLIENT_IP']) ) {
          $ipaddr = $_SERVER['HTTP_CLIENT_IP'];
      } else if( !empty($_SERVER['HTTP_X_FORWARDED_FOR']) ) {
          $ipaddr = $_SERVER['HTTP_X_FORWARDED_FOR'];
      } else {
          $ipaddr = $_SERVER['REMOTE_ADDR'];
      }
        
      if( $ipaddr === false ) {
          return '0.0.0.0';
      }
        
      if( strstr($ipaddr, ',') ) {
          $x = explode(',', $ipaddr);
          $ipaddr = end( $x );
      }
        
      if( filter_var($ipaddr, FILTER_VALIDATE_IP) === false ) {
          $ipaddr = '0.0.0.0';
      }
        
      return $ipaddr;
  }

// -------------------------------------------------------------------------------------
  /**
  * Check URL
  * 
  * @param string $url
  */
  function is_url_exists($url)
  {
  	  if( !\SB\Match::is_url($url) ) return false;
	  $hdrs = @get_headers($url);
	  if( isset($hdrs[0]) && preg_match('/HTTP\/1\.[0-1]\s+\d{3}/is', $hdrs[0]) ) return true;
	  return false;
  }

// -------------------------------------------------------------------------------------
  /**
  * Sending header to disable caching pages
  * 
  */
  function http_cache_off()
  {
      if( !headers_sent() ) {
          header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
          header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
          header('Cache-Control: no-cache, must-revalidate');
          header('Cache-Control: post-check=0,pre-check=0', false);
          header('Cache-Control: max-age=0', false);
          header('Pragma: no-cache');
      }
  }

// -------------------------------------------------------------------------------------
  /**
  * extract the fractional part of a fractional number
  * 
  * @param mixed $num
  * @return mixed
  */
  function fract($num)
  {
      if( is_numeric($num) ) {
          $num -= floor( (float)$num );
          return (int)str_replace('0.', '', (string)$num);
      }
        
      return null;
  }

// -------------------------------------------------------------------------------------

  function float_extract($num)
  {
      return ( is_float($num) ) ? explode('.', (string)$num) : false;
  }

// -------------------------------------------------------------------------------------
  /**
  * проверка на четность
  */
  function is_even($num)
  {
      if( !is_numeric($num) ) return false;
      if( !preg_match('/[\.]/s', $num) ) {
          return ( fract($num) & 1 ) ? false : true;
      } else {
          return ( $num & 1 ) ? false : true;
      }
  }
  
// ------------------------------------------------------------------------------

  function escape($str, $escapemethod = 'htmlspecialchars')
  {
      if( is_array($str) && count($str) ) {
          $_ = [];
          foreach($str as $key=>$val) {
              $_[ $key ] = escape($val);
          }
          return $_;
      } else if( is_string($str) ) {
          if( in_array($escapemethod, ['htmlspecialchars', 'htmlentities']) ) {
              return call_user_func($escapemethod, $str, ENT_COMPAT, 'UTF-8');
          }
          
          return htmlspecialchars($str, ENT_QUOTES, 'UTF-8');
      }
      
      return $str;
  }    
  
// ------------------------------------------------------------------------------

  function unescape($str)
  {
      if( is_array($str) && count($str) ) {
          $_ = [];
          foreach($str as $key=>$val) {
              $_[ $key ] = unescape($val);
          }
          return $_;
      } else if( is_string($str) ) {
          return htmlspecialchars_decode($str, ENT_QUOTES);
      }
      
      return $str;
  }
  
// ------------------------------------------------------------------------------
  /**
  * Salt generation
  * 
  * @return string
  */
  function salt_generation( $lenght = 18 )
  {
      return substr( sha1(mt_rand()), 0, $lenght );
  }

// ------------------------------------------------------------------------------

  function mkpass($password, $salt = null)
  {
      if( !isset($password) ) return false;
      if( null === $salt ) {
          $salt = substr( sha1( mt_rand() ), 0, 16 );
      }
    
      return crypt( md5($password), '$1$' . $salt . '$' );
  }
 
// ------------------------------------------------------------------------------

  function encrypt($string, $key = null) 
  {
      // The key-scheduling algorithm (KSA)
      $s   = [];        
      $key = (null === $key) ? '6b8dafbb42161887b74823ad7e4048f8' : (string)$key;
      
      for( $i = 0; $i < 256; $i++ ) {
          $s[$i] = $i;
      }        

      $j = 0;
      $x = null;

      for( $i = 0; $i < 256; $i++ ) {
          $j     = ($j + $s[$i] + ord($key[$i % strlen($key)])) % 256;
          $x     = $s[$i];
          $s[$i] = $s[$j];
          $s[$j] = $x;
      }

      // The pseudo-random generation algorithm (PRGA)
      $cipher = '';
      $i = $j = $y = 0;

      for($y = 0; $y < strlen($string); $y++ ) {
          $i     = ($i + 1) % 256;
          $j     = ($j + $s[$i]) % 256;
          $x     = $s[$i];
          $s[$i] = $s[$j];
          $s[$j] = $x;
          
          $cipher .= $string[$y] ^ chr($s[($s[$i] + $s[$j]) % 256]);
      }

      return $cipher;
  }
  
// ------------------------------------------------------------------------------

  function decrypt($cipher, $key = null) {
      $key = (null === $key) ? '6b8dafbb42161887b74823ad7e4048f8' : (string)$key;
      return encrypt($cipher, $key);
  }  
  
// ------------------------------------------------------------------------------

  function str_base64_encrypt($str)
  {
      return base64_encode( encrypt($str, 'string_encryption') );
  }
  
// ------------------------------------------------------------------------------

  function str_base64_decrypt($str)
  {
      return decrypt( base64_decode($str), 'string_encryption' );
  }
  
// ------------------------------------------------------------------------------
  /**
  * Convert array to object
  * 
  * @param array $_array
  * @return \SB\stdObject
  */
  function array_to_object( $_array )
  {
	  if( array_count($_array) > 0 ) {
		  return new \SB\stdObject($_array);
	  }

	  return new \SB\stdObject();
  }
  
// -------------------------------------------------------------------------------------
  /**
  * Get settings from INI files
  * 
  * @param string $name
  * @return \SB\stdObject
  */
  function load_ini($name)
  {
      if( !isset(\SB\Cloud::Instance()->ini) ) {
          \SB\Cloud::Instance()->ini = [];
      }
      
      $cloud_ini = \SB\Cloud::Instance()->ini;
      
      if( isset($cloud_ini[ $name ]) ) return $cloud_ini[ $name ];
            
      if( isset($name) && is_file(SETTINGS_DIR . DIR_SEP . (string)$name . '.ini') ) {
          try {
              $tmp = parse_ini_file( SETTINGS_DIR . DIR_SEP . (string)$name . '.ini', true );
          } catch(\SB\Exception\SB_Exception $e) {
              system_exception( $e );
          }
          
          if( array_count($tmp) > 0 ) {
              $ini = new \SB\stdObject();
              
              foreach($tmp as $key=>$val) {
                  if( array_count($val) > 0 ) {
                      $section = str_replace( '.', '_', sb_strtolower($key) );
                      $data    = [];
                      
                      foreach($val as $k=>$v) {
                          $v = str_replace( 
                              ['%TEMP_DIR%', '%ROOT_DIR%', '%DOCUMENT_ROOT%', '%BASE_URL%', '%LOGS_DIR%'],
                              [TEMP_DIR, ROOT, DOCUMENT_ROOT, BASE_URL, LOGS_DIR],
                              $v
                          );
                          
                          $data[ $k ] = $v;
                      }
                      
                      $ini->$section = $data;
                  } else {
                      $varible       = str_replace( '.', '_', sb_strtolower($key) );
                      $ini->$varible = $val;
                  }
              }
              
              $cloud_ini[ $name ]        = $ini;
              \SB\Cloud::Instance()->ini = $cloud_ini;
              
              unset($cloud_ini);
              
              return $ini;
          }
      }
      
      return new \SB\stdObject();
  }
  
// -------------------------------------------------------------------------------------
  /**
  * Check module
  * 
  * @param string $name
  * @return bool
  */
  function is_module_exists( $name )
  {
      if( empty($name) or !is_scalar($name) or preg_match("/[^a-z0-9\_]/is", (string)$name) ) return false;
      if( $name == 'Plugin' or $name == 'Plugins' or $name == 'Phpinfo' ) return false;
      if( in_array($name, \SB\Cloud::Instance()->modules) ) return true;
      
      return false;
  }
  
// -------------------------------------------------------------------------------------
  /**
  * Check controller
  * 
  * @param string $module
  * @param string $controller
  * @return bool
  */
  function is_controller_exists($module, $controller)
  {
  	  $C_DIR = MODULES_DIR . DIR_SEP . (string)$module . DIR_SEP . 'Controllers' . DIR_SEP;
  	  
	  if( is_module_exists($module) && preg_match('/^[a-z\_]+$/i', (string)$controller) and is_file( $C_DIR . "{$controller}.php" ) ) {
		  return true;
	  }
	  
	  return false;
  }
  
// -------------------------------------------------------------------------------------
  /**
  * Check plugin
  * 
  * @param string $name
  * @return bool
  */
  function is_plugin_exists( $name )
  {
      if( empty($name) || !is_scalar($name) || preg_match("/[^a-z0-9\_]/is", (string)$name) ) return false;
      if( in_array($name, \SB\Cloud::Instance()->plugins) ) return true;
      
      return false;
  }
// -------------------------------------------------------------------------------------
  /**
  * Show system error
  * 
  * @param string $message
  * @param integer $status
  * @param string $title
  */
  function system_error($message, $status = 500)
  {
      if( is_ajax() ) {
          $html = "<div id=\"error-header\" style=\"font-size: 10pt; font-weight: bold;\">" . CORE_NAME . " system error</div><br />" .
                  "<div id=\"error-content\" style=\"font-size: 8pt;\">{$message}</div>";
      } else {
          set_http_status($status);
          
          $html = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN"><html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" />' .
                  "<meta name=\"generator\" content=\"" . FRAMEWORK . "\" /><meta name=\"description\" content=\"" . CORE_NAME . " system error\" />" .
                  "<title>" . CORE_NAME . " system error</title>\n<style>\nbody { font-family: Tahoma, Arial, sans-serif; fint-size: 8pt; }</style></head><body>" .
                  "<div id=\"error-header\" style=\"font-size: 14pt; color: red;\">" . CORE_NAME . " system error</div><br /><div id=\"error-content\" style=\"font-size: 10pt;\">{$message}</div>" .
                  '<div id="error-footer" style="margin-top: 10px; border-top: 1px solid gray; font-size: 8pt;">' .
                  '<span style="color: gray;">' . FRAMEWORK . ', Copyright &copy; 2014 Tishchenko A., All rights reserved.</span></div></body></html>';
      }
              
      \SB\Log::Instance()->add( $message, true )->write();

      terminate($html);
  }
// -------------------------------------------------------------------------------------
  function system_error_handler($errno, $errstr, $errfile, $errline)
  {
      if( !(error_reporting() & $errno) ) return;

      switch ($errno) {
          case E_ERROR:
          case E_CORE_ERROR:
          case E_COMPILE_ERROR:
          case E_PARSE:
          case E_USER_ERROR:
          case E_RECOVERABLE_ERROR:
              system_error( "[{$errno}] {$errstr}\n<br />Fatal error on line {$errline} in file {$errfile}\n", 500 );
              break;

          case E_USER_WARNING:
          case E_WARNING:
              \SB\Log::Instance()->add("WARNING: [{$errno}] {$errstr}, {$errfile}: {$errline}");
              echo "<b>WARNING</b> [{$errno}] {$errstr}, {$errfile}: {$errline}<br />\n";
              break;

          case E_USER_NOTICE:
          case E_NOTICE:
              \SB\Log::Instance()->add("NOTICE: [{$errno}] {$errstr}, {$errfile}: {$errline}");
              echo "<b>NOTICE</b> [{$errno}] {$errstr}, {$errfile}: {$errline}<br />\n";
              break;

          case E_STRICT:
          default:
              \SB\Log::Instance()->add("UNKNOWN: [{$errno}] {$errstr}, {$errfile}: {$errline}");
              echo "Unknown error type: [{$errno}] {$errstr}, {$errfile}: {$errline}<br />\n";
              break;
      }

      return true;
  }
  
// -------------------------------------------------------------------------------------
  /**
  * Show system error
  * 
  * @param string $message
  * @param integer $status
  * @param string $title
  */
  function system_exception(\Exception $e)
  {
      $trace = str_replace('#', "<br />#", $e->getTraceAsString());
      
      if( is_ajax() ) {
          $html = "<div id=\"exeption-title\" style=\"font-size: 14pt;\">" . CORE_NAME . " exception</div><br />" .
                  "<div id=\"exeption-content\" style=\"font-size: 10pt;\">" .
                  "<strong>File:</strong> {$e->getFile()}<br />\n" .
                  "<strong>Line:</strong> {$e->getLine()}<br />\n" .
                  "<strong style=\"color: red;\">Message:</strong> {$e->getMessage()}\n</div>" .
                  "<div id=\"exeption-trace\">{$trace}</div>";
      } else {
          set_http_status( $e->getCode() );
          
          $html = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN"><html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" />' .
                  "<meta name=\"generator\" content=\"" . FRAMEWORK . "\" /><meta name=\"description\" content=\"" . CORE_NAME . " exception\" />" .
                  "<title>" . CORE_NAME . " exception</title>\n<style>\nbody { font-family: Tahoma, Arial, sans-serif; fint-size: 8pt; }\n</style></head><body>" .
                  "<div id=\"exeption-header\" style=\"font-size: 14pt;\">" . CORE_NAME . " exception</div><br /><div id=\"exeption-content\" style=\"font-size: 10pt;\">" .
                  "<strong>File:</strong> {$e->getFile()}<br />\n<strong>Line:</strong> {$e->getLine()}<br />\n<strong style=\"color: red;\">Message:</strong> {$e->getMessage()}\n</div>" .
                  "<div id=\"exeption-trace\"><p style=\"font-size: 10pt;\"><strong>Trace:</strong><br />\n<span style=\"font-size: 8pt;\">{$trace}</span></p></div>" .
                  '<div id="exeption-footer" style="margin-top: 10px; border-top: 1px solid gray; font-size: 8pt;">' .
                  '<span style="color: gray;">' . FRAMEWORK . ', Copyright &copy; 2013 Tishchenk A., All rights reserved.</span></div></body></html>';
      }
      
      \SB\Log::Instance()->add( $e->getMessage(), true )->write();
      
      terminate($html);
  }

// ------------------------------------------------------------------------------
  /**
  * Make correct URL
  * 
  * @param string $url
  * @param bool $add_suf
  */
  function make_url($url, $add_suf = true) 
  {
      if( empty($url) || !is_scalar($url) ) return false;
      if( preg_match('#^(http|https|ftp)://(.*?)#i', $url) ) return $url;
      if( !preg_match('/^\//', $url) ) {
		  $url = '/' . $url;
      }
      
      $url = preg_replace('#^([a-z0-9\/\.\_\-]+)\/$#isu', '$1', $url);
      if( $add_suf && !preg_match('/^(\w+)\.html$/is', $url) ) { $url = $url.'.html';}
      
      return BASE_URL . $url;
  }

// -------------------------------------------------------------------------------------
  function get_x_response_type()
  {
      global $_SERVER;
      
      return $_SERVER['HTTP_X_RESPONSE_TYPE'];
  }
// -------------------------------------------------------------------------------------
  /**
  * Redirector
  * 
  * @param string $url
  * @param string $message
  * @param bool $error
  * @param integer $status
  */
  function redirect($url = '', $message = '', $error = false, $status = 301)
  {
      if( empty($url) ) { 
          $url = '/' . DEFAULT_MODULE;
      }

      if( !empty($message) && !preg_match('#^(http|https|ftp)://(.*?)#i', $url) ) {
      	  make_temp( 
              make_temp_name_from_uri($url), 
              ['message' => $message, 'error' => $error] 
          );
      }

      http_cache_off();
      
      if( is_sb_ajax() && !headers_sent() ) {
          if( get_x_response_type() == 'html' ) {
              exit( 
                javascript('$sb.redirect("' . make_url($url) . '");')
              );
          } else if( get_x_response_type() == 'script' ) {
              exit('$sb.redirect("' . make_url($url) . '");');
          }
      }
      
      header( 'Location: ' . make_url($url), true, $status);
      terminate();
  }
  
// ------------------------------------------------------------------------------
  /**
  * Get ststus by code number
  * 
  * @param integer $code
  */
  function get_http_status($code)
  {
      if( !is_numeric($code) ){ return false;}
    
      $_HTTP_STATUS = [
            100 => '100 Continue',
            101 => '101 Switching Protocols',
            102 => '102 Processing',
            200 => '200 OK',
            201 => '201 Created',
            202 => '202 Accepted',
            203 => '203 Non-SB_Authoritative Information',
            204 => '204 No Content',
            205 => '205 Reset Content',
            206 => '206 Partial Content',
            207 => '207 Multi Status',
            226 => '226 IM Used',
            300 => '300 Multiple Choices',
            301 => '301 Moved Permanently',
            302 => '302 Found',
            303 => '303 See Other',
            304 => '304 Not Modified',
            305 => '305 Use Proxy',
            306 => '306 (Unused)',
            307 => '307 Temporary Redirect',
            400 => '400 Bad Request',
            401 => '401 Unauthorized',
            402 => '402 Payment Required',
            403 => '403 Forbidden',
            404 => '404 Not Found',
            405 => '405 Method Not Allowed',
            406 => '406 Not Acceptable',
            407 => '407 Proxy nCore_Authentication Required',
            408 => '408 Request Timeout',
            409 => '409 Conflict',
            410 => '410 Gone',
            411 => '411 Length Required',
            412 => '412 Precondition Failed',
            413 => '413 Request Entity Too Large',
            414 => '414 Request-URI Too Long',
            415 => '415 Unsupported Media Type',
            416 => '416 Requested Range Not Satisfiable',
            417 => '417 Expectation Failed',
            420 => '420 Policy Not Fulfilled',
            421 => '421 Bad Mapping',
            422 => '422 Unprocessable Entity',
            423 => '423 Locked',
            424 => '424 Failed Dependency',
            426 => '426 Upgrade Required',
            449 => '449 Retry With',
            500 => '500 Internal Server Error',
            501 => '501 Not Implemented',
            502 => '502 Bad Gateway',
            503 => '503 Service Unavailable',
            504 => '504 Gateway Timeout',
            505 => '505 HTTP Version Not Supported',
            506 => '506 Variant Also Varies',
            507 => '507 Insufficient Storage',
            509 => '509 Bandwidth Limit Exceeded',
            510 => '510 Not Extended'
      ];
    
      if( !empty($_HTTP_STATUS[$code]) ) {
          return $_HTTP_STATUS[$code];
      }
    
      return false;
  }
// --------------------------------------------------------------------------------
  /**
  * Set HTTP status
  * 
  * @param integer $code
  */
  function set_http_status($code)
  {
      $status = get_http_status($code);
      
      if( $status != false && !headers_sent() ) {
          header('HTTP/1.1 ' . $status);
          header('Status: ' . $status);
          return true;
      }
      
      return false;
  }
// --------------------------------------------------------------------------------
  /**
  * Check for ajax request
  * 
  */
  function is_ajax()
  {
  	  global $_SERVER;
  	  
	  if( !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' ) {
	      return true; 
	  } else if( !empty($_SERVER['X_REQUESTED_WITH']) && $_SERVER['X_REQUESTED_WITH'] == 'XMLHttpRequest' ) {
	      return true;
	  } else if( !empty($_SERVER['HTTP_ACCEPT']) && (false !== strpos($_SERVER['HTTP_ACCEPT'], 'text/x-ajax')) ) {
	      return true;
	  }
	  
	  return false;
  }
// --------------------------------------------------------------------------------
  /**
  * Check for Second-Breath ajax request
  * From plugin Common
  */
  function is_sb_ajax()
  {
      global $_SERVER;

      if( is_ajax() ) {
          if( isset($_SERVER['HTTP_X_REQUEST_TYPE']) && $_SERVER['HTTP_X_REQUEST_TYPE'] == 'Expedited' ) {
              return true;
          }
      }
      
      return false;
  }                
// --------------------------------------------------------------------------------
  /**
  * Internal function for terminate script execution
  * Reseved for internal use
  * Exit/Die alias
  * 
  * @param mixed $message
  */
  function terminate($message = null)
  {
      if( class_exists('\\SB\\Kernel') ) {
          \SB\Kernel::Terminate( $message );
      }
      
	  exit($message);
  }
// ------------------------------------------------------------------------------
  /**
  * Return files list
  * Support files masks
  * 
  * @param string $path
  * @param array $extentions
  * @param bool $files_only
  * @return []
  */
  function get_files_list($path, $extentions = array(), $files_only = false)
  {
      if( empty($path) || !is_dir($path) ) {
          return false;
      }
      
      if( array_count($extentions) > 1 ) {
          $extentions = '*.{' . implode(',', $extentions) . '}';
          $flag = GLOB_BRACE;
      } else if( array_count($extentions) == 1 ) {
          $extentions = '*.' . reset($extentions);
          $flag = null;
      } else {
          $extentions = '*';
          $flag = null;
      }

      $files = glob( $path . DIR_SEP . $extentions , $flag);
      
      if( !empty($files) ) {
          $_result = [];
          
          foreach($files as $key=>$val) {
              if( is_file($val) ) {
              	  if( !$files_only ) {
              	  	  $_result[ str_replace($path . DIR_SEP, '', $val) ] = filesize($val);
				  } else {
					  $_result[] = str_replace($path . DIR_SEP, '', $val);
				  }
              }
          }
          
          return $_result;
      }
      
      return false;
  }
// ------------------------------------------------------------------------------
  /**
  * return directory list by path
  * 
  * @param string $path
  * @return []
  */
  function get_directory_list($path)
  {
      if( empty($path) || !is_dir($path) ) {
          return false;
      }
      
      $search_path = $path;
      
      if( !preg_match('#^(.*)' . DIR_SEP . '$#', $path) ) {
          $search_path = $path = $path . DIR_SEP;
      }

      if( !preg_match('#^(.*)\*$#', $search_path) ) {
          $search_path = $search_path . '*';
      }
      
      $dirs = glob( $search_path , GLOB_ONLYDIR);
      
      if( !empty($dirs) ) {
          $_result = [];
          
          foreach($dirs as $key=>$val) {
              if( is_dir($val) ) {
                  $_result[] = str_replace($path, '', $val);
              }
          }
          
          return $_result;
      }
      
      return false;
  }
// ------------------------------------------------------------------------------
  /**
  * Show 404 error message
  * 
  */
  function show_error_404()
  {
	  if( is_controller_exists('Errors', 'Index') ) {
		  redirect('/errors/index/show404');
	  }
	  
	  system_error('404, Not found', 404);
  }
// ------------------------------------------------------------------------------
  /**
  * Show 404 error message
  * 
  */
  function show_error_403()
  {
      if( is_controller_exists('Errors', 'Index') ) {
          redirect('/errors/index/forbidden');
      }
      
      system_error('403, Forbidden', 403);
  }
// ------------------------------------------------------------------------------
  /**
  * Get plugin content
  * 
  * @param array $params
  * @param mixed $smarty
  */
  function sb_plugin($name, $params = null)
  {    
  	  $plugin_file = PLUGINS_DIR . DIR_SEP . $name . DIR_SEP . $name . '.php';

	  if( is_file($plugin_file) ) {
		  $plugin_class = "Application\\Plugins\\{$name}\\{$name}";
		  $plugin       = new $plugin_class($params);
		  
          if( method_exists($plugin, '_before') ) {
              $plugin->_before();
          }

		  if( method_exists($plugin, 'content') ) {
		  	  $_ = $plugin->content();
		  }
          
          if( method_exists($plugin, '_after') ) {
              $plugin->_after();
          }
          
          unset($plugin_file, $plugin, $plugin_class);
          
          return $_;
	  }
	  
	  return null;
  }
  
// ------------------------------------------------------------------------------

  function make_temp_name_from_uri($uri)
  {
  	  if( empty($uri) || !preg_match('/[a-z_\/\-]/is', $uri) ) {
		  return false;
  	  }
  	  
  	  $uri  = preg_replace('#^/#', '', $uri);
	  $path = explode('/', $uri, 4);
	  
	  if( isset($path[3]) ) {
		  unset($path[3]);
	  }
	  
	  return implode('_', $path) . '_' . \SB\Session::Instance()->id(true) . '.tmp';
  }

// ------------------------------------------------------------------------------

  function make_temp($temp_name, $content, $keep = 60)
  {
	  if( empty($temp_name) ) return false;
	  
	  $file_name = $temp_name . '_' . \SB\Session::Instance()->id(true) . '.tmp';
	  $tmp       = new \SB\Temp($file_name, TEMP_DIR);

	  return $tmp->set_content([
	      'content' => $content,
	      'keepto'  => time() + (int)$keep,
	  ])->write();
  }
  
// ------------------------------------------------------------------------------

  function pick_temp($temp_name)
  {
	  if( empty($temp_name) ) return null;
	  
	  $file_name = $temp_name . '_' . \SB\Session::Instance()->id(true) . '.tmp';
	  
	  if( !is_file(TEMP_DIR . DIR_SEP . $file_name) ) return null;
	  
	  $tmp = new \SB\Temp($file_name, TEMP_DIR);
	  $_   = $tmp->read();
	  $tmp->delete();
	  
	  return ( time() > (int)$_['keepto'] ) ? null : $_['content'];
  }

// ------------------------------------------------------------------------------

  function sb_ucfirst( $str )
  {
      if( function_exists('mb_strlen') ) {
          return mb_strtoupper( mb_substr( $str, 0, 1, 'UTF-8' ), 'UTF-8' ) . mb_substr( $str, 1, mb_strlen( $str ), 'UTF-8' );
      }

      if( function_exists('iconv') ) {
          $result = iconv('utf-8', 'windows-1251', $str);
          return iconv('windows-1251', 'utf-8', ucfirst( $result ) );
      }
      
      return ucfirst( $str );
  }

// ------------------------------------------------------------------------------

  function sb_strlen( $str )
  {
      if( function_exists('mb_strlen') ) return mb_strlen($str, 'UTF-8');
      if( function_exists('iconv') ) {
          $result = iconv('utf-8', 'windows-1251', $str);
          return strlen( $result );
      }
      
      return strlen( $str );
  }

// ------------------------------------------------------------------------------

  function sb_strtolower( $str )
  {
      if( function_exists('mb_strtolower') ) return mb_strtolower($str, 'UTF-8');
      if( function_exists('iconv') ) {
          $result = iconv('utf-8', 'windows-1251', $str);
          return iconv('windows-1251', 'utf-8', strtolower( $result ) );
      }

      return strtolower( $str );
  }

// ------------------------------------------------------------------------------

  function sb_strtoupper( $str )
  {
      if( function_exists('mb_strtoupper') ) return mb_strtoupper($str, 'UTF-8');
      if( function_exists('iconv') ) {
          $result = iconv('utf-8', 'windows-1251', $str);
          return iconv('windows-1251', 'utf-8', strtoupper( $result ) );
      }

      return strtoupper( $str );
  }

// ------------------------------------------------------------------------------

  function sb_strstr($haystack, $needle, $part = false)
  {
      return ( function_exists('mb_strstr') ) ? mb_strstr($haystack, $needle, $part, 'UTF-8') : strstr($haystack, $needle, $part);
  }

// ------------------------------------------------------------------------------

  function string_pack( $str )
  {
    $str = str_replace(["\r",',','.','!','?',':',';','(',')','[',']','{','}','"',"'"], ' ', strip_tags($str));
    return sb_strtolower( preg_replace('/\s+/s', ' ', str_replace("\n", ' ', $str)) );
  }

// ------------------------------------------------------------------------------

  function crop_string($string, $lenght)
  {
      $string = strip_tags($string);

      if( function_exists('mb_strlen') ) {
          $len    = (mb_strlen($string) > $lenght) ? mb_strripos( mb_substr($string, 0, $lenght), ' ' ) : $lenght;
          $result = mb_substr($string, 0, $len);
          
          return (mb_strlen($string) > $lenght) ? $result . '...' : $result;
      }

      if( function_exists('iconv') ) {
          $result = iconv('utf-8', 'windows-1251', $string);
          $length = strripos( substr($result, 0, $length), ' ');
          
          return iconv('windows-1251', 'utf-8', substr($result, 0, $length) );
      }
      
      $len    = (strlen($string) > $lenght) ? strripos( substr($string, 0, $lenght), ' ' ) : $lenght;
      $result = substr($string, 0, $len);
      
      return (mb_strlen($string) > $lenght) ? $result . '...' : $result;
  }

// ------------------------------------------------------------------------------

  function uuid()
  {
      return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
          mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),
          mt_rand( 0, 0x0fff ) | 0x4000,
          mt_rand( 0, 0x3fff ) | 0x8000,
          mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
      );
  }

// ------------------------------------------------------------------------------

  function is_uuid($uuid)
  {
      return preg_match('/^\{?[0-9a-f]{8}\-?[0-9a-f]{4}\-?[0-9a-f]{4}\-?[0-9a-f]{4}\-?[0-9a-f]{12}\}?$/i', $uuid) === 1;
  }
  
// ------------------------------------------------------------------------------

  function get_work_path()
  {
      $module = \SB\Router::Instance()->get_module_name();
      
      if( $module == 'Plugins' && is_plugin_exists($module) ) {
          return PLUGINS_DIR . DIR_SEP . \SB\Router::Instance()->get_controller_name();
      } else if( is_module_exists($module) ) {
          return MODULES_DIR . DIR_SEP . $module;
      }
      
      return false;
  }

// ------------------------------------------------------------------------------

  function sb_date_format($string, $format = null, $default_date = '', $formatter = 'auto')
  {
      if( $format === null ) {
          $format = '%b %e, %Y';
      }
      
      if( $string != '' && $string != '0000-00-00' && $string != '0000-00-00 00:00:00' ) {
          $timestamp = sb_make_timestamp($string);
      } else if( $default_date != '' ) {
          $timestamp = sb_make_timestamp($default_date);
      } else {
          return;
      } 
      
      if( $formatter == 'strftime' || ($formatter == 'auto' && strpos($format,'%') !== false) ) {
          if( DIRECTORY_SEPARATOR == '\\' ) {
              $_win_from = ['%D', '%h', '%n', '%r', '%R', '%t', '%T'];
              $_win_to   = ['%m/%d/%y', '%b', "\n", '%I:%M:%S %p', '%H:%M', "\t", '%H:%M:%S'];

              if( strpos($format, '%e') !== false ) {
                  $_win_from[] = '%e';
                  $_win_to[]   = sprintf('%\' 2d', date('j', $timestamp));
              } 
              
              if( strpos($format, '%l') !== false ) {
                  $_win_from[] = '%l';
                  $_win_to[]   = sprintf('%\' 2d', date('h', $timestamp));
              } 
              
              $format = str_replace($_win_from, $_win_to, $format);
          } 
          
          return strftime($format, $timestamp);
      } else {
          return date($format, $timestamp);
      }
  } 

// ------------------------------------------------------------------------------

  function sb_make_timestamp($string)
  {
      if( empty($string) ) {
          return time();
      } else if( $string instanceof \DateTime ) {
          return $string->getTimestamp();
      } else if( strlen($string) == 14 && ctype_digit($string) ) {
          return mktime( substr($string, 8, 2), substr($string, 10, 2), substr($string, 12, 2),
                         substr($string, 4, 2), substr($string, 6, 2), substr($string, 0, 4));
      } else if( is_numeric($string) ) {
          return (int) $string;
      } else {
          $time = strtotime($string);

          if( $time == -1 || $time === false ) {
              return time();
          }
          
          return $time;
      }
  }
  
// ------------------------------------------------------------------------------

  function gz_file_pack($file_in, $file_out = null)
  {
      if( !is_file($file_in) ) {
          return false;
      }

      $content = file_get_contents($file_in);
    
      if( !isset($file_out) ) {
          $file_out = $file_in . '.gz';
      }
                                    
      if( $gz = gzopen($file_out, 'w9') ) {
          gzwrite($gz, $content);
          gzclose($gz);
          return true;
      } else {
          return false;
      }
  }

// ------------------------------------------------------------------------------
  /**
  * array( 'литр', 'литра', 'литров' )
  * 
  * @param mixed $num
  * @param mixed $words
  */
  function num2word($num, array $words) 
  {
      $num = (int)$num % 100;
      if( $num > 19 ) $num = (int)$num % 10;

      switch( $num ) {
          case 1: return "{$words[0]}";
          
          case 2: 
          case 3: 
          case 4: return "{$words[1]}"; 
          
          default: return "{$words[2]}"; 
      }
  }
  
// ------------------------------------------------------------------------------

  function javascript( $str )
  {
      return "<script type=\"text/javascript\">\n{$str}\n</script>";
  }

// ------------------------------------------------------------------------------

  function html_framing( $body, $tag, $tag_id = null, $tag_class = null, $tab_style = null )
  {
      if( !empty($tag_id) ) {
          $tag_id = " id=\"{$tag_id}\"";
      }

      if( !empty($tag_class) ) {
          $tag_class = " class=\"{$tag_class}\"";
      }

      if( !empty($tab_style) ) {
          $tab_style = " style=\"{$tab_style}\"";
      }
      
      return "<{$tag}{$tag_id}{$tag_class}{$tab_style}>{$body}</{$tag}>";
  }

// ------------------------------------------------------------------------------

  function obj_to_array( $obj )
  {
      if( !is_object($obj) ) return false;
      
      $publics = function($obj) {
          return get_object_vars($obj);
      };
      
      $values = $publics($obj);
      
      foreach($values as $key=>$val) {
          if( is_object($val) ) $values[ $key ] = obj_to_array($val);
      }
      
      return $values;
  }

// ------------------------------------------------------------------------------

  function get_object_public_vars($obj)
  {
      if( !is_object($obj) ) {
          return null;
      }
      
      return get_object_vars($obj);
  }
  
// ------------------------------------------------------------------------------
  /**
  * Получение первого элемента масива
  * Если $key = true, то вернется array с ключем
  * и значением первого элемента
  * 
  * @param array $_array
  * @param bool $key
  */
  function array_get_first( $_array, $key = false ) 
  {
      if( array_count($_array) <= 0 ) return false;

      reset($_array);

      $k       = key($_array);
      $element = $_array[ $k ];
        
      unset($_array);
        
      if( $key === true ) return [ $k => $element ];
        
      return $element;
  }

// ------------------------------------------------------------------------------
  /**
  * Получение ключа первого элемента масива
  * 
  * @param array $_array
  */
  function array_get_first_key( $_array ) 
  {
      if( array_count($_array) <= 0 ) return false;

      reset($_array);
      $k = key($_array);
      
      unset($_array);
        
      return $k;
  }
  
// ------------------------------------------------------------------------------
  /**
  * Получение последнего элемента масива
  * Если $key = true, то вернется array с ключем
  * и значением последнего элемента
  * 
  * @param array $_array
  * @param bool $key
  */
  function array_get_last( $_array, $key = false ) 
  {
      if( array_count($_array) <= 0 ) return false;

      end($_array);

      $k       = key($_array);
      $element = $_array[ $k ];
      
      unset($_array);
        
      if( $key === true ) return [ $k => $element ];
        
      return $element;
  } 

// ------------------------------------------------------------------------------
  /**
  * Получение ключа последнего элемента масива
  * 
  * @param array $_array
  */
  function array_get_last_key( $_array ) 
  {
      if( array_count($_array) <= 0 ) return false;

      end($_array);
      $k = key($_array);

      unset($_array);
      
      return $k;
  } 

// ------------------------------------------------------------------------------
  /**
  * Получение количества записей в масиве
  * Если это не масив, вернется 0
  * 
  * @param array $_
  * @param mixed $mode
  * @return integer
  */
  function array_count($_, $mode = null)
  {
      return (!is_array($_)) ? 0 : count($_, $mode);
  }

// ------------------------------------------------------------------------------
  /**
  * Более быстрый вариант проверки наличия ключа в масиве
  * 
  * @param mixed $key
  * @param array $_array
  * @return bool
  */
  function array_key_isset($key, $_array)
  {
      if( !is_array($_array) ) return false;
      
      return (isset($_array[ $key ] ) || array_key_exists($key, $_array));
  }
  
// ------------------------------------------------------------------------------

  function sb_json_encode($data = false, $options = null)
  {
      if( function_exists('json_encode') ) {
          return json_encode($data, $options);
      }
      
      if( is_string($data) ) return '"' . addslashes($data) . '"';
      if( is_numeric($data)) return $data;
      if( $data === null )   return 'null';
      if( $data === true )   return 'true';
      if( $data === false )  return 'false';

      $assoc = false;
      $i     = 0;
      
      foreach($data as $k=>$v) {
          if( $k !== $i++ ) {
              $assoc = true;
              break;
          }
      }
      
      $res = array();
      
      foreach($data as $k=>$v) {
          $v = sb_json_encode($v);
          if( $assoc ) {
              $k = '"' . addslashes($k) . '"';
              $v = "{$k}:{$v}";
          }
          
          $res[] = $v;
      }
      
      $res = implode(',', $res);
      return ($assoc) ? '{' . $res . '}' : '[' . $res . ']';
  }

// ------------------------------------------------------------------------------

  function sb_json_decode($json, $assoc = null)
  {
      if( function_exists('json_decode') ) {
          return json_decode($json, $assoc);
      }
      
      $comment = false;
      $out     = '$x = ';
      
      for($i = 0; $i < strlen($json); $i++) {
          if( !$comment ) {
              if( ($json[$i] == '{') || ($json[$i] == '[') ) $out .= ' array(';
              else if (($json[$i] == '}') || ($json[$i] == ']')) $out .= ')';
              else if ($json[$i] == ':') $out .= '=>';
              else $out .= $json[$i];
          } else $out .= $json[$i];
          
          if( $json[$i] == '"' && $json[($i-1)]!="\\" ) $comment = !$comment;
      }
      
      eval($out . ';');
      
      return $x;
  }
  
// ------------------------------------------------------------------------------
  /**
  * Получение отрезка времени в unixtime
  * от начала дня до конца за определенную дату.
  * 
  * Форматы даты:
  *   m/d/Y
  *   d.m.Y
  *   Y-m-d
  * 
  * @param string $_date
  * @return mixed
  */
  function make_unixtime_area_of_date($_date)
  {
      if( empty($_date) || !is_string($_date) ) {
          return false;
      }
      
      if( \SB\Match::is_date($_date) || \SB\Match::is_sql_date($_date) ) {
          $_ = array(
              'start' => strtotime("{$_date} 0:00:00"),
              'end'   => strtotime("{$_date} 23:59:59"),
          );
      
          return $_;
      }
      
      return false;
  }

// ------------------------------------------------------------------------------

  function make_days_from_timestamp($timestamp_start, $timestamp_end)
  {
      if( !is_numeric($timestamp_start) || !is_numeric($timestamp_end) ) {
          return false;
      }

      if( $timestamp_start > $timestamp_end ) {
          $tmp             = $timestamp_start;
          $timestamp_start = $timestamp_end;
          $timestamp_end   = $tmp;
          
          unset($tmp);
      }
      
      $first_day = [
          'start' => strtotime( date('Y-m-d', $timestamp_start) . ' 00:00:00' ),
          'end'   => strtotime( date('Y-m-d', $timestamp_start) . ' 23:59:59' ),
      ];
      
      $last_day = [
          'start' => strtotime( date('Y-m-d', $timestamp_end) . ' 00:00:00' ),
          'end'   => strtotime( date('Y-m-d', $timestamp_end) . ' 23:59:59' ),
      ];
      
      $_    = round( ($last_day['end'] - $first_day['start']) / 86400 );
      $days = [];
      
      for($i = 0; $i < $_; $i++) {
          $days[$i] = [
              'date'  => date('Y-m-d', $first_day['start'] + ($i * 86400)),
              'start' => ($i == 0) ? $timestamp_start : $first_day['start'] + ($i * 86400),
              'end'   => ($i == ($_ - 1)) ? $timestamp_end : $first_day['end'] + ($i * 86400),
          ];
      }
      
      return $days;
  }

// ------------------------------------------------------------------------------
  /**
  * Преобразование количества секунд
  * в Человеко-понятный вид
  * 
  * @param int $sec
  * @param mixed $rus
  */
  function sec2time($sec, $rus = true)
  {
      if( !is_numeric($sec) ) {
          return false;
      }
      
      $sec  = (int)$sec;
      $days = intval($sec / 86400);

      $d  = ($rus) ? 'д' : 'd';
      
      $sec_balance = $sec - ($days * 86400);

      if( $sec_balance <= 0 ) {
          $_  = ($days > 0) ? "{$days}{$d} " : '';
          return $_ . '0:00:00';
      }
      
      $hours = intval($sec_balance / 3600);
      $sec_balance = $sec_balance - ($hours * 3600);

      if( $sec_balance <= 0 ) {
          $_  = ($days > 0) ? "{$days}{$d} " : '';
          $_ .= ($hours > 0) ? "{$hours}:" : '0:';
          return  $_ . "00:00";
      }

      $mins = intval($sec_balance / 60);
      $sec_balance = $sec_balance - ($mins * 60);
      
      if( $sec_balance <= 0 ) {
          $_  = ($days > 0) ? "{$days}{$d} " : '';
          $_ .= ($hours > 0) ? "{$hours}:" : '0:';
          $_ .= ($mins > 0) ? (($mins < 10) ? "0{$mins}:" : "{$mins}:") : '00:';
          
          return  $_ . "00";
      }
      
      $_  = ($days > 0) ? "{$days}{$d} " : '';
      $_ .= ($hours > 0) ? "{$hours}:" : '0:';
      $_ .= ($mins > 0) ? (($mins < 10) ? "0{$mins}:" : "{$mins}:") : '00:';

      return  $_ . (($sec_balance < 10) ? "0{$sec_balance}" : "{$sec_balance}");
  }
  
// ------------------------------------------------------------------------------

  function serialize_pack_64( $mix ) 
  {
      return base64_encode( gzcompress( serialize( $mix ), 9 ) );
  }
  
// ------------------------------------------------------------------------------

  function serialize_unpack_64( $packed ) 
  {
      if( ($_ = base64_decode($packed)) == '' ) return null;
      if( ($_ = gzuncompress($_)) == '' ) return null;
      
      unset($packed);
      
      return unserialize($_);
  }
  
// ------------------------------------------------------------------------------
  /**
  * Принудительная очистка памяти
  */
  function memory_clear()
  {
      if( function_exists('gc_collect_cycles') ) {
          gc_enable();
          $_ = gc_collect_cycles();
          gc_disable();
          
          return (int)$_;
      }
      
      return 0;
  }
  
// ------------------------------------------------------------------------------

  function get_mem_use( $max = true, $round_to = 2 )
  {
      if( function_exists('memory_get_peak_usage') && $max ) {
          return round( (memory_get_peak_usage(true))/1048576, $round_to );
      } else if( function_exists('memory_get_usage') && !$max ) {
          return round( memory_get_usage()/1048576, $round_to );
      } 
      
      return 0;
  }
  
// ------------------------------------------------------------------------------

  function generate_file_name($extension)
  {
      return time() . substr( md5(microtime()), 0, rand(5, 12) ) . $extension;
  } 

// ------------------------------------------------------------------------------

  function get_disabled_functions()
  {
      $_ = explode(',', ini_get('disable_functions'));
      $PHP_DISABLED_FUNCTIONS = array();

      foreach($_ as $key=>$val) {
          $func = trim($val);
          if( !empty($func) ) {
              $PHP_DISABLED_FUNCTIONS[] = $val;
          }
      }
      
      unset($_, $key, $val, $func);
      
      return $PHP_DISABLED_FUNCTIONS;
  }
  
// ------------------------------------------------------------------------------

  function js_minify($js_str)
  {
      if( empty($js_str) || !is_string($js_str) ) {
          return false;
      }
      
//      $js_str = preg_replace('/\/\*(.*?)\*\//s', '', $js_str);
//      $js_str = str_replace( array("\n","\r"), '', $js_str);
//      $js_str = preg_replace('/[\s]+/s', ' ', $js_str);
//      $js_str = preg_replace('/{\s+/s', '{', $js_str);
//      $js_str = preg_replace('/\s+:/s', ':', $js_str);
      return $js_str;
  }

// ------------------------------------------------------------------------------

  function truncate($string, $length = 80, $etc = '...', $break_words = false, $middle = false)
  {
      if ($length == 0) {
          return '';
      }

      if( function_exists('mb_substr') ) {
          if( mb_strlen($string, 'UTF-8') > $length ) {
              $length -= min($length, mb_strlen($etc, 'UTF-8'));

              if( !$break_words && !$middle ) {
                  $string = preg_replace('/\s+?(\S+)?$/u', '', mb_substr($string, 0, $length + 1, 'UTF-8'));
              }
             
              if( !$middle ) {
                  return mb_substr($string, 0, $length, 'UTF-8') . $etc;
              }
    
              return mb_substr($string, 0, $length / 2, 'UTF-8') . $etc . mb_substr($string, - $length / 2, $length, 'UTF-8');
          }
    
         return $string;
     }

     // no MBString fallback
     if( isset($string[ $length ]) ) {
         $length -= min($length, strlen($etc));
         
         if( !$break_words && !$middle ) {
             $string = preg_replace('/\s+?(\S+)?$/', '', substr($string, 0, $length + 1));
         }
         if( !$middle ) {
             return substr($string, 0, $length) . $etc;
         }
   
         return substr($string, 0, $length / 2) . $etc . substr($string, - $length / 2);
     }

     return $string;
  } 
  
// ------------------------------------------------------------------------------
  /**
  * Pagination function
  * 
  * @param integer $current_page
  * @param integer $pages_count
  * @param string $prefix
  * @param string $url_suffix
  * 
  * @return string
  */
  function pagination($current_page, $pages_count, $prefix, $url_suffix = '.html')
  {
      if( !is_numeric($current_page) || !is_numeric($pages_count) || empty($prefix) ) {
          return '';
      }
      
      if( $pages_count > 1 ) {
          $_           = '<div id="pagination" class="pagination"><ul>';
          $base_url    = BASE_URL;
          $I18n        = \SB\I18n::Instance();
          $more_after  = false; 
          $more_before = false;
          
          if($current_page > 1) {
              $_ .= "<li><a href=\"{$base_url}/{$prefix}-1{$url_suffix}\">&laquo; " . sb_ucfirst( $I18n->first_page ) ."</a></li>";
              $_ .= "<li><a href=\"{$base_url}/{$prefix}-" . ($current_page - 1) . "{$url_suffix}\" title=\"" . sb_ucfirst( $I18n->previous_page ) . "\">&laquo;</a></li>";
          }
            
          for($j = 1; $j <= $pages_count; $j++) {
              if( $j >= ($current_page - 3) && $j <= ($current_page + 3) ) {
                  if( $current_page == $j ) {
                      $_ .= "<li><a class=\"active\">{$j}</a></li>";
                  } else {
                      $_ .= "<li><a href=\"{$base_url}/{$prefix}-{$j}{$url_suffix}\">{$j}</a></li>";
                  }
              } else if( $j < ($current_page - 3) && $more_after == false ) {
                  $more_after = true;
                  $_ .= "<li>&nbsp;...&nbsp;</li>";
              } else if( $j > ($current_page + 3) && $more_before == false ) {
                  $more_before = true;
                  $_ .= "<li>&nbsp;...&nbsp;</li>";
              }
            
          }
            
          if($current_page < $pages_count) {
              $_ .= "<li><a href=\"{$base_url}/{$prefix}-" . ($current_page + 1) . "{$url_suffix}\" title=\"" . sb_ucfirst( $I18n->next_page ) . "\">&raquo;</a></li>";
              $_ .= "<li><a href=\"{$base_url}/{$prefix}-{$pages_count}{$url_suffix}\">" . sb_ucfirst( $I18n->last_page ) . " &raquo;</a></li>";
          }
          
          $_ .= "</ul></div>";
          
          return $_;
      }
      
      return '';
  }
// ------------------------------------------------------------------------------
  /**
  * Write packed content to file
  * 
  * @param string $filename
  * @param string $content
  * @param integer $compression_level
  */
  function file_put_gz_content($filename, $content, $compression_level = 5)
  {
      if( !empty($filename) && is_scalar($filename) && is_scalar($content) ) {
          return file_put_contents( $filename, gzcompress((string)$content, (int)$compression_level) );
      }
      
      return false;
  }
// ------------------------------------------------------------------------------
  /**
  * Read and unpack temp file
  * 
  * @param string $filename
  */
  function file_get_gz_content($filename)
  {
      if( !empty($filename) && is_file($filename) ) {
          $_ = file_get_contents($filename);
          return ( $_ != '' ) ? gzuncompress( $_ ) : '';
      }
      
      return false;
  }
  
  function sb_microtime()
  {
      return round( microtime(true) * 1000 );
  }
  
  function get_file_extension($filename)
  {
      return substr(
          strrchr($filename, '.'), 
          1
      );
  }
  
  function is_image($filepath)
  {
      if( is_file($filepath) && function_exists('getimagesize') ) {
          $img = getimagesize($filepath);
          if( !empty($img['mime']) ) {
              return true;
          }
      }
      
      return false;
  }
  
  function get_image_extension($filepath)
  {
      if( is_file($filepath) && function_exists('getimagesize') ) {
          $img = getimagesize($filepath);

          switch($img['mime']) {
              case 'image/jpeg': return 'jpg';
              case 'image/gif':  return 'gif';
              case 'image/png':  return 'png';
              case 'image/x-ms-bmp': return 'bmp';
              default: return '';
          }
      }
      
      return '';
  }
}
