<?php namespace SB\Cache {
/**
* @author        Tishchenko Alexander
* @copyright     Copyright (c) 2014 All rights to Tishchenko A.
* @package       Second-Breath PHP5 framework
* @filesource    Memcache.php
* @name          \SB\Cache\Memcache
*/

    class Memcache extends \SB\Cache\Dummy 
    {
        private static $_instance;
        protected $api;
        protected $id;
// -----------------------------------------------------------------------------
        public function __construct()
        {
            if( !class_exists('\Memcache') ) {
                system_error('Error! Memcache php module not installed');
            }
            
            $this->api = new \Memcache();
            if( !$this->api->connect('localhost') ) {
                system_error('Error! Memcache connected fail');
            }
            
            $this->id = \SB\Session::Instance()->id( true );
        }
// -----------------------------------------------------------------------------
        public function get( $name )
        {
            if( !empty($name) && isset($name) ) {
                return $this->api->get($this->id . '_' . $name);
            }
            
            return null;
        }
// -----------------------------------------------------------------------------
        public function set($name, $value, $seconds = 60)
        {
            if( $value === null && isset($name) ) {
                return $this->api->delete($this->id . '_' . $name);
            } else if( $value !== null && is_string($name) && \SB\Match::is_varible_name($name) ) {
                return $this->api->set($this->id . '_' . $name, $value, null, $seconds);
            }
            
            return false;
        }
// -----------------------------------------------------------------------------
        public function clear()
        {
            $this->api->flush();
            
            return $this;
        }
// -----------------------------------------------------------------------------
        public function get_info( $id = null )
        {
            return $this->api->getStats();
        }
// -----------------------------------------------------------------------------
        public function get_version()
        {
            return $this->api->getVersion();
        }
// -----------------------------------------------------------------------------
        public function __set($name, $value)
        {
            $this->set($name, $value);
        }
// -----------------------------------------------------------------------------
        public function __get($name)
        {
            return $this->get($name);
        }
// -----------------------------------------------------------------------------
        public function __isset($name)
        {
            if( ($_ = $this->get($name)) !== null ) {
                return true;
            }
            
            return false;
        }
// -----------------------------------------------------------------------------
        public function __unset($name)
        {
            $this->api->delete($this->id . '_' . $name);
        }
// -----------------------------------------------------------------------------
        public function __destruct()
        {
            $this->api->close();
        }
// -----------------------------------------------------------------------------
        /**
        * Return instance of class
        */
        public static function Instance()
        {
            if( null === self::$_instance ) {
                self::$_instance = new self();
            }

            return self::$_instance;
        }
    }
}