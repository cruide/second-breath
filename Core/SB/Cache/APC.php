<?php namespace SB\Cache {
/**
* @author        Tishchenko Alexander
* @copyright     Copyright (c) 2014 All rights to Tishchenko A.
* @package       Second-Breath PHP5 framework
* @filesource    APC.php
* @name          \SB\Cache\APC
*/

    class APC extends \SB\Cache\Dummy 
    {
        private static $_instance;
        
        protected $id;
// -----------------------------------------------------------------------------
        public function __construct()
        {
            if( !function_exists('apc_store') ) {
                system_error('Error! APC php module not installed');
            }
            
            $this->id = \SB\Session::Instance()->id( true );
        }
// -----------------------------------------------------------------------------
        public function get( $name )
        {
            if( !empty($name) && is_string($name) && \SB\Match::is_varible_name($name) && isset($name) ) {
                return apc_fetch($this->id . '_' . $name);
            }
            
            return null;
        }
// -----------------------------------------------------------------------------
        public function set($name, $value, $seconds = 60)
        {
            if( $value === null && \SB\Match::is_varible_name($name) && isset($name) ) {
                return apc_delete($this->id . '_' . $name);
            } else if( $value !== null && \SB\Match::is_varible_name($name) ) {
                return apc_store($this->id . '_' . $name, $value, $seconds);
            }
            
            return false;
        }
// -----------------------------------------------------------------------------
        public function clear()
        {
            apc_clear_cache();
            
            return $this;
        }
// -----------------------------------------------------------------------------
        public function get_info($id = null)
        {
            return apc_cache_info();
        }
// -----------------------------------------------------------------------------
        public function __set($name, $value)
        {
            $this->set($name, $value);
        }
// -----------------------------------------------------------------------------
        public function __get($name)
        {
            return $this->get($name);
        }
// -----------------------------------------------------------------------------
        public function __isset($name)
        {
            return apc_exists($this->id . '_' . $name);
        }
// -----------------------------------------------------------------------------
        public function __unset($name)
        {
            apc_delete($this->id . '_' . $name);
        }
// -----------------------------------------------------------------------------
        /**
        * Return instance of class
        */
        public static function Instance()
        {
            if( null === self::$_instance ) {
                self::$_instance = new self();
            }

            return self::$_instance;
        }
    }
}