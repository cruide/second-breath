<?php namespace SB\Cache {
/**
* @author        Tishchenko Alexander
* @copyright     Copyright (c) 2014 All rights to Tishchenko A.
* @package       Second-Breath PHP5 framework
* @filesource    xCache.php
* @name          \SB\Cache\xCache
*/

    class xCache extends \SB\Cache\Dummy
    {
        private static $_instance;
        
        protected $id;
        protected $var_size;
// -----------------------------------------------------------------------------
        public function __construct()
        {
            if( !function_exists('xcache_info') ) {
                system_error('xCache Error! PHP module not installed');
            }
            
            $this->var_size = @ini_get('xcache.var_size');
            $this->id       = \SB\Session::Instance()->id( true );
        }
// -----------------------------------------------------------------------------
        public function enabled()
        {
            return (!empty( $this->var_size ));
        }
// -----------------------------------------------------------------------------
        public function get( $name )
        {
            if( !empty($name) && is_string($name) && \SB\Match::is_varible_name($name) && isset($name) ) {
                return xcache_get( $this->id . '_' . $name );
            }
            
            return null;
        }
// -----------------------------------------------------------------------------
        public function set($name, $value, $seconds = 60)
        {
            if( $value === null && \SB\Match::is_varible_name($name) && isset($name) ) {
                return xcache_unset($this->id . '_' . $name);
            } else if( $value !== null && \SB\Match::is_varible_name($name) ) {
                return xcache_set($this->id . '_' . $name, $value, $seconds);
            }
            
            return false;
        }
// -----------------------------------------------------------------------------
        public function clear()
        {
            $count = xcache_count( XC_TYPE_VAR );

            for($i = 0; $i < $count; $i++) {
                $values = $this->get_list( $i );

                if( array_count($values['cache_list']) > 0 ) {
                    foreach($values['cache_list'] as $key=>$val) {
                        if( preg_match("/^{$this->id}\_/i", $val['name']) ) {
                            xcache_unset( $val['name'] );
                        }
                    }
                    
                    unset($key, $val);
                }
            }
            
            return $this;
        }
// -----------------------------------------------------------------------------
        public function get_list( $id = null )
        {
            if( is_int($id) ) {
                return xcache_list( XC_TYPE_VAR, $id);
            }
            
            $count = xcache_count( XC_TYPE_VAR );
            $_     = array();

            for($i = 0; $i < $count; $i++) {
                $_[ $i ] = xcache_list( XC_TYPE_VAR, $i);
            }

            return $_;
        }
// -----------------------------------------------------------------------------
        public function get_info( $id = null )
        {
            if( is_int($id) ) {
                return xcache_info( XC_TYPE_VAR, $id);
            }

            $count = xcache_count( XC_TYPE_VAR );
            $_     = array();

            for($i = 0; $i < $count; $i++) {
                $_[ $i ] = xcache_info( XC_TYPE_VAR, $i);
            }

            return $_;
        }
// -----------------------------------------------------------------------------
        public function __set($name, $value)
        {
            $this->set($name, $value);
        }
// -----------------------------------------------------------------------------
        public function __get($name)
        {
            return $this->get($name);
        }
// -----------------------------------------------------------------------------
        public function __isset($name)
        {
            return ($this->enabled()) ? xcache_isset($this->id . '_' . $name) : false;
        }
// -----------------------------------------------------------------------------
        public function __unset($name)
        {
            if( $this->enabled() ) {
                xcache_unset($this->id . '_' . $name);
            }
        }
// -----------------------------------------------------------------------------
        /**
        * Return instance of class
        */
        public static function Instance()
        {
            if( null === self::$_instance ) {
                self::$_instance = new self();
            }

            return self::$_instance;
        }
    }
}