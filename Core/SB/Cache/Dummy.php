<?php namespace SB\Cache {

    class Dummy
    {
        private static $_instance;
// -----------------------------------------------------------------------------
        public function __set($name, $value) { }
// -----------------------------------------------------------------------------
        public function __get($name)
        {
            return null;
        }
// -----------------------------------------------------------------------------
        public function __isset($name)
        {
            return false;
        }
// -----------------------------------------------------------------------------
        public function __unset($name) { }
// -----------------------------------------------------------------------------
        public function get($name) 
        {
            return null;
        }
// -----------------------------------------------------------------------------
        public function set($name, $value, $seconds = 60) { }
// -----------------------------------------------------------------------------
        public function enabled()
        {
            return true;
        }
// -----------------------------------------------------------------------------
        public function clear() 
        {
            return $this;
        }
// -----------------------------------------------------------------------------
        public function get_list( $id = null )
        {
            return array();
        }
// -----------------------------------------------------------------------------
        public function get_info( $id = null )
        {
            return array();
        }
// -----------------------------------------------------------------------------
        public function get_version() 
        {
            return false;
        }
// -----------------------------------------------------------------------------
        public function __destruct() { }
// -----------------------------------------------------------------------------
        /**
        * Return instance of class
        */
        public static function Instance()
        {
            if( null === self::$_instance ) {
                self::$_instance = new self();
            }

            return self::$_instance;
        }
    }

}