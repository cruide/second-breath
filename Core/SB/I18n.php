<?php namespace SB {
/**
* @author        Tishchenko Alexander
* @copyright     Copyright (c) 2014 All rights to Tishchenko A.
* @package		 Second-Breath PHP5 framework
* @filesource	 Language.php
* @name			 \SB\Language
*/

  final class I18n_Element extends \SB\stdObject 
  {
      public function __get($value)
      {
          return "%{$value}%";
      }
  }

  final class I18n_Elements extends \SB\stdObject 
  {
      public function __get($value)
      {
          return new I18n_Element();
      }
  }
  
  final class I18n extends \stdClass
  {
      private $_default, $_current, $_strings;
      private $_current_code;
      protected static $_instance;
      protected $languages;
      
      const SB_DEFAULT_LOCALE = 'EN';
// -------------------------------------------------------------------------------------
      public static function Instance()
      {
          if( null === self::$_instance ) {
              self::$_instance = new self();
          }
 
          return self::$_instance;
      }
// -------------------------------------------------------------------------------------
      public function __construct()
      {
      	  global $_COOKIE;
      	  
          $config          = load_ini('config')->application;
          $this->languages = [];

          if( is_dir(LANGUAGES_DIR) ) {
              foreach(@glob( LANGUAGES_DIR . DIR_SEP . '*.php' ) as $key=>$val) {
                  $tmp  = include( $val );
                  $code = str_replace( [LANGUAGES_DIR . DIR_SEP, '.php'], '', $val );
                  
                  if( array_count($tmp) >= 2 && isset($tmp['#LANGUAGE']) && isset($tmp['#CODE']) ) {
                      if( $code != $tmp['#CODE'] ) {
                          throw new \SB\Exception\I18n(
                              "Incorrect locale code into {$val}."
                          );
                      } else {
                          $this->languages[ $tmp['#CODE'] ] = $tmp['#LANGUAGE'];
                      }
                  }
              }
              
              unset($tmp, $key, $val);
          }

          if( isset($config['language']) ) {
                $this->_default = ( isset($this->languages[ $config['language'] ]) ) 
                                 ? $config['language'] 
                                 : self::SB_DEFAULT_LOCALE;
          } else {
              $this->_default = self::SB_DEFAULT_LOCALE;
          }

          if( !is_file( LANGUAGES_DIR . DIR_SEP . "{$this->_default}.php" ) ) {
              throw new \SB\Exception\I18n(
                "Default {$this->_default}.php language file not fount"
              );
          }
          
          $this->_strings          = new I18n_Element();
          $this->_strings->Modules = new I18n_Elements();
          $this->_strings->Plugins = new I18n_Elements();

          if( array_key_isset('SBLOCALE', $_COOKIE) ) {
              $SB_LOCALE = $_COOKIE['SBLOCALE'];
          } else {
              if( isset($config['language']) ) {
                  $config['language'] = strtoupper( $config['language'] );
              }
              
              if( isset($this->languages[ $config['language'] ]) ) {
                  $SB_LOCALE = $config['language'];
              } else {
                  $SB_LOCALE = self::SB_DEFAULT_LOCALE;
              }
          }

          if( !empty($SB_LOCALE) && isset($this->languages[ $SB_LOCALE ]) ) {
              $this->_current = $SB_LOCALE;
          } else {
              $this->_current = $this->_default;
          }
          
          $this->_run();
      }
// ---------------------------------------------------------------------------------
      public function is_exists( $code )
      {
          return (!empty($code) && is_string($code)) ? isset($this->_strings->$code) : false;
      }
// ---------------------------------------------------------------------------------
      public function func_get_current_locale_name()
      {
          return $this->_current;
      }
// ---------------------------------------------------------------------------------
      public function func_get_current_locale()
      {
          return $this->languages[ $this->_current ];
      }
// ---------------------------------------------------------------------------------
      public function func_set_locale($code)
      {
      	  if( array_key_isset($code, $this->languages) ) {
              setcookie('SBLOCALE', sb_strtoupper($code), time() + 86400, '/');
		  }
      }
// ---------------------------------------------------------------------------------
      public function func_add_string($str_name, $str)
      {
          if( !empty($str_name) and is_string($str_name) 
                 and !empty($str) and is_string($str) )
          {
              $this->_strings->$str_name = $str;
          }
          
          return $this;
      }
// ---------------------------------------------------------------------------------
      public function get($name)
      {
          if( isset($this->_strings->$name) ) {
              return $this->_strings->$name;
          }
          
          return "%{$name}%";
      }
// ---------------------------------------------------------------------------------
      public function __get($name)
      {
          return $this->get($name);
      }
// ---------------------------------------------------------------------------------
      public function __isset($name)
      {
          return isset($this->_strings->$name);
      }
// ---------------------------------------------------------------------------------
      private function _run()
      {
          $this->_load_strings( $this->_default );

          $modules = \SB\Cloud::Instance()->modules;
          foreach($modules as $key=>$val) {
              $this->_load_module_strings($val, $this->_default );
          }
          
          if( $this->_current != $this->_default ) {
              $this->_load_strings( $this->_current );

              foreach($modules as $key=>$val) {
                  $this->_load_module_strings($val, $this->_current );
              }
          }
          
          \SB\Native::assign_global('current_lang', $this->_current);
          \SB\Native::assign_global('default_lang', $this->_default);
          \SB\Native::assign_global('languages'   , $this->languages);
          \SB\Native::assign_global('I18n'        , $this->_strings);
      }
// -------------------------------------------------------------------------------------
      private function _load_strings($code)
      {
      	  if( empty($code) || strlen($code) !== 2 || !array_key_isset($code, $this->languages) ) {
			  throw new \SB\Exception\I18n("Incorrect locale code: {$code}");
      	  }

          $_file = LANGUAGES_DIR . DIR_SEP . $code . '.php';
          
          if( is_file($_file) ) {
              $lng = include( $_file );

              if( array_count($lng) > 2 ) {
                  if( !isset($lng['#CODE']) || $lng['#CODE'] != $code ) {
                      throw new \SB\Exception\I18n(
                          "Incorrect locale code into {$_file}."
                      );
                  }

                  unset($lng['#LANGUAGE'], $lng['#CODE']);
                  
                  foreach($lng as $key=>$val) {
                      if( \SB\Match::is_varible_name($key) && $val != 'Modules' && $val != 'Plugins' ) {
                          $this->_strings->$key = $val;
                      }
                  }
              }
              
              $lng = $_file = null;
          }
      }
// -------------------------------------------------------------------------------------
      private function _load_module_strings($module, $code = 'EN')
      {
          if( empty($module) || !is_module_exists($module) ) {
              throw new \SB\Exception\I18n("Incorrect module: {$module}");
          }

      	  if( empty($code) || strlen($code) !== 2 || !array_key_isset($code, $this->languages) ) {
			  throw new \SB\Exception\I18n("Incorrect locale code: {$code}");
      	  }

          $_module_file = correct_directory_path( MODULES_DIR . DIR_SEP . $module . DIR_SEP . 'I18n', true ) . $code . '.php';

          if( is_file($_module_file) ) {
              $lng  = include($_module_file);
              $i18n = new I18n_Element();
              
              if( array_count($lng) > 2 ) {
                  if( !isset($lng['#CODE']) || $lng['#CODE'] != $code ) {
                      throw new \SB\Exception\I18n(
                          "Incorrect locale code into {$_module_file}."
                      );
                  }
                  
                  unset($lng['#LANGUAGE'], $lng['#CODE']);
                  
                  foreach($lng as $key=>$val) {
                      if( \SB\Match::is_varible_name($key) && $val != 'Modules' && $val != 'Plugins' ) {
                          $i18n->$key = $val;
                      }
                  }
              }
              
              $this->_strings->Modules->$module = $i18n;
              
              unset($i18n, $lng, $_action_file);
          }
      }
// -------------------------------------------------------------------------------------
      /**
      * Verified by two-letter locale value
      * 
      * @param string $locale_code
      */
      public function func_is_locale_exists($locale_code)
      {
          return array_key_isset($locale_code, $this->languages);
      }
// -------------------------------------------------------------------------------------
      public function load_plugin_strings($plugin_name)
      {
          if( !is_plugin_exists($plugin_name) ) {
              return false;
          }

      	  $plugin_i18n_path = correct_directory_path(
      	      PLUGINS_DIR . DIR_SEP . $plugin_name . DIR_SEP . 'I18n',
      	      true
      	  );

          $plugin_i18n_default = $plugin_i18n_path . $this->_default . '.php';
      	  $plugin_i18n_file    = $plugin_i18n_path . $this->_current . '.php';

          $i18n = new I18n_Element();
          
          /**
          * Загружаем дефолтный файл
          */
          if( is_file($plugin_i18n_default) ) {
              $lng  = include($plugin_i18n_default);
              
              if( array_count($lng) > 2 ) {
                  if( !isset($lng['#CODE']) || $lng['#CODE'] != $this->_default ) {
                      throw new \SB\Exception\I18n(
                          "Incorrect locale code into {$plugin_i18n_default}."
                      );
                  }

                  unset($lng['#LANGUAGE'], $lng['#CODE']);
                  
                  foreach($lng as $key=>$val) {
                      if( \SB\Match::is_varible_name($key) && $val != 'Modules' && $val != 'Plugins' ) {
                          $i18n->$key = $val;
                      }
                  }
              }
              
              unset($lng, $val, $key);
          }

          /**
          * Загружаем файл текущего языка
          */
      	  if( is_file($plugin_i18n_file) ) {
              $lng = include($plugin_i18n_file);
              
              if( array_count($lng) > 2 ) {
                  if( !isset($lng['#CODE']) || $lng['#CODE'] != $this->_current ) {
                      throw new \SB\Exception\I18n(
                          "Incorrect locale code into {$plugin_i18n_file}."
                      );
                  }

                  unset($lng['#LANGUAGE'], $lng['#CODE']);
                  
                  foreach($lng as $key=>$val) {
                      if( \SB\Match::is_varible_name($key) && $val != 'Modules' && $val != 'Plugins' ) {
                          $i18n->$key = $val;
                      }
                  }
              }
              
              unset($lng, $val, $key);
      	  }
      	  
          $this->_strings->Plugins->$plugin_name = $i18n;
          
          unset($i18n, $key, $val);
           
          return true;
      }
// -------------------------------------------------------------------------------------
      public function func_getLanguagesList()
      {
          return $this->languages;
      }
  }
}