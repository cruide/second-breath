<?php namespace SB {
/**
* @author        Tishchenko Alexander
* @copyright     Copyright (c) 2014 All rights to Tishchenko A.
* @package		 Second-Breath PHP5 framework
* @filesource	 Router.php
* @name			 \SB\Router
*/

    class Router extends \stdClass
    {
	    private $_request, $_request_type, $_ajax, $_uri, $module, $controller, $method, $_is_plugin;
	    private static $regexps;
	    private static $_instance;
// -------------------------------------------------------------------------------------
	    public function __construct()
	    {
		    global $_REQUEST_URI;

            $superfluous      = array('.html', '.htm', '.php5', '.php', '.php3', '.shtml', '.phtml', '.dhtml', '.xhtml', '.inc', '.cgi', '.pl','.xml', '.js');
            $this->_is_plugin = false;
		    $this->_uri       = preg_replace( "#/+#s", '/', $_REQUEST_URI );
		    $this->_uri       = preg_replace( array("#/$#s","#^/+#"), '', $this->_uri );
		    $this->_uri       = $this->_request = str_replace($superfluous, '', $this->_uri);

		    if( array_count(self::$regexps) > 0 ) {
			    foreach(self::$regexps as $key=>$val) {
				    if( preg_match('#'.$val['alias'].'#is', $this->_uri, $_null) ) {
					    $this->_uri = preg_replace('#'.$val['alias'].'#is', $val['replace'], $this->_uri);
					    break;
				    }
			    }
		    }

		    if( preg_match("%[^a-z0-9\_\/\-\.]%isu", $this->_uri) ) {
			    system_error("Abnormal request: {$this->_uri}", 500);
		    }

		    if( empty($this->_uri) ) {
			    $this->module     = DEFAULT_MODULE;
			    $this->controller = DEFAULT_CONTROLLER;
			    $this->method     = 'default';
			    return;
		    }

		    $parsed = explode('/', $this->_uri);

		    /* Get module name */
		    if( !empty($parsed[0]) ) {
			    $_module = ucfirst( strtolower($parsed[0]) );
		    } else {
			    $_module = null;
		    }

		    /* Get controller name */
		    if( !empty($parsed[1]) ) {
			    $_controller = ucfirst( strtolower($parsed[1]) );
		    } else {
			    $_controller = null;
		    }

		    /* Get method name */
		    if( !empty($parsed[2]) ) {
			    $_method = $parsed[2];
		    } else {
			    $_method = null;
		    }

		    if( is_module_exists($_module) ) {
			    if( is_controller_exists($_module, $_controller) ) {
				    $this->module     = $_module;
				    $this->controller = $_controller;

				    if( !empty($_method) ) {
					    if( !preg_match("/[^a-z0-9_]/is", $_method) ) {
						    $this->method = $_method;
					    } else {
						    $this->method = '__unknown__method__name__';
					    }
				    } else {
					    $this->method = 'default';
				    }
			    } else if( empty($_controller) && is_controller_exists($_module, DEFAULT_CONTROLLER) ) {
				    $this->module     = $_module;
				    $this->controller = DEFAULT_CONTROLLER;
				    $this->method     = 'default';
				    return;
			    } else {
				    show_error_404();
			    }
		    } else if( $_module == 'Plugins' && is_plugin_exists($_controller) ) {
			    $this->module     = 'Plugins';
			    $this->controller = $_controller;
			    $this->method     = 'content';
                $this->_is_plugin = true;
                
			    array_shift($parsed);
			    array_shift($parsed);

			    $this->_set_get_params($parsed);
			    return;
		    } else if( $_module == 'Phpinfo' ) {
                $cfg = load_ini('config')->application;
                if( isset($cfg['develop_mode']) && $cfg['develop_mode'] == true ) {
                    phpinfo();
                    exit();
                }
                
                show_error_404();
                
            } else if( $_module == 'Coreinfo' ) {
                if( $_controller == 'Json' ) {
                    $_          = new \stdClass();
                    $_->name    = CORE_NAME;
                    $_->version = CORE_VERSION;
                    $_->status  = CORE_VERSION_NAME;
                    $_->author  = 'Tishchenko Alexander';
                    $_->image   = BASE_URL . '/coreimage';
                    
                    if( !headers_sent() ) {
                        header('Cache-Control: no-cache, must-revalidate');
                        header('Expires: ' . date('r', time() - 86400));
                        header('Content-type: application/json; charset=utf-8');
                    }
                    
                    exit( sb_json_encode($_) );
                } else {
                    header('Content-type: text/xml; charset=utf-8');
                    
                    exit( 
                        "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<coreinfo>\n\t<name><![CDATA[" . CORE_NAME . 
                        "]]></name>\n\t<version><![CDATA[" . CORE_VERSION . "]]></version>\n" . 
                        "\t<status><![CDATA[" . CORE_VERSION_NAME . "]]></status>\n" .
                        "\t<author><![CDATA[Tishchenko Alexander]]></author>\n" .
                        "\t<image><![CDATA[" . BASE_URL . "/coreimage]]></image>\n" .
                        "</coreinfo>\n" 
                    );
                }
            } else if( $_module == 'Coreimage' ) {
                $this->show_image_label();
		    } else {
			    show_error_404();
		    }

		    array_shift($parsed);
		    array_shift($parsed);
		    array_shift($parsed);

		    $this->_set_get_params($parsed);
	    }
// -------------------------------------------------------------------------------------
        public function is_plugin()
        {
            return $this->_is_plugin;
        }
// -------------------------------------------------------------------------------------
        public function get_original_uri()
        {
		    return $this->_request;
        }

// -------------------------------------------------------------------------------------
	    public function get_plugin_name()
	    {
		    return $this->controller;
	    }
// -------------------------------------------------------------------------------------
	    public function get_module_name()
	    {
		    return $this->module;
	    }
// -------------------------------------------------------------------------------------
	    public function get_controller_name()
	    {
		    return $this->controller;
	    }
// -------------------------------------------------------------------------------------
	    public function get_method_name()
	    {
		    return $this->method;
	    }
// -------------------------------------------------------------------------------------
	    private function _set_get_params($_array = array())
	    {
		    global $_GET;

		    if( array_count($_array) > 0 and is_even(count($_array)) ) {
			    $_array_count = floor( count($_array) / 2 );

			    for($i=0; $i/2 < $_array_count; $i+=2) {
				    $_GET[ $_array[$i] ] = ( !empty($_array[$i+1]) ) ? $_array[$i+1] : '';
			    }
		    }
	    }
// -------------------------------------------------------------------------------------
        public function show_image_label()
        {
            if( !headers_sent() ) {
                $now               = mktime(date('H'), date('i'), date('s'), date('m'), date('d'), date('Y'));
                $expires           = mktime(date('H'), date('i'), date('s') + 30 , date('m'), date('d'), date('Y'));
                $expires_gmt       = gmdate('D, d M Y H:i:s', $expires) . ' GMT';
                $last_modified_gmt = gmdate('D, d M Y H:i:s', $now) . ' GMT';

                header('Content-type: image/png');
                header('Expires: ' . $expires_gmt);
                header('last-modified: ' . $last_modified_gmt);
                header('Cache-Control: no-store, no-cache, must-revalidate');

                echo base64_decode( 
                    file_get_contents(
                        SB_PATH . DIR_SEP . 'framework.image'
                    )
                );
            }
            
            exit();
        }

// -------------------------------------------------------------------------------------
	    public static function Instance($recreate = false)
	    {
		    if( null === self::$_instance or $recreate === true ) {
			    self::$_instance = new self();
		    }

		    return self::$_instance;
	    }
// -------------------------------------------------------------------------------------

        public static function Route($regexp, $replace)
        {
            if( self::$regexps === null ) {
        	    self::$regexps = array();
		    }

            if( !empty($replace) and !empty($regexp) ) {
                self::$regexps[] = array (
                    'alias'   => (string)$regexp,
                    'replace' => (string)$replace
                );
            }
        }
    }
}