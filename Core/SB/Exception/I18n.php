<?php namespace SB\Exception {
/**
* @author		Alexander Tishchenko
* @copyright	Copyright (c) 2013 Tishchenko A.
* @package		Second-Breath PHP5 framework
* @filesource	Language.php
* @name			\SB\Exception\Language
*/

    class I18n extends \Exception { }
}
