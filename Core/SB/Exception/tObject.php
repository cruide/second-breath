<?php namespace SB\Exception {
/**
* @author		Alexander Tishchenko
* @copyright	Copyright (c) 2013 Tishchenko A.
* @package		Second-Breath PHP5 framework
* @filesource	tObject.thp
* @name			\SB\Exception\tObject
*/

    class tObject extends \Exception { }
}
