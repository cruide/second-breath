<?php namespace SB\Exception {
/**
* @author       Alexander Tishchenko
* @copyright    Copyright (c) 2013 Tishchenko A.
* @package		Second-Breath PHP5 framework
* @filesource	Phpmail.php
* @name			\SB\Exception\Phpmail
*/
    if( class_exists('\\phpmailerException') 
    && !class_exists('\\SB\\Exception\\Phpmail') ) 
    {
        class Phpmail extends \phpmailerException { }
    } else {
        class Phpmail extends \Exception { }
    }
}
