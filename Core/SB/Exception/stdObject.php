<?php namespace SB\Exception {
/**
* @author       Alexander Tishchenko
* @copyright    Copyright (c) 2013 Tishchenko A.
* @package      Second-Breath PHP5 framework
* @filesource   Input.php
* @name         \SB\Exception\stdObject
*/

    class stdObject extends \Exception {}
}
