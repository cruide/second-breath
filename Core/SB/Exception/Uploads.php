<?php namespace SB\Exception {
/**
* @author		Alexander Tishchenko
* @copyright	Copyright (c) 2013 Tishchenko A.
* @package		Second-Breath PHP5 framework
* @filesource	Uploads.php
* @name			\SB\Exception\Uploads
*/

    class Uploads extends \Exception { }
}

