<?php namespace SB\Exception {
/**
* @author		Alexander Tishchenko
* @copyright	Copyright (c) 2013 Tishchenko A.
* @package		Second-Breath PHP5 framework
* @filesource	Adapter.php
* @name			\SB\Exception\DB_Driver
*/

    class DB_Driver extends \Exception {}

}


