<?php namespace SB\Exception {
/**
* @author		Alexander Tishchenko
* @copyright	Copyright (c) 2013 Tishchenko A.
* @package		Second-Breath PHP5 framework
* @filesource	Service.php
* @name			\SB\Exception\Service
*/

    class Service extends \Exception { }
}
