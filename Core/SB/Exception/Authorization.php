<?php namespace SB\Exception {
/**
* @author		Alexander Tishchenko
* @copyright	Copyright (c) 2013 Tishchenko A.
* @package		Second-Breath PHP5 framework
* @filesource	Authorization.php
* @name			\SB\Exception\Authorization
*/

    class Authorization extends \Exception { }
    
}
