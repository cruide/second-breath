<?php namespace SB\Exception {
/**
* @author		Alexander Tishchenko
* @copyright	Copyright (c) 2013 Tishchenko A.
* @package		Second-Breath PHP5 framework
* @filesource	Controller.php
* @name			\SB\Abstracts\Form
*/

    class Form extends \Exception {}
}