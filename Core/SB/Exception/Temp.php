<?php namespace SB\Exception {
/**
* @author		Alexander Tishchenko
* @copyright	Copyright (c) 2013 Tishchenko A.
* @package		Second-Breath PHP5 framework
* @filesource	Temp.php
* @name			\SB\Exception\Temp
*/

    class Temp extends \Exception { }
}