<?php namespace SB\Exception {
/**
* @author		Alexander Tishchenko
* @copyright	Copyright (c) 2013 Tishchenko A.
* @package		Second-Breath PHP5 framework
* @filesource	Access.php
* @name			\SB\Exception\Access
*/

    class Access extends \Exception { }
    
}
