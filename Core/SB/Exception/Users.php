<?php namespace SB\Exception {
/**
* @author		Alexander Tishchenko
* @copyright	Copyright (c) 2013 Tishchenko A.
* @package		Second-Breath PHP5 framework
* @filesource	Users.php
* @name			\SB\Exception\Users
*/

    class Users extends \Exception { }
    
}
