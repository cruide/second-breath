<?php namespace SB\Exception {
/**
* @author		Alexander Tishchenko
* @copyright	Copyright (c) 2013 Tishchenko A.
* @package		Second-Breath PHP5 framework
* @filesource	Router.php
* @name			\SB\Exception\Router
*/

    class Router extends \Exception {}
}
