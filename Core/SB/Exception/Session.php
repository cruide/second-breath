<?php namespace SB\Exception {
/**
* @author		Alexander Tishchenko
* @copyright	Copyright (c) 2013 Tishchenko A.
* @package		Second-Breath PHP5 framework
* @filesource	Session.php
* @name			\SB\Exception\Session
*/

    class Session extends \Exception { }
}
