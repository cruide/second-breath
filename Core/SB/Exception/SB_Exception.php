<?php namespace SB\Exception {
/**
* @author		Alexander Tishchenko
* @copyright	Copyright (c) 2013 Tishchenko A.
* @package		Second-Breath PHP5 framework
* @filesource	SB_Exception.php
* @name			\SB\Exception\SB_Exception
*/

    class SB_Exception extends \Exception
    {
// -------------------------------------------------------------------------------------
        public static function setExceptionHandler($callback = null)
        {
            if( null === $callback ) {
                $callback = array("\\SB\\Exception\\SB_Exception", "handleException");
            }
            
            if( is_callable($callback) ) {
                set_exception_handler($callback);
            }
        }
// -------------------------------------------------------------------------------------
        public function __construct($message = null, $code = 0)
        {
            if( $message == null ) {
                throw new $this('Unknown ' . get_class($this));
            }
            
            parent::__construct($message, $code);
        }
// -------------------------------------------------------------------------------------
        public function __toString()
        {
            $trace = str_replace('#', "<br />#", $this->getTraceAsString());
            
            if( is_ajax() ) {
                $html = "<div id=\"exeption-title\" style=\"font-size: 10pt; font-weight: bold;\">" . CORE_NAME . " exception</div><br />" .
                        "<div id=\"exeption-content\" style=\"font-size: 8pt;\">" .
                        "<strong>File:</strong> {$this->getFile()}<br />\n<strong>Line:</strong> {$this->getLine()}<br />\n" .
                        "<strong style=\"color: red;\">Message:</strong> {$this->getMessage()}\n</div>" .
                        "<div id=\"exeption-trace\" style=\"font-size: 8pt;\">{$trace}</div>";
            } else {
                $html = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN"><html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" />' .
                        "<meta name=\"generator\" content=\"" . FRAMEWORK . "\" /><meta name=\"description\" content=\"" . CORE_NAME . " exception\" />" .
                        "<title>" . CORE_NAME . " exception</title><style>\nbody { font-family: Tahoma, Arial, sans-serif; fint-size: 8pt; }\n</style></head><body>" .
                        "<div id=\"exeption-header\" style=\"font-size: 14pt;\">" . CORE_NAME . " exception</div><br />" .
                        "<div id=\"exeption-content\" style=\"font-size: 10pt;\">" .
                        "<strong>File:</strong> {$this->getFile()}<br />\n<strong>Line:</strong> {$this->getLine()}<br />\n" .
                        "<strong style=\"color: red;\">Message:</strong> {$this->getMessage()}\n</div>" .
                        "<div id=\"exeption-trace\"><p style=\"font-size: 10pt;\"><strong>Trace:</strong><br />\n<span style=\"font-size: 8pt;\">{$trace}</span></p></div>" .
                        '<div id="exeption-footer" style="margin-top: 10px; border-top: 1px solid gray; font-size: 8pt;">' .
                        '<span style="color: gray;">' . FRAMEWORK . ', Copyright &copy; 2013 Tishchenk A., All rights reserved.</span></div></body></html>';
            }
            
            return $html;
        }
// -------------------------------------------------------------------------------------
        public static function handleException(\Exception $exception)
        {
            http_cache_off();
            echo $exception;
        }
    }    
}