<?php namespace SB\Exception {
/**
* @author       Tishchenko Alexander
* @copyright    Copyright (c) 2014 All rights to Tishchenko A.
* @package		Second-Breath PHP5 framework
* @filesource	Countries.php
* @name			\SB\Exception\Countries
*/

    class Countries extends \Exception {}
}
