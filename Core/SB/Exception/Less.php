<?php namespace SB\Exception {
/**
* @author		Alexander Tishchenko
* @copyright	Copyright (c) 2013 Tishchenko A.
* @package		Second-Breath PHP5 framework
* @filesource	Plugin.php
* @name			\SB\Exception\Less
*/

    class Less extends \Exception {}
}
