<?php namespace SB {
/**
* @author        Tishchenko Alexander
* @copyright     Copyright (c) 2014 All rights to Tishchenko A.
* @package		 Second-Breath PHP5 framework
* @filesource	 Exception.php
* @name			 \SB\Theme
*/

  class Theme extends \stdClass
  {
      protected $_css_list;
      protected $_theme_name;
      protected $_module_name;
      protected $_layout;
      protected $_config;
      protected $render;
      protected $_headers;
      protected $_assigns;
      protected static $_instance;
// -------------------------------------------------------------------------------------
      public static function Instance()
      {
          if( null === self::$_instance ) {
              self::$_instance = new self();
          }
 
          return self::$_instance;
      }
// -------------------------------------------------------------------------------------
      public function __construct()
      {
      	  $this->_config     = load_ini('config')->interface;
          $this->_theme_name = $this->_config['theme'];
          $this->_headers    = array();
          $this->_layout     = $this->_config['layout'];
          $this->_assigns    = array();
          
          if( empty($this->_theme_name) ) {
              $this->_theme_name = 'Default';
          }
          
          if( empty($this->_layout) ) {
              $this->_layout = 'Layout';
          }
          
          $this->set_header('Content-type: text/html; charset=UTF-8');
          $this->enable();
      }
      
// -------------------------------------------------------------------------------------
      /**
      * Set headers
      * 
      * @param string $header
      * @return Theme
      */
      public function set_header($header)
      {
          /* if $header is array */
          if( array_count($header) > 0 ) {
              foreach($header as $val) {
                  if( !empty($val) and is_string($val)
                  and !in_array($val, $this->_headers) ) 
                  {
                      if( preg_match('/^Content\-type/i', $val) ) {
                          $this->_content_type_clear();
                      }
                      
                      $this->_headers[] = $val;
                  }
              }
              
              return $this;
          }
          
          /* if $header is string */
          if( !empty($header) and is_string($header) 
          and !in_array($header, $this->_headers) ) 
          {
              if( preg_match('/^Content\-type/i', $header) ) {
                  $this->_content_type_clear();
              }

              $this->_headers[] = $header;
          }
          
          return $this;
      }
// -------------------------------------------------------------------------------------
      protected function _content_type_clear()
      {
          $tmp = [];

          foreach($this->_headers as $val) {
              if( !preg_match('/^Content\-type/i', $val) ) {
                  $tmp[] = $val;
              }
          }
          
          $this->_headers = $tmp;
      }
// -------------------------------------------------------------------------------------
      public function set_layout($name)
      {
          if( !preg_match('#\.' . \SB\Native::EXTENSION . '$#', $name) ) {
              $file_name = $name . '.' . \SB\Native::EXTENSION;
          }

          if( !empty($file_name) && is_file($this->get_theme_path() . DIR_SEP . $file_name) ) {
              $this->_layout = $name;
          }
      }
// -------------------------------------------------------------------------------------
      public function use_external_css($url)
      {
          if( !is_array($this->_css_list) ) {
              $this->_css_list = [];
          }
          
          if( is_url_exists($url) ) {
              $this->_css_list[] = $url;
          }
          
          return $this;
      }
// -------------------------------------------------------------------------------------
      public function use_theme_css($css_filename)
      {
          $css_file_path = $this->get_theme_path() . DIR_SEP . 'Css' . DIR_SEP . $css_filename;

          if( preg_match('/\.less\./is', $css_filename) ) {
              $less_filename = preg_replace('/\.css$/i', '', $css_filename);
              $css_file_path = $this->get_theme_path() . DIR_SEP . 'Less' . DIR_SEP . $less_filename;
          }
          
          if( is_file($css_file_path) ) {
              $this->_css_list[] = $css_filename;
          }
          
          return $this;
      }
// -------------------------------------------------------------------------------------
      public function set_theme($theme_name)
      {
          if( !empty($theme_name) && $theme_name != 'Common' 
          and is_dir(INTERFACE_DIR . DIR_SEP . $theme_name) ) 
          {
              $this->_theme_name = $theme_name;
              
              $view = \SB\Kernel::$controller->getUi();

              if( null !== $view && $view instanceof \SB\Native ) {
                  
	              $m_name       = \SB\Router::Instance()->get_module_name();
				  $module_views = correct_directory_path( 
				      MODULES_DIR . DIR_SEP . $m_name . DIR_SEP . 'Views'
				  );

				  $inteface_views = correct_directory_path( 
				      $this->get_theme_path() . DIR_SEP . 'Modules' . DIR_SEP . $m_name
				  );
				  
                  $view->setTemplateDir( (is_dir($inteface_views)) ? $inteface_views : $module_views );
			  }
          }
      }
// -------------------------------------------------------------------------------------
      public function disable()
      {
		  $this->render = false;
		  return $this;
      }
// -------------------------------------------------------------------------------------
      public function enable()
      {
		  $this->render = true;
		  return $this;
      }
// -------------------------------------------------------------------------------------
      public function get_theme_url()
      {
          return INTERFACE_URL . '/' . $this->_theme_name;
      }
// -------------------------------------------------------------------------------------
      public function get_theme_name()
      {
          return $this->_theme_name;
      }
// -------------------------------------------------------------------------------------
      public function get_theme_path()
      {
          return INTERFACE_DIR . DIR_SEP . $this->_theme_name;
      }
// -------------------------------------------------------------------------------------
      public function get_ajax_debug_info($out)
      {
          if( is_ajax() ) {
              $_ = 'Exec: ' . \SB\Profiler::stop() . 's, Mem: ' . round( (memory_get_peak_usage(true)) / 1048576, 2) .'Mb';
              return $out . javascript("\$('#debuger-information').html('{$_}');");
          }
          
          return '';
      }
// -------------------------------------------------------------------------------------
      public function get_debug_info($out) {
          
          return str_replace(
              '<!-- DEBUG-INFO -->', 
              'Exec: ' . \SB\Profiler::stop() .
              's, Mem: ' . round( (memory_get_peak_usage(true)) / 1048576, 2) .
              'Mb, SQL: ' . \SB\Base\DB_Joint::Instance()->get_log_count() . ' requests',
              $out
          );
      }
// -------------------------------------------------------------------------------------
      public function assign($key, $value)
      {
          $this->_assigns[ $key ] = $value;
          
          return $this;
      }
// -------------------------------------------------------------------------------------
      public function display($content)
      {
          if( is_ajax() || !$this->render ) {

              if( !headers_sent() && array_count($this->_headers) > 0 ) {
                  foreach($this->_headers as $key=>$val) {
                      header($val);
                  }
              }

              http_cache_off();

              if( !\SB\Cookie::is_saved() ) \SB\Cookie::Instance()->save();
              if( sb_strlen($content) > 102400 ) ini_set('zlib.output_compression', 1);

              echo $content;
              
			  return;
          }

          $templater = new \SB\Native( $this->get_theme_path() );
          
          if( array_count($this->_assigns) > 0 ) {
              foreach($this->_assigns as $key=>$val) {
                  $templater->assign($key, $val);
              }
          }
          
          $templater->assign('content', $content);

          if( function_exists('memory_get_peak_usage') ) {
              $templater->assign('max_mem_use', get_mem_use(true));
          } else { 
              $templater->assign('max_mem_use', '-//-');
          }

          $out = $templater->fetch( $this->_layout ) ;
          
          if( !headers_sent() && array_count($this->_headers) > 0 ) {
              foreach($this->_headers as $key=>$val) {
                  header($val);
              }
          }
          
          if( !\SB\Cookie::is_saved() ) \SB\Cookie::Instance()->save();
          if( sb_strlen($out) > 102400 ) ini_set('zlib.output_compression', 1);
          
          unset($templater);
          memory_clear();

          if( array_count($this->_css_list) > 0 ) {
              $_ = "\n\t\t<!-- DYNAMIC CSS -->\n";
              
              foreach($this->_css_list as $key=>$val) {
                  if( preg_match('/^http/is', $val) ) {
                      $_ .= "\t\t<link rel=\"stylesheet\" type=\"text/css\" href=\"{$val}\" />\n";
                  } else {
                      $url = $this->get_theme_url() . '/Css/' . $val;
                      $_ .= "\t\t<link rel=\"stylesheet\" type=\"text/css\" href=\"{$url}\" />\n";
                  }
              }
              
              $out = preg_replace('#\<\/head\>#is', $_ . "</head>\n", $out);
              
              unset($_, $key, $val, $url);
          }
          
          echo $this->get_debug_info($out);
      }
  }
}