<?php namespace SB {
/**
* @author        Tishchenko Alexander
* @copyright     Copyright (c) 2014 All rights to Tishchenko A.
* @package       Second-Breath PHP5 framework
* @filesource    Cloud.php
* @name          \SB\Cloud
*/

    class Cloud extends \stdClass
    {
        private static $_instance;
        protected $properties;
// -------------------------------------------------------------------------------------
        public function __construct()
        {
            $this->properties = [];
        }
// -------------------------------------------------------------------------------------
        public function get($name)
        {
            if( !isset($name) || !\SB\Match::is_varible_name($name) ) {
                throw new \Exception(
                    "Incorrect varible name `{$name}`"
                );
            }
            
            if( !isset($this->$name) ) {
                return null;
            }
            
            return $this->$name;
        }
// -------------------------------------------------------------------------------------
        public function __get($name)
        {
            return $this->get($name);
        }
// -------------------------------------------------------------------------------------
        public function __set($name, $value)
        {
            $this->set($name, $value);
        }
// -------------------------------------------------------------------------------------
        public function __isset($name)
        {
            return isset( $this->properties[ $name ] );
        }
// -------------------------------------------------------------------------------------
        public function set($name, $value)
        {
            if( isset($name) && \SB\Match::is_varible_name($name) ) {
                if( $value === null && isset($this->$name) ) {
                    unset($this->$name);
                } else {
                    $this->$name = $value;
                }
            }
            
            return $this;
        }
// -------------------------------------------------------------------------------------
        public static function Instance()
        {
            if( null === self::$_instance ) {
                self::$_instance = new self();
            }
 
            return self::$_instance;
        }
    }
    
}