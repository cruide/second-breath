<?php namespace SB {
/**
* @author        Tishchenko Alexander
* @copyright     Copyright (c) 2014 All rights to Tishchenko A.
* @package       Second-Breath PHP5 framework
* @filesource    stdObject.php
* @name          \SB\stdObject
*/

    class stdObject extends \stdClass
    {
// -------------------------------------------------------------------------------------
        /**
        * Constructor of class
        * 
        * @param array $_
        * @return stdObject
        */
        public function __construct( $_ = array() )
        {
            $this->fromArray($_);
        }
// -------------------------------------------------------------------------------------
        /**
        * Set properties from array
        * 
        * @param array $_
        */
        public function fromArray( $_ )
        {
            if( array_count($_) <= 0 ) return false;
            
            foreach($_ as $key=>$val) {
                if( \SB\Match::is_varible_name($key) ) $this->$key = $val;
            }
            
            return true;
        }
// -------------------------------------------------------------------------------------
        /**
        * return properties as array
        * 
        */
        public function toArray( $all = false )
        {
            $publics = function($obj) {
                return get_object_vars($obj);
            };
            
            return ($all === true) ? get_object_vars($this) : $publics($this);
        }
// -------------------------------------------------------------------------------------
        /**
        * load properties from file
        * 
        * @param string $filePath
        */
        public function fromFile( $filePath )
        {
            if( !is_file($filePath) || !is_readable($filePath) ) return false;
            
            try {
                $_ = @unserialize( file_get_contents($filePath) );
            } catch( \SB\Exception\stdObject $e ) {
                system_exception($e);
            }
            
            return ( !is_array($_) ) ? false : $this->fromArray($_);
        }
// -------------------------------------------------------------------------------------
        /**
        * Save properties to file
        * 
        * @param mixed $filePath
        */
        public function toFile( $filePath )
        {
            if( empty($filePath) ) return false;
            
            try {
                file_put_contents($filePath, serialize($this->toArray()));
            } catch( \SB\Exception\stdObject $e ) {
                system_exception($e);
            }
            
            return true;
        }
// -------------------------------------------------------------------------------------
        public function __get($name)
        {
            if( isset($this->$name) ) {
                return $this->$name;
            }
            
            return null;
        }
// -------------------------------------------------------------------------------------
        public function className()
        {                     
            $className = str_replace('\\', '|', get_class($this));
            return preg_replace('#(.*)\|([a-z0-9_]+)$#i', '$2', $className);
        }
// -------------------------------------------------------------------------------------
        public function classId()
        {
            return md5( get_class($this) );
        }
    }
    
}