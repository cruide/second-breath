<?php namespace SB\Interfaces {

    interface DB_Driver {
	    public function init($host, $port, $prefix);
	    public function connect($dbase, $user, $password);
	    public function get_prefix();
	    public function reload_tables();
	    public function server_version();
	    public function prepare($query);
	    public function execute($data = array());
	    public function fetch($obj = false);
	    public function fetch_all($assoc = true);
	    public function last_insert_id();
	    public function is_table_exists($table);
	    public function get_fields_list($table_name, $_as_object);
	    public function get_primary_key($table_name);
	    public function empty_table($table_name);
	    public function optimize_table($table_name);
	    public function repair_table($table_name);
	    public function check_table($table_name);
	    public function is_connected();
        public function exec($request);
        public function query($request);
    }
}