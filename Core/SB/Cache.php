<?php namespace SB {

    class Cache extends \stdClass
    {
        protected $api;
        private static $_instance;
// -----------------------------------------------------------------------------
        public function __construct()
        {
            if( function_exists('xcache_info') ) {
                $this->api = \SB\Cache\xCache::Instance();
            } else if( class_exists('\Memcache') ) {
                $this->api = \SB\Cache\Memcache::Instance();
            } else if( function_exists('apc_store') ) {
                $this->api = \SB\Cache\APC::Instance();
            } else {
                $this->api = \SB\Cache\Dummy::Instance();
            }
        }
// -----------------------------------------------------------------------------
        public function __get($name)
        {
            return $this->api->get($name);
        }
// -----------------------------------------------------------------------------
        public function __set($name, $value)
        {
            $this->api->set($name, $value);
        }
// -----------------------------------------------------------------------------
        public function __isset($name)
        {
            return $this->api->__isset($name);
        }
// -----------------------------------------------------------------------------
        public function __unset($name)
        {
            $this->api->__unset($name);
        }
// -----------------------------------------------------------------------------
        public function __call($name, $args)
        {
            if( method_exists($this->api, $name) ) {
                return call_user_func_array(
                    array($this->api, $name),
                    $args
                );
            } else {
                throw new \Exception(
                    "\\SB\\Cache: Call to undefined method {$name}"
                );
            }
        }
// -----------------------------------------------------------------------------
        public static function __callStatic($name, $args)
        {
            return self::Instance()->__call($name, $args);
        }
// -----------------------------------------------------------------------------
        /**
        * Return instance of class
        */
        public static function Instance()
        {
            if( null === self::$_instance ) {
                self::$_instance = new self();
            }

            return self::$_instance;
        }
    }
    
}