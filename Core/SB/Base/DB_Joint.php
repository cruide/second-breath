<?php namespace SB\Base {

    final class DB_Joint
    {
	    private static $_nodes, $_instance;
	    private $config;
// -------------------------------------------------------------------------------------
	    public function __construct()
	    {
		    $this->config = load_ini('dbase')->toArray();

		    if( is_array($this->config) ) {
                
                if( !isset($this->config['primary']) ) {
                    throw new \SB\Exception\SB_Exception(
                        'Primary DB configuration not found into dbase.ini'
                    );
                }
                
                $primary = $this->config['primary'];
                unset($this->config['primary']);
                
			    $driver = "\\SB\\Base\\Drivers\\{$primary['driver']}";
			    $this->_create_dbase_interface( 'primary', $primary, new $driver() );
//                $this->db_use('primary');

                if( array_count($this->config) > 0 ) {
                    foreach($this->config as $key=>$val) {
                        $driver = "\\SB\\Base\\Drivers\\{$val['driver']}";
                        $this->_create_dbase_interface( (string)$key, $val, new $driver() );
                    }
                }
		    }
	    }
// -------------------------------------------------------------------------------------
	    private function _create_dbase_interface( $name, array $config, \SB\Interfaces\DB_Driver $element)
	    {
		    try {
			    $element->init($config['host'], $config['port'], $config['prefix']);
			    $element->connect($config['base'], $config['user'], $config['password']);
		    } catch(\SB\Exception\DB_Driver $e) {
			    system_exception( $e );
		    }
		    
		    self::$_nodes[ $name ] = $element;
	    }
// -------------------------------------------------------------------------------------
        public function is_interface($name)
        {
            return isset( self::$_nodes[$name] );
        }
// -------------------------------------------------------------------------------------
        public function get_by_base_name( $name )
        {
            foreach(self::$_nodes as $key=>$val) {
                if( $val->get_base_name() == $name ) {
                    return self::$_nodes[ $key ];
                }
            }
            
            return false;
        }
// -------------------------------------------------------------------------------------
        public function __get($name)
        {
            return $this->db_use($name);
        }
// -------------------------------------------------------------------------------------
        public function db_use($name)
        {
            if( isset($name) and isset(self::$_nodes[$name]) ) {
                return self::$_nodes[ $name ];
            }
            
            throw new \SB\Exception\SB_Exception("Unknown database `{$name}`");
        }
// -------------------------------------------------------------------------------------
	    public function __call($name, $args)
	    {
		    if( method_exists(self::$_nodes['primary'], $name) ) {
			    return call_user_func_array(
			        array(self::$_nodes['primary'], $name),
			        $args
			    );
		    } else {
			    throw new \SB\Exception\DB_Driver("Call to undefined method {$name}");
		    }
	    }
// -------------------------------------------------------------------------------------
	    public static function Instance()
	    {
		    if( null === self::$_instance ) {
			    self::$_instance = new self();
		    }

		    return self::$_instance;
	    }
    }
}