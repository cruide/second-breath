<?php namespace SB\Base {
/**
* @author        Tishchenko Alexander
* @copyright     Copyright (c) 2014 All rights to Tishchenko A.
* @package		 Second-Breath PHP5 framework
* @name			 \SB\Base\tObject
*/

    class tObject extends \SB\stdObject
    {
        protected $_tObject_db, $_tObject_table, $_tObject_shadow;
// -------------------------------------------------------------------------------------
        /**
        * Constructor of class
        * 
        * @param string $table
        * @param array $filterss
        * @return tObject
        */
        public function __construct($table, $filters = null)
        {
            if( empty($table) || !is_scalar($table) ) {
                throw new \SB\Exception\tObject(
                    'Name of the table must not be empty', 500
                );
            }

            if( preg_match('#\-\>#', $table) ) {
                $data = explode('->', $table);
              
                if( empty($data[1]) ) {
                    throw new \SB\Exception\tObject(
                        "Incorrect table name: {$data[1]}", 500
                    );
                }
              
                if( \SB\Base\DB_Joint::Instance()->is_interface($data[0]) ) {
                    $this->_tObject_db = \SB\Base\DB_Joint::Instance()->db_use($data[0]);
                } else {
                    $this->_tObject_db = \SB\Base\DB_Joint::Instance();
                }
                
                $table = $data[1];
            } else {
                $this->_tObject_db = \SB\Base\DB_Joint::Instance();
            }
            
            if( !$this->_tObject_db->is_table_exists( $table ) ) {
                throw new \SB\Exception\tObject(
                    "Table `{$table}` not found", 500
                );
            }

            $this->_tObject_table  = new \SB\stdObject();
            $this->_tObject_shadow = new \SB\stdObject();

            $this->_tObject_table->columns     = $this->_tObject_db->get_fields_list($table);
            $this->_tObject_table->primary_key = $this->_tObject_db->get_primary_key($table);
            $this->_tObject_table->name        = $table;

            if( empty($this->_tObject_table->primary_key) ) {
                throw new \SB\Exception\tObject( 
                    "Undefined primary key into `{$table}`" , 500
                );
            }

            foreach($this->_tObject_table->columns as $key=>$val) {
                if( $val == 'integer' || $val == 'float' || $val == 'double' || $val == 'decimal' ) {
                    $this->$key = $this->_tObject_shadow->$key = 0;
                } else {
                    $this->$key = $this->_tObject_shadow->$key = '';
                }
            }

            if( array_count($filters) > 0 ) {
                $this->get($filters);
            }
            
            unset($key, $val, $data);
        }
// -------------------------------------------------------------------------------------
        public function column_exists($name)
        {
            if( is_array($this->_tObject_table->columns) ) return array_key_isset($name, $this->_tObject_table->columns);
            return isset($this->_tObject_table->columns->$name);
        }
// -------------------------------------------------------------------------------------
        /**
        * Get data from table by filters
        * 
        * @param array $filters
        */
        public function get($filters)
        {
            if( empty($filters) || !is_array($filters) ) {
                return false;
            }
            
            $where_keys = $where_data = [];
            
            foreach($filters as $key=>$val) {
                if( array_key_isset($key, $this->_tObject_table->columns) ) {
                    $where_keys[]       = "`{$key}` = :{$key}";
                    $where_data[ $key ] = $val;
                }
            }
            
            $where = implode(' AND ', $where_keys);
            
            try {
                $_tmp  = $this->_tObject_db->select($this->_tObject_table->name, [
                    'where'  => $where,
                    'values' => $where_data,
                    'limit'  => 1,
                ]);
            } catch( \SB\Exception\tObject $e ) {
                system_exception($e);
            }
                         
            if( empty($_tmp) ) {
                return false;
            }
            
            foreach($_tmp as $key=>$val) {
                $this->$key = $this->_tObject_shadow->$key = $val;
            }
            
            return true;
        }
// -------------------------------------------------------------------------------------
        public function save()
        {
            $key_name = $this->_tObject_table->primary_key;

            if( !empty($this->_tObject_shadow->$key_name) ) {
                return $this->_tObject_update();
            }
            
            return $this->_tObject_insert();
        }
// -------------------------------------------------------------------------------------
        protected function _tObject_insert()
        {
            $primary_key_name = $this->_tObject_table->primary_key;

            if( !empty($this->_tObject_shadow->$primary_key_name) ) {
                return false;
            }
            
            $columns    = array();
            $values     = array();
            $properties = $this->toArray();
            
            foreach($properties as $key=>$val) {
                if( is_scalar($val) && array_key_isset($key, $this->_tObject_table->columns) ) {
                    if( ($key == $this->_tObject_table->primary_key && $val != '') || $key != $this->_tObject_table->primary_key ) {
                        if( $val != '' ) {
                            $columns[]      = "`{$key}` = :{$key}";
                            $values[ $key ] = $val;
                        }
                    }
                }
            }
            
            $setters = implode(', ', $columns);
            
            if( empty($setters) ) {
                return false;
            }

            try {
                $this->_tObject_db
                     ->prepare("INSERT INTO `{$this->_tObject_table->name}` SET {$setters}")
                     ->execute($values);
            } catch( \SB\Exception\tObject $e ) {
                system_exception($e);
            }
       
            foreach($properties as $key=>$val) {
                if( is_scalar($val) && array_key_isset($key, $this->_tObject_table->columns) ) {
                    if( ($key == $this->_tObject_table->primary_key && $val != '') || $key != $this->_tObject_table->primary_key ) {
                        $this->_tObject_shadow->$key = $val;
                    }
                }
            }
            
            return $this->$primary_key_name = $this->_tObject_shadow->$primary_key_name = $this->_tObject_db->last_insert_id();
        }
// -------------------------------------------------------------------------------------
        protected function _tObject_update()
        {
            $primary_key_name = $this->_tObject_table->primary_key;
            
            if( empty($this->_tObject_shadow->$primary_key_name) || $this->_tObject_shadow->$primary_key_name != $this->$primary_key_name ) {
                return false;
            }

            $columns    = [];
            $values     = [];
            $properties = $this->toArray();

            foreach($properties as $key=>$val) {
                if( array_key_isset($key, $this->_tObject_table->columns) && $key != $primary_key_name && $val != $this->_tObject_shadow->$key ) {
                    $columns[]      = "`{$key}` = :{$key}";
                    $values[ $key ] = $val;
                }
            }
            
            $setters = implode(', ', $columns);
            $values['table_primary_key'] = $this->_tObject_shadow->$primary_key_name;

            if( empty($setters) ) {
                return true;
            }
            
            try {
                $this->_tObject_db
                     ->prepare("UPDATE `{$this->_tObject_table->name}` SET {$setters} WHERE (`{$primary_key_name}` = :table_primary_key)")
                     ->execute($values);
            } catch( \SB\Exception\tObject $e ) {
                system_exception($e);
            }
       
            foreach($properties as $key=>$val) {
                if( is_scalar($val) && array_key_isset($key, $this->_tObject_table->columns) && $key != $primary_key_name && $this->$key != $this->_tObject_shadow->$key ) {
                    $this->_tObject_shadow->$key = $val;
                }
            }
            
            return true;
        }
// -------------------------------------------------------------------------------------
        public function __destruct()
        {
            unset( $this->_tObject_db, $this->_tObject_shadow, $this->_tObject_table );
            memory_clear();
        }
    }
    
}
