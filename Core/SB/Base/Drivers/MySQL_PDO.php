<?php namespace SB\Base\Drivers {
/**
* @author		Alexander Tishchenko
* @copyright	Copyright (c) 2013 Alexander Tishchenko
* @package		Second-Breath PHP5 framework
* @filesource	MySQL_PDO.php
* @name			\SB\Base\Drivers\MySQL_PDO
*/

    if( !extension_loaded('pdo_mysql') ) { 
        trigger_error( 'MySQL PDO extension not found...', E_USER_ERROR );
    }
    
// -------------------------------------------------------------------------------------
    class MySQL_PDO implements \SB\Interfaces\DB_Driver
    {
        protected 
            $_TABLES, 
            $_PDO, 
            $_STATEMENT, 
            $_CONFIG,
            $_FIELDS_CACHE, 
            $_KEYS_CACHE, 

            $_connected, 
            $_mysql_version;
            
        private static 
            $_QUERY_LOG;
            
        const ERROR   = E_USER_ERROR;
        const WARNING = E_USER_WARNING;
        const NOTICE  = E_USER_NOTICE;
        
// -------------------------------------------------------------------------------------
        public function __construct()
        {
            if( self::$_QUERY_LOG == null ) { 
                self::$_QUERY_LOG = []; 
            }
            
            $this->_FIELDS_CACHE = [];
            $this->_connected    = false;
            
            $this->_CONFIG = [
                'host'     => 'localhost',
                'dbase'    => '',
                'user'     => 'root',
                'password' => '',
                'port'     => 3306,
                'prefix'   => 'db_',
            ];
        }
// -------------------------------------------------------------------------------------
        /**
        * Return base name used
        * 
        */
        public function get_base_name()
        {
            return (isset($this->_CONFIG['dbase'])) ? $this->_CONFIG['dbase'] : false;
        }
// -------------------------------------------------------------------------------------
        public function init($host = 'localhost', $port = 3306, $prefix = 'db_')
        {
            $this->_CONFIG['host']   = $host;
            $this->_CONFIG['port']   = $port;
            $this->_CONFIG['prefix'] = $prefix;
            
            return $this;
        }
  // -------------------------------------------------------------------------------------
        public function connect($dbase, $user = 'root', $password = '')
        {
            if( !isset($dbase) ) {
                return false;
            }
            
            $this->_CONFIG['dbase']    = $dbase;
            $this->_CONFIG['user']     = $user;
            $this->_CONFIG['password'] = $password;
            
		    try {
			    $this->_PDO = new \PDO(
                    "mysql:host={$this->_CONFIG['host']};" .
                    "port={$this->_CONFIG['port']};" .
                    "dbname={$this->_CONFIG['dbase']}", 

                    $this->_CONFIG['user'], 
                    $this->_CONFIG['password']
                );
		    } catch( \PDOException $e ) {
			    system_error( 'Unable to connect to the MySQL database. ' . $e->getMessage(), 500 );
		    }
            
            $this->_connected = true;
            
            $this->_PDO->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            $this->_PDO->setAttribute(\PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, true);
            
            $this->exec('SET character_set_database = utf8');
            $this->exec('SET NAMES utf8');

            $this->reload_tables();
            
            return $this->_connected;
        }
  // -------------------------------------------------------------------------------------
        public function is_connected()
        {
            return $this->_connected;
        }
  // -------------------------------------------------------------------------------------
        public function error( \Exception $e )
        {
            system_exception( $e );
        }
  // -------------------------------------------------------------------------------------
        public function exec( $request )
        {
            if( $this->_connected && isset($request) ) {
                
                $_     = $this->_PDO->exec( $request );
                $error = $this->_PDO->errorInfo();
                
                if( !empty($error[2]) ) {
                    throw new \PDOException( $error[2] );
                }
                
                $this->_add_request_log( $request );
                
                return $_;
            }
        }
  // -------------------------------------------------------------------------------------
        public function query( $request, $fetch_type = null, $var1 = null, $var2 = null )
        {
            if( $this->_connected && isset($request) ) {
                try {
                    if( $fetch_type === null ) {
                        $_ = $this->_PDO->query( $request );
                    } else if( $fetch_type !== null && $var1 !== null ) {
                        $_ = $this->_PDO->query( $request, $fetch_type, $var1 );
                    } else if($fetch_type !== null && $var1 !== null && $var !== null) {
                        $_ = $this->_PDO->query( $request, $fetch_type, $var1, $var2 );
                    }
                } catch( \PDOException $e ) {
                    $this->error( $e );
                }
                
                $this->_add_request_log( $request );
                
                return $_;
            }
        }
  // -------------------------------------------------------------------------------------
        /**
        * Reload tables names from DBase
        * 
        */
        public function reload_tables()
        {
            $tables = $this->prepare('SHOW TABLES FROM ' . $this->_CONFIG['dbase'] )
                           ->execute()
                           ->fetch_all();

            foreach($tables as $key=>$val) {
                $this->_TABLES[] = reset( $val );
            }
        }
  // -------------------------------------------------------------------------------------
        public function server_version()
        {
      	    if( empty($this->_mysql_version) ) {
                $ver = $this->prepare('SELECT VERSION()')->execute()->fetch();
			    $this->_mysql_version = reset( $ver );
      	    }
      	    
            return $this->_mysql_version;
        }
  // -------------------------------------------------------------------------------------
        /**
        * Check for table exists
        * 
        * @param string $table
        * @return bool
        */
        public function is_table_exists($table)
        {
            if( empty($table) ) {
                return false;
            }
            
            $table = $this->_correct_table_name($table);

            if( array_count($this->_TABLES) > 0 ) {
                return in_array($table, $this->_TABLES);
            }
            
            return false;
        }
// -------------------------------------------------------------------------------------
        public function get_prefix()
        {
		    return $this->_CONFIG['prefix'];
        }
// -------------------------------------------------------------------------------------
        public function get_tables_list()
        {
            return $this->_TABLES;
        }
// -------------------------------------------------------------------------------------
        /**
        * Return fields list
        * 
        * @param string $table_name
        * @return array
        */
        public function get_fields_list($table_name, $_as_object = false)
        {
            if( empty($table_name) ) {
                return false;
            }

            $showed_table_name = $this->_correct_table_name($table_name);
            
            if( !empty($this->_FIELDS_CACHE[ $showed_table_name ]) 
            && is_array($this->_FIELDS_CACHE[ $showed_table_name ]) ) 
            {
                return ($_as_object) ?
                    $this->_array_to_object( $this->_FIELDS_CACHE[ $showed_table_name ] ) :
                    $this->_FIELDS_CACHE[ $showed_table_name ];
            }
            
            if( !$this->is_table_exists($table_name) ) {
                throw new \SB\Exception\DB_Driver(
                    __METHOD__ . ' - Table `' . 
                    $showed_table_name . '` on database `' . 
                    $this->_CONFIG['dbase'] . '` not found'
                );
            }
            
            $_null = $this->query('SHOW FIELDS FROM ' . $showed_table_name)
                          ->fetchAll();
            
            if( empty($_null) ) {
                throw new \SB\Exception\DB_Driver(
                    __METHOD__ . ' - Empty result for fields request into `' .
                    $showed_table_name . '`',
                    500
                );
            }
            
            foreach($_null as $key=>$val) {
                if( preg_match('/^enum\(/i', $val['Type']) ) {
                    $val['Type'] = str_replace([
                            'enum(', 'ENUM(', ')', '\'', '"',
                        ], '', $val['Type']
                    );

                    $val['Type'] = explode(',', $val['Type']);
                } else if( preg_match('/^(varchar|text|char|tinytext|mediumtext|longtext)\(/i', $val['Type']) ) {
                    $val['Type'] = 'string';
                } else if( preg_match('/^(int|bigint|tinyint|smallint|mediumint|bit)\(/i', $val['Type']) ) {
                    $val['Type'] = 'integer';                                     
                } else if( preg_match('/^float\(/i', $val['Type']) ) {
                    $val['Type'] = 'float';                                     
                } else if( preg_match('/^double\(/i', $val['Type']) ) {
                    $val['Type'] = 'double';
                } else if( preg_match('/^decimal\(/i', $val['Type']) ) {
                    $val['Type'] = 'decimal';
                }

                $this->_FIELDS_CACHE[ $showed_table_name ][ $val['Field'] ] = $val['Type'];
            }

            return ($_as_object) ?
                $this->_array_to_object( $this->_FIELDS_CACHE[ $showed_table_name ] ) :
                $this->_FIELDS_CACHE[ $showed_table_name ];
        }
  // -------------------------------------------------------------------------------------
        /**
        * Return values of ENUM fields
        * 
        * @param string $table_name
        * @param string $field_name
        * @return bool/array
        */
        public function get_enum_values($table_name, $field_name) 
        {
            $showed_table_name = $this->_correct_table_name($table_name);

            if( !isset($this->_FIELDS_CACHE[ $showed_table_name ]) ) {
                $this->get_fields_list($table_name);
            }

            if( isset($this->_FIELDS_CACHE[ $showed_table_name ][ $field_name ])
            && is_array($this->_FIELDS_CACHE[ $showed_table_name ][ $field_name ]) )
            {
                return $this->_FIELDS_CACHE[ $showed_table_name ][ $field_name ];
            }
            
            return false;
        }
  // -------------------------------------------------------------------------------------
        /**
        * Return key field name of table
        * 
        * @param string $table_name
        * @return mixed
        */
        public function get_primary_key($table_name)
        {
            if( empty($table_name) ) {
                return false;
            }

            $real_table_name = $this->_correct_table_name($table_name);
            
            if( !empty($this->_KEYS_CACHE[ $real_table_name ]) ) {
                return $this->_KEYS_CACHE[ $real_table_name ];
            }
            
            $_tmp = [];
            
            if( $this->is_table_exists($table_name) ) {
                $_tmp = $this->query("SHOW KEYS FROM {$real_table_name}")
                             ->fetchAll();
            } else {
                throw new \SB\Exception\DB_Driver(
                    "MySQL_PDO::get_primary_key - Table `{$real_table_name}` on database `{$this->_CONFIG['dbase']}` not found",
                    500
                );
            }
            
            if( array_count($_tmp) ) {
                foreach($_tmp as $key=>$val) {
                    if( $val['Key_name'] == 'PRIMARY' ) { 
                        $this->_KEYS_CACHE[ $real_table_name ] = $val['Column_name'];
                        return $val['Column_name'];
                    }
                }
            }
             
            return false;
        }
  // -------------------------------------------------------------------------------------
        public function prepare($request, $driver_options = array())
        {
            if( empty($request) ) {
                throw new \SB\Exception\DB_Driver(
                    "MySQL_PDO::prepare - Empty request ({$request})"
                );
            }
            
            if( isset($this->_STATEMENT) ) $this->_STATEMENT = null;
            
            $request          = $this->_correct_table_name($request);
            $this->_STATEMENT = $this->_PDO->prepare($request, $driver_options);
            
            return $this;
        }
// -------------------------------------------------------------------------------------
        public function execute( $data = array() )
        {
            $vars = [];

            if( array_count($data) > 0 ) {
                foreach($data as $key=>$val) {
                    if( $this->_is_correct_variable_name($key) 
                    && preg_match("/:{$key}/is", $this->_STATEMENT->queryString) ) 
                    {
                        $vars[ ':'.$key ] = $val;
                    } else {
                        $vars[] = $val;
                    }
                }
            }

            if( $this->_STATEMENT instanceof \PDOStatement ) {
                try {
                    $this->_STATEMENT->execute( $vars );
                } catch( \PDOException $e ) {
                    file_put_contents(TEMP_DIR . DIR_SEP . 'sql_error', "REQUEST: {$this->_STATEMENT->queryString}\n");
                    system_exception( $e );
                }

                $this->_add_request_log( $this->_STATEMENT->queryString );    
                    
                $error = (int)$this->_STATEMENT->errorCode();
                if( !empty($error) ) {
                    $error = $this->_STATEMENT->errorInfo();
                    $this->error( new \PDOException(
                        $error[2] . ' into ' . $this->_STATEMENT->queryString 
                    ));
                }
            } else {
                throw new \SB\Exception\DB_Driver(
                    'MySQL_PDO::execute - Not prepared to request'
                );
            }
            
            return $this;
        }
  // -------------------------------------------------------------------------------------
        public function fetch_all( $mode = \PDO::FETCH_ASSOC )
        {
            if( !($this->_STATEMENT instanceof \PDOStatement) ) {
                throw new \SB\Exception\DB_Driver(
                    'nMySQL_PDO::fetch_all - Not prepared to request'
                );
            }
            
            return $this->_STATEMENT->fetchAll( $mode );
        }
// -------------------------------------------------------------------------------------
        public function fetch($obj = false, $mode = \PDO::FETCH_ASSOC)
        {
            if( !($this->_STATEMENT instanceof \PDOStatement) ) {
                throw new \SB\Exception\DB_Driver(
                    'nMySQL_PDO::fetch - Not prepared to request'
                );
            }

            return ($obj) ? 
                $this->_STATEMENT->fetchObject('\\stdClass') : 
                $this->_STATEMENT->fetch( $mode );
        }
// -------------------------------------------------------------------------------------
        /**
        * Метод для выборки из БД
        * 
        * @param string $table
        * @param array $terms
        * @return array
        * 
        * Входящие данные
        * 
        * array(
        *     'fields' => array(),      - Перечень полей
        *     'where' => '',            - условие выбора /  `a` = 1 AND `b` = :data  /
        *     'values' => array(),      - если в условие есть ключи, то передаём array с данными как в execute() 
        *     'order' => array(),       - Описание сортировки array( `a` => 'ASC', `b` => 'DESC' )
        *     'limit' => '',            - Лимит записей. В случае передачи 1, будет использован fetch(). Аналог LIMIT 30
        *     'from' => '',             - От какой записи выбирать. Аналог LIMIT 30,30
        * )
        * 
        */
        public function select($table, array $terms = [])
        {
            if( empty($terms) ) return $this->select_all($table);
            if( !$this->is_table_exists($table) ) return false;
            
            $fields_in_table = $this->get_fields_list($table);
            
            /**
            * Selected fields make
            */
            if( isset($terms['fields']) && is_array($terms['fields']) && count($terms['fields']) > 0 ) {
                $fields_selected = [];
                
                foreach($terms['fields'] as $key=>$val) {
                    if( is_scalar($val) && array_key_isset($val, $fields_in_table) ) {
                        $fields_selected[] = "`{$val}`";
                    }
                }
                
                $fields = implode(',', $fields_selected);
                unset($fields_selected, $key, $val);
            }
                
            $fields  = (empty($fields)) ? '*' : $fields;
            $request = "SELECT {$fields} FROM {$table}";
            
            if( !empty($terms['where']) ) {
                $request .= " WHERE ({$terms['where']})";
            }
            
            if( !empty($terms['group']) && array_key_isset($terms['group'], $fields_in_table) ) {
                $request .= " GROUP BY `{$terms['group']}`";
            }
        
            if( isset($terms['order']) && is_array($terms['order']) && count($terms['order']) > 0 ) {
                $order_selected = [];

                foreach($terms['order'] as $key=>$val) {
                    if( array_key_isset($key, $fields_in_table) ) {
                        $sort = ($val == 'DESC') ? 'DESC' : 'ASC';
                        $order_selected[] = "`{$key}` {$sort}";
                    }
                }
                
                $order = implode(', ', $order_selected);
                unset($order_selected, $key, $val, $sort);
                
                $request .= " ORDER BY {$order}";
            }
            
            if( isset($terms['limit']) && is_numeric($terms['limit']) && $terms['limit'] > 0 ) {
                $request .= ' LIMIT ' . ( (isset($terms['from']) && is_numeric($terms['from'])) ? "{$terms['from']}," : '' ) . $terms['limit'];
            } else {
                $terms['limit'] = 0;
            }
            
            $this->prepare($request)->execute( (isset($terms['values']) && array_count($terms['values']) > 0) ? $terms['values'] : null );
            
            return ( $terms['limit'] == 1 ) ? $this->fetch() : $this->fetch_all();
        }
// -------------------------------------------------------------------------------------
        protected function _make_where(array $where)
        {
            if( !empty($where) ) {
                foreach($where as $key=>$val ) {
                    if( is_array($val) && isset($val['operator']) && array_key_isset('value', $val) ) {
                        return "`{$key}` {$val['operator']} " . (is_numeric($val['value']) ? $val['value'] : "'{$val['value']}'");
                    }
                }
            }
            
            return null;
        }
// -------------------------------------------------------------------------------------
        public function select_all($tab_name, $order = null)
        {
            if( !$this->is_table_exists($tab_name) ) return false;

            return $this->prepare( "SELECT * FROM `{$tab_name}`" . ((!empty($order)) ? " ORDER BY {$order}" : '') )
                        ->execute()
                        ->fetch_all();
        }
// -------------------------------------------------------------------------------------
        /**
        * Получение количества записей с таблицы
        * 
        * Пример:
        * $where = '`id` = 5'
        * 
        * @param string $table
        * @param string $where
        * 
        * @return mixed
        */
        public function count($table, $where = null)
        {
            if( !$this->is_table_exists($table) ) {
                return false;
            }
            
            if( !empty($where) ) {
                $where = " WHERE ({$where})";
            }
            
            $_ = $this->prepare("SELECT COUNT(*) AS `count` FROM `{$table}`{$where}")
                      ->execute()
                      ->fetch();

            return (isset($_['count'])) ? (int)$_['count'] : false;
        }
// -------------------------------------------------------------------------------------
        public function insert($table, $fields)
        {
            if( !$this->is_table_exists($table) || !is_array($fields) ) return false;
            if( count($fields) == 0 ) return false;
            
            $table_fields = $this->get_fields_list($table);
            $sets = $values = [];
            
            foreach($fields as $key=>$val) {
                if( array_key_isset($key, $table_fields) ) {
                    $sets[] = "`{$key}` = :{$key}";
                    $values[ $key ] = $val;
                }
            }
            
            if( count($sets) > 0 ) {
                return $this->prepare("INSERT INTO `{$table}` SET " . implode(', ', $sets))
                            ->execute($values)
                            ->last_insert_id();
            }
            
            return false;
        }
// -------------------------------------------------------------------------------------
        public function last_insert_id()
        {
            if( !($this->_PDO instanceof \PDO) ) {
                throw new \SB\Exception\DB_Driver(
                    'nMySQL_PDO::last_insert_id - Class is not initialized'
                );
            }

            return $this->_PDO->lastInsertId();
        }
// -------------------------------------------------------------------------------------
        /**
        * Empty table function
        * 
        * @param string $table_name
        */
        public function empty_table($table_name)
        {
            if( !empty($table_name) && $this->is_table_exists($table_name) ) {
                $this->prepare(
                    'TRUNCATE ' . $this->_correct_table_name($table_name)
                )->execute();
                
                return true;
            }
            
            return false;
        }
  // -------------------------------------------------------------------------------------
        /**
        * Optimize table function
        * 
        * @param string $table_name
        */
        public function optimize_table($table_name)
        {
            if(!empty($table_name) && $this->is_table_exists($table_name)) {
                $this->prepare(
                    'OPTIMIZE TABLE ' . $this->_correct_table_name($table_name)
                )->execute();
                
                return true;
            }
            
            return false;
        }
  // -------------------------------------------------------------------------------------
        /**
        * Repair table function
        * 
        * @param string $table_name
        * @param bool $ext
        * @param bool $frm
        * 
        * @return bool
        */
        public function repair_table($table_name, $ext = false, $frm = false)
        {
            if( !empty($table_name) && $this->is_table_exists($table_name) ) {

                $_result = $this->prepare( 'REPAIR TABLE ' . $this->_correct_table_name($table_name) . (($ext) ? ' EXTENDED' : '') . (($frm) ? ' FRM' : '') )
                                ->execute()
                                ->fetch();
                
                if($_result['Msg_text'] == 'Ok') {
                    return true;
                }
                
                return false;
            }
            
            return false;
        }
  // -------------------------------------------------------------------------------------
        /**
        * Check table function
        * 
        * @param string $table_name
        */
        public function check_table($table_name, $ext = false)
        {
            if( !empty($table_name) && $this->is_table_exists($table_name) ) {

                $_result = $this->prepare( 'CHECK TABLE ' . $this->_correct_table_name($table_name) . (($ext) ? ' EXTENDED' : '') )
                                ->execute()
                                ->fetch();
                
                if( $_result['Msg_text'] == 'Ok' ) {
                    return true;
                }
                
                return false;
            }
            
            return false;
        }
  // -------------------------------------------------------------------------------------
        /**
        * Return request log
        * 
        */
        public static function get_log()
        {
            return self::$_QUERY_LOG;
        }
  // -------------------------------------------------------------------------------------
        public function close()
        {
            
        }
  // -------------------------------------------------------------------------------------
        public function get_log_count()
        {
            return array_count( self::$_QUERY_LOG );
        }
  // -------------------------------------------------------------------------------------
        protected function _correct_table_name($request)
        {
            if( !empty($this->_TABLES) ) {
                foreach($this->_TABLES as $key=>$val) {
                    $modifed_name = str_replace($this->_CONFIG['prefix'], '', $val);
                    $request      = str_replace ([
                            'table_' . $modifed_name,
                            'tab_' . $modifed_name
                        ],
                        $this->_CONFIG['prefix'] . $modifed_name, 
                        $request
                    );
                }
            }
            
            return $request;
        }
  // -------------------------------------------------------------------------------------
        protected function _array_to_object($_array)
        {
            if( array_count($_array) > 0 ) {
                $_ = new \SB\stdObject();
                
                foreach($_array as $key=>$val) {
                    if( $this->_is_correct_variable_name($key) ) {
                        $_->$key = $val;
                    }
                }
                
                return $_;
            }
            
            return new \SB\stdObject();
        }
// -------------------------------------------------------------------------------------
        protected function _is_correct_variable_name( $variable )
        {
            if( isset($variable)
             && preg_match('/^[a-z0-9\_]+$/i', $variable)
             && preg_match('/^[a-z\_]/i', $variable) )
            {
                return true;
            }
            
            return false;
        }
// -------------------------------------------------------------------------------------
        protected function _add_request_log( $request )
        {
            if( isset($request) ) {
                self::$_QUERY_LOG[] = [
                    'REQUEST'  => $request,
                    'DATETIME' => date('d.m.Y H:i:s'),
                ];
            }
        }
    }
}
