<?php namespace SB\Base\Drivers {
/**
* @author		Alexander Tishchenko
* @copyright	Copyright (c) 2013 Tishchenko A.
* @package		Second-Breath PHP5 framework
* @filesource	MySQLi.php
* @name			\SB\Base\Drivers\MySQLi
*/

    if( !extension_loaded('mysqli') ) {
        trigger_error( 'MySQLi extension not found...', E_USER_ERROR );
    }

    class MySQLi implements \SB\Interfaces\DB_Driver
    {
        protected
           $_query,
           $_TABLES,
           $_CONFIG,
           $_FIELDS_CACHE,
           $_KEYS_CACHE,
           $_fetched,
           $_connected,
           $result,
           $instance;

        private static 
            $_QUERY_LOG;
            
        const ERROR   = E_USER_ERROR;
        const WARNING = E_USER_WARNING;
        const NOTICE  = E_USER_NOTICE;
// -------------------------------------------------------------------------------------
        /**
        * Constructor of class
        * 
        */
        public function __construct()
        {
            if( self::$_QUERY_LOG == null ) { 
                self::$_QUERY_LOG = array(); 
            }

            $this->_FIELDS_CACHE = array();
            $this->_connected    = false;
            
            $this->_CONFIG = array (
                'host'     => 'localhost',
                'dbase'    => '',
                'user'     => 'root',
                'password' => '',
                'port'     => 3306,
                'prefix'   => 'db_',
            );
        }
// -------------------------------------------------------------------------------------
        /**
        * Return base name used
        * 
        */
        public function get_base_name()
        {
            return (isset($this->_CONFIG['dbase'])) ? $this->_CONFIG['dbase'] : false;
        }
// -------------------------------------------------------------------------------------
        public function init($host = 'localhost', $port = 3306, $prefix = 'db_')
        {
            $this->_CONFIG['host']   = $host;
            $this->_CONFIG['port']   = $port;
            $this->_CONFIG['prefix'] = $prefix;
            
            return $this;
        }
// -------------------------------------------------------------------------------------
        /**
        * Connection function
        * 
        * @param string $host
        * @param string $dbase
        * @param string $user
        * @param string $password
        */
        public function connect($dbase, $user = 'root', $password = '')
        {
            if($this->_connected === true) {
                return;
            }
            
            if( empty($dbase) ) {
                throw new \SB\Exception\DB_Driver( 
                    'Undefined host or database information for connetion' 
                );
            }

            $this->instance = new \mysqli(
                $this->_CONFIG['host'], 
                $user, $password, $dbase, 
                $this->_CONFIG['port']
            );

            if( $this->instance->connect_errno ) {
                throw new \SB\Exception\DB_Driver( $this->instance->connect_error );
            }
            
            $this->_CONFIG['user']     = $user;
            $this->_CONFIG['password'] = $password;
            $this->_CONFIG['dbase']    = $dbase;
            
            $this->_connected = true;

            $this->query('SET character_set_database = utf8');
            $this->query('SET NAMES utf8');
            
            $this->_get_tables_list();
        }
// -------------------------------------------------------------------------------------
        public function query( $request, $resultmode = null )
        {
            if( $this->_connected ) {
                 $_ = $this->instance->query( $request, $resultmode );
                 
                 if( $this->instance->errno ) {
                     throw new \SB\Exception\DB_Driver( 'Message: ' . $this->instance->error . ', Request: ' . $request );
                 }
                 
                 return $_;
            }
            
            return false;
        }
// -------------------------------------------------------------------------------------
        public function exec( $request )
        {
            return $this->query( $request );
        }
// -------------------------------------------------------------------------------------
        public function error( $message, $error = MySQLi::ERROR )
        {
            system_error( $message, 500 );
        }
// -------------------------------------------------------------------------------------
        public function get_prefix()
        {
		    return $this->_CONFIG['prefix'];
        }
  // -------------------------------------------------------------------------------------
        /**
        * Reload tables names from DBase
        * 
        */
        public function reload_tables()
        {
            $this->_TABLES = null;
            $this->_get_tables_list();
        }
  // -------------------------------------------------------------------------------------
        public function server_version()
        {
            if( !$this->_connected ) {
                return false;
            }
            
            return $this->instance->server_info;
        }
  // -------------------------------------------------------------------------------------
        /**
        * Prepare function for execute request
        * 
        * @param string $query
        * @return $this
        */
        public function prepare( $query )
        {
            if( !empty($query) and $this->_connected === true ) {
                $this->_query = $query;
            }
            
            return $this;
        }
  // -------------------------------------------------------------------------------------
        /**
        * Execute function for request
        * 
        * @param array $data
        * @return $this
        */
        public function execute( $data = array() )
        {
            if( empty($this->_query) or !$this->_connected ) {
                return $this;
            }

            $request = $this->_correct_table_name( $this->_query );
            
            if( array_count($data) > 0 ) {
                $request = $this->_parse( $request, $data );
            }
            
            if( is_object($this->result)  ) {
                $this->result->close();
            }
            
            $this->result   = $this->query( $request );
            $this->_fetched = false;

            $this->_add_request_log( $request );
            
            return $this;
        }
  // -------------------------------------------------------------------------------------
        /**
        * Fetch function for one row
        * 
        * @param bool $obj
        * @return array
        */
        public function fetch($obj = false)
        {
            if( $this->_connected and is_object($this->result) and !$this->_fetched ) {
                $tmp = ($obj) ? $this->result->fetch_object() : $this->result->fetch_assoc();
            } else {
                return array();
            }
            
            $this->_fetched = true;
            return $tmp;
        }
  // -------------------------------------------------------------------------------------
        /**
        * Fetch row function
        * 
        * @return array or false
        */
        public function fetch_row()
        {
            if($this->_connected and is_object($this->result) and !$this->_fetched) {
                $this->_fetched = true;
                return $this->result->fetch_row();
            }
            
            return false;
        }
  // -------------------------------------------------------------------------------------
        /**
        * Fetch all rows of request result
        * 
        * @param bool $assoc
        * @return array
        */
        public function fetch_all($assoc = true)
        {
            if($this->_connected and is_object($this->result) and !$this->_fetched) {
                if( $this->result->num_rows > 0 ) {
                    $_return = array();
                    for($j = 0; $j < $this->result->num_rows; $j++) {
                        if( $assoc ) {
                            $_return[] = $this->result->fetch_assoc();
                        } else {
                            $_return[] = $this->result->fetch_array();
                        }
                    }

                    $this->_fetched = true;
                    return $_return;
                } else { 
                    return array();
                }
            }
            
            return array();
        }
// -------------------------------------------------------------------------------------
        /**
        * Метод для выборки из БД
        * 
        * @param string $table
        * @param array $terms
        * @return array
        * 
        * Входящие данные
        * 
        * array(
        *     'fields' => array(),      - Перечень полей
        *     'where' => '',            - условие выбора /  `a` = 1 AND `b` = :data  /
        *     'values' => array(),      - если в условие есть ключи, то передаём array с данными как в execute() 
        *     'order' => array(),       - Описание сортировки array( `a` => 'ASC', `b` => 'DESC' )
        *     'limit' => '',            - Лимит записей. В случае передачи 1, будет использован fetch(). Аналог LIMIT 30
        *     'from' => '',             - От какой записи выбирать. Аналог LIMIT 30,30
        * )
        * 
        */
        public function select($table, array $terms = array())
        {
            if( empty($terms) ) return $this->select_all($table);
            if( !$this->is_table_exists($table) ) return false;
            
            $fields_in_table = $this->get_fields_list($table);
            
            /**
            * Selected fields make
            */
            if( isset($terms['fields']) && is_array($terms['fields']) && count($terms['fields']) > 0 ) {
                $fields_selected = array();
                
                foreach($terms['fields'] as $key=>$val) {
                    if( is_scalar($val) && array_key_isset($val, $fields_in_table) ) {
                        $fields_selected[] = "`{$val}`";
                    }
                }
                
                $fields = implode(',', $fields_selected);
                unset($fields_selected, $key, $val);
            }
                
            $fields  = (empty($fields)) ? '*' : $fields;
            $request = "SELECT {$fields} FROM {$table}";
            
            if( !empty($terms['where']) ) {
                $request .= " WHERE ({$terms['where']})";
            }
            
            if( !empty($terms['group']) && array_key_isset($terms['group'], $fields_in_table) ) {
                $request .= " GROUP BY `{$terms['group']}`";
            }

            if( isset($terms['order']) && is_array($terms['order']) && count($terms['order']) > 0 ) {
                $order_selected = array();
                
                foreach($terms['order'] as $key=>$val) {
                    if( array_key_isset($key, $fields_in_table) ) {
                        $sort = ($val == 'DESC') ? 'DESC' : 'ASC';
                        $order_selected[] = "`{$key}` {$sort}";
                    }
                }
                
                $order = implode(', ', $order_selected);
                unset($order_selected, $key, $val, $sort);
                
                $request .= " ORDER BY {$order}";
            }
            
            if( isset($terms['limit']) && is_numeric($terms['limit']) && $terms['limit'] > 0 ) {
                $request .= ' LIMIT ' . ( (isset($terms['from']) && is_numeric($terms['from'])) ? "{$terms['from']}," : '' ) . $terms['limit'];
            } else {
                $terms['limit'] = 1;
            }
            
            $this->prepare($request)->execute( (isset($terms['values']) && array_count($terms['values']) > 0) ? $terms['values'] : null );
            
            return ( $terms['limit'] == 1 ) ? $this->fetch() : $this->fetch_all();
        }
// -------------------------------------------------------------------------------------
        public function select_all($tab_name, $order = null)
        {
            return $this->prepare( "SELECT * FROM `{$tab_name}`" . ((!empty($order)) ? " ORDER BY {$order}" : '') )
                        ->execute()
                        ->fetch_all();
        }
// -------------------------------------------------------------------------------------
        /**
        * Получение количества записей с таблицы
        * 
        * Пример:
        * $where = '`id` = 5'
        * 
        * @param string $table
        * @param string $where
        * 
        * @return mixed
        */
        public function count($table, $where = null)
        {
            if( !$this->is_table_exists($table) ) {
                return false;
            }
            
            if( !empty($where) ) {
                $where = " WHERE ({$where})";
            }
            
            $_ = $this->prepare("SELECT COUNT(*) AS `count` FROM `{$table}`{$where}")
                      ->execute()
                      ->fetch();

            return (isset($_['count'])) ? (int)$_['count'] : false;
        }
// -------------------------------------------------------------------------------------
        public function insert($table, $fields)
        {
            if( !$this->is_table_exists($table) or !is_array($fields) ) return false;
            if( count($fields) == 0 ) return false;
            
            $table_fields = $this->get_fields_list($table);
            $sets = $values = array();
            
            foreach($fields as $key=>$val) {
                if( array_key_isset($key, $table_fields) ) {
                    $sets[] = "`{$key}` = :{$key}";
                    $values[ $key ] = $val;
                }
            }
            
            if( count($sets) > 0 ) {
                return $this->prepare("INSERT INTO `{$table}` SET " . implode(', ', $sets))->execute($values)->last_insert_id();
            }
            
            return false;
        }
// --------------------------------------------------------------------------------
        /**
        * Function return last insert identifier
        * 
        */
        public function last_insert_id()
        {
            return $this->instance->insert_id;
        }
// -------------------------------------------------------------------------------------
        /**
        * Check for table exists
        * 
        * @param string $table
        * @return bool
        */
        public function is_table_exists($table)
        {
            if(empty($table) or !$this->_connected) {
                return false;
            }
            
            $table = $this->_correct_table_name($table);

            if( array_count($this->_TABLES) > 0 ) {
                return in_array($table, $this->_TABLES);
            }
            
            return false;
        }
// -------------------------------------------------------------------------------------
        public function get_tables_list()
        {
            return $this->_TABLES;
        }
// -------------------------------------------------------------------------------------
        /**
        * Return request log
        * 
        */
        public static function get_log()
        {
            return self::$_QUERY_LOG;
        }
// -------------------------------------------------------------------------------------
        public function get_log_count()
        {
            return array_count(self::$_QUERY_LOG);
        }
  // -------------------------------------------------------------------------------------
        /**
        * Return fields list
        * 
        * @param string $table_name
        * @return array
        */
        public function get_fields_list($table_name, $_as_object = false)
        {
            if( !$this->_connected or empty($table_name) ) {
                return false;
            }

            $_return           = array();
            $showed_table_name = $this->_correct_table_name($table_name);
            
            if( !empty($this->_FIELDS_CACHE[ $showed_table_name ]) 
                  and is_array($this->_FIELDS_CACHE[ $showed_table_name ]) ) 
            {
                return ($_as_object) ?
                    $this->_array_to_object( $this->_FIELDS_CACHE[ $showed_table_name ] ) : 
                    $this->_FIELDS_CACHE[ $showed_table_name ];
            }
            
            if( !$this->is_table_exists($table_name) ) {
                throw new \SB\Exception\DB_Driver( 
                    "Table `{$showed_table_name}` on database `{$this->_CONFIG['dbase']}` not found"
                );
            }

            $_null = $this->prepare("SHOW COLUMNS FROM `{$table_name}`")
                          ->execute()
                          ->fetch_all();

            if( empty($_null) ) {
                throw new \SB\Exception\DB_Driver(
                    __METHOD__ . ' - Empty result for fields request into `' .
                    $showed_table_name . '`',
                    500
                );
            }             
                          
            $this->_FIELDS_CACHE[ $showed_table_name ] = array();
            
            foreach($_null as $key=>$val) {
                if( preg_match('/^enum\(/i', $val['Type']) ) {
                    $val['Type'] = str_replace( array(
                            'enum(', 'ENUM(', ')', '\'', '"',
                        ), '', $val['Type']
                    );

                    $val['Type'] = explode(',', $val['Type']);
                } else if( preg_match('/^(varchar|text|char|tinytext|mediumtext|longtext)\(/i', $val['Type']) ) {
                    $val['Type'] = 'string';
                } else if( preg_match('/^(int|bigint|tinyint|smallint|mediumint|bit)\(/i', $val['Type']) ) {
                    $val['Type'] = 'integer';                                     
                } else if( preg_match('/^float\(/i', $val['Type']) ) {
                    $val['Type'] = 'float';                                     
                } else if( preg_match('/^double\(/i', $val['Type']) ) {
                    $val['Type'] = 'double';
                } else if( preg_match('/^decimal\(/i', $val['Type']) ) {
                    $val['Type'] = 'decimal';
                }

                $this->_FIELDS_CACHE[ $showed_table_name ][ $val['Field'] ] = $val['Type'];
            }

            return ($_as_object) ?
                $this->_array_to_object( $this->_FIELDS_CACHE[ $showed_table_name ] ) : 
                $this->_FIELDS_CACHE[ $showed_table_name ];
        }
  // -------------------------------------------------------------------------------------
        /**
        * Return values of ENUM fields
        * 
        * @param string $table_name
        * @param string $field_name
        * @return bool/array
        */
        public function get_enum_values($table_name, $field_name) 
        {
            $showed_table_name = $this->_correct_table_name($table_name);

            if( empty($this->_FIELDS_CACHE[ $showed_table_name ]) ) {
                $this->get_fields_list($table_name);
            }

            if( isset($this->_FIELDS_CACHE[ $showed_table_name ][ $field_name ])
            and is_array($this->_FIELDS_CACHE[ $showed_table_name ][ $field_name ]) )
            {
                return $this->_FIELDS_CACHE[ $showed_table_name ][ $field_name ];
            }
            
            return false;
        }
  // -------------------------------------------------------------------------------------
        /**
        * Return key field name of table
        * 
        * @param string $table_name
        * @return mixed
        */
        public function get_primary_key($table_name)
        {
            if( !$this->_connected or empty($table_name) ) {
                return false;
            }

            if( !empty($this->_KEYS_CACHE[ $table_name ]) ) {
                return $this->_KEYS_CACHE[ $table_name ];
            }
            
            $_tmp = array();
             
            if( $this->is_table_exists($table_name) ) {
                $_tmp = $this->prepare('SHOW KEYS FROM ' . $table_name)
                             ->execute()->fetch_all();
            } else {
                throw new \SB\Exception\DB_Driver( 'Table `' . $this->_correct_table_name($table_name) . '` on database `' . $this->_CONFIG['dbase'] . '` not found' );
            }
            
            foreach($_tmp as $key=>$val) {
                if( $val['Key_name'] == 'PRIMARY' ) { 
                    $this->_KEYS_CACHE[ $table_name ] = $val['Column_name'];
                    return $val['Column_name'];
                }
            }
             
            return false;
        }
  // -------------------------------------------------------------------------------------
        /**
        * Return last request count
        * 
        * @return int
        */
        public function last_request_count()
        {
            return $this->result->num_rows;
        }
  // -------------------------------------------------------------------------------------
        /**
        * Empty table function
        * 
        * @param string $table_name
        */
        public function empty_table($table_name)
        {
            if( !empty($table_name) and $this->is_table_exists($table_name) ) {
                $this->prepare('TRUNCATE ' . $this->_correct_table_name($table_name))->execute();
                return true;
            }
            
            return false;
        }
  // -------------------------------------------------------------------------------------
        /**
        * Optimize table function
        * 
        * @param string $table_name
        */
        public function optimize_table($table_name)
        {
            if( !empty($table_name) and $this->is_table_exists($table_name) ) {
                $this->prepare('OPTIMIZE TABLE ' . $this->_correct_table_name($table_name))->execute();
                return true;
            }
            
            return false;
        }
  // -------------------------------------------------------------------------------------
        /**
        * Repair table function
        * 
        * @param string $table_name
        * @param bool $ext
        * @param bool $frm
        * 
        * @return bool
        */
        public function repair_table($table_name, $ext = false, $frm = false)
        {
            if(!empty($table_name) and $this->is_table_exists($table_name)) {

                $_result = $this->prepare( 'REPAIR TABLE ' . $this->_correct_table_name($table_name) . (($ext) ? ' EXTENDED' : '') . (($frm) ? ' FRM' : '') )
                                ->execute()->fetch();
                
                return ($_result['Msg_text'] == 'Ok') ? true : false;
            }
            
            return false;
        }
  // -------------------------------------------------------------------------------------
        /**
        * Check table function
        * 
        * @param string $table_name
        */
        public function check_table($table_name, $ext = false)
        {
            if(!empty($table_name) and $this->is_table_exists($table_name)) {

                $_result = $this->prepare( 'CHECK TABLE ' . $this->_correct_table_name($table_name) . (($ext) ? ' EXTENDED' : '') )
                                ->execute()->fetch();
                
                return ($_result['Msg_text'] == 'Ok') ? true : false;
            }
            
            return false;
        }
  // -------------------------------------------------------------------------------------
        protected function _correct_table_name($request)
        {
            if($this->_connected and !empty($this->_TABLES)) {
                foreach($this->_TABLES as $key=>$val) {
                    $_mofifed_name = str_replace($this->_CONFIG['prefix'], '', $val);
                    $request = str_replace ( 
                                 array('table_'.$_mofifed_name,'tab_'.$_mofifed_name), 
                                 $this->_CONFIG['prefix'].$_mofifed_name, 
                                 $request
                    );
                }
            }
            
            return $request;
        }
  // -------------------------------------------------------------------------------------
        protected function _get_tables_list()
        {
            if( !empty($this->_TABLES) ) {
                return $this->_TABLES;
            }

            $tables = $this->prepare('SHOW TABLES FROM ' . $this->_CONFIG['dbase'])
                           ->execute()
                           ->fetch_all();
             
            foreach($tables as $key=>$val) {
                $this->_TABLES[] = reset( $val );
            }
        }
  // -------------------------------------------------------------------------------------
        protected function _parse($str, $data = array())
        {
            if( empty($str) ) {
                return false;
            }

            $_parsed_str = preg_replace('/\s+/su', ' ', str_replace( array("\n","\r"), '', $str) );

            if( empty($data) or !is_array($data) ) {
                return $_parsed_str;
            }
            
            if( preg_match_all('/\:[a-z0-9\_]+/isu', $_parsed_str, $tmp) ) {
                
                if( count($tmp[0]) > count($data) ) {
                    throw new \SB\Exception\DB_Driver( 
                        'Number of keys in the request sent more, SQL request: ' . $str 
                    );
                }

                foreach($tmp[0] as $key=>$val) {
                    $key_name = str_replace(':', '', $val);
                    
                    if( array_key_isset($key_name, $data) ) {
                        $_parsed_str = str_replace($val, $this->_verify_value($data[$key_name]), $_parsed_str);
                    } else {
                        throw new \SB\Exception\DB_Driver( 
                            "Not found the key `{$key_name}` in the query, SQL request: {$str}" 
                        );
                    }
                }
            } else if( preg_match_all('/\?/su', $_parsed_str, $tmp) ) {
                if( count($tmp[0]) > count($data) ) {
                    throw new \SB\Exception\DB_Driver( 
                        "Enough value for a query, SQL request: {$str}" 
                    );
                }
                
                foreach($data as $key=>$val) {
                    $_parsed_str = preg_replace('/\?/su', $this->_verify_value($val), $_parsed_str, 1);
                }
            }

            return $_parsed_str;
        }
  // -------------------------------------------------------------------------------------
        protected function _verify_value($value)
        {
            if( get_magic_quotes_gpc() ) { 
                $value = stripslashes($value);
            }

            if( is_object($this->instance) and $this->is_connected() ) {
                $value = $this->instance->real_escape_string($value);
            } else if( is_object($this->instance) ) {
                $value = $this->instance->escape_string($value);
            }
            
            return (is_numeric($value) or $value == '0') ? $value : "'{$value}'";
        }
// -------------------------------------------------------------------------------------
        public function is_connected()
        {
            return $this->_connected;
        }
// -------------------------------------------------------------------------------------
        protected function _array_to_object($_array)
        {
            if( array_count($_array) > 0 ) {
                $_ = new \stdClass();
                
                foreach($_array as $key=>$val) {
                    if( $this->_is_correct_variable_name($key) ) {
                        $_->$key = $val;
                    }
                }
                
                return $_;
            }
            
            return new \stdClass();
        }
// -------------------------------------------------------------------------------------
        protected function _is_correct_variable_name( $variable )
        {
            if( isset($variable)
            and preg_match('/^[a-z0-9\_]+$/i', $variable)
            and preg_match('/^[a-z\_]/i', $variable) )
            {
                return true;
            }
            
            return false;
        }
// -------------------------------------------------------------------------------------
        protected function _add_request_log( $request )
        {
            if( isset($request) ) {
                self::$_QUERY_LOG[] = array(
                    'REQUEST'  => $request,
                    'DATETIME' => date('d.m.Y H:i:s'),
                );
            }
        }
// -------------------------------------------------------------------------------------
        public function close()
        {
            if( is_object($this->instance) ) {
                if( is_object($this->result) ) {
                    $this->result->close();
                }
                
                $this->instance->close();
            }
        }
    }
}
