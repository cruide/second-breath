<?php namespace SB {
/**
* @author       Tishchenko Alexander
* @copyright    Copyright (c) 2014 All rights to Tishchenko A.
* @package      Second-Breath PHP5 framework
* @filesource   Users.php
* @name         Users library
*/
    
    final class Users extends \SB\Abstracts\Library
    {
        private static $_instance;
        private $cache;
        private $i18n;
        
        private $groups = array(
            0 => array('name' => 'Root'          , 'code' => 'root'),
            1 => array('name' => 'Adminisrtators', 'code' => 'administrators'),
            2 => array('name' => 'Moderators'    , 'code' => 'moderators'),
            3 => array('name' => 'Advanced users', 'code' => 'advanced_users'),
            4 => array('name' => 'Users'         , 'code' => 'users'),
            5 => array('name' => 'Noobs'         , 'code' => 'noobs'),
            6 => array('name' => 'Guests'        , 'code' => 'guests'),
        );
// -----------------------------------------------------------------------------
        public function _init()
        {
            $this->cache = array();
            $this->i18n  = \SB\I18n::Instance();
            
            if( !isset($this->db) ) {
                throw new \SB\Exception\Users(
                    'Users library require database'
                );
            }
            
            foreach($this->groups as $key=>$val) {
                if( $this->i18n->is_exists( $val['code'] ) ) {
                    $name = $val['code'];
                    $this->groups[ $key ]['name'] = sb_ucfirst( $this->i18n->get($name) );
                }
            }
        }
// -----------------------------------------------------------------------------
        /**
        * Поиск по кешу пользователей
        * 
        * @param string $fieldname
        * @param string $hash
        * @return mixed
        */
        private function _search_into_cache($fieldname, $hash)
        {
            if( empty($fieldname) || empty($hash) || !is_scalar($fieldname) || !is_scalar($hash) ) {
                return false;
            }
            
            if( array_count($this->cache) == 0 ) {
                return false;
            }
            
            foreach($this->cache as $key=>$val) {
                if( array_key_isset($fieldname, $val) && $val[ $fieldname ] == $hash ) {
                    return $key;
                }
            }
            
            return false;
        }
// -----------------------------------------------------------------------------
        /**
        * Return group list
        * 
        * @return array
        */
        public function get_group_list()
        {
            return $this->groups;
        }
// -----------------------------------------------------------------------------
        /**
        * Return group by ID
        * 
        * @param int $id
        * @return mixed
        */
        public function get_group_by_id( $id )
        {
            if( is_numeric($id) && isset($this->groups[ $id ]) ) {
                return $this->groups[ $id ];
            }
            
            return false;
        }
// -----------------------------------------------------------------------------
        /**
        * Get user by ID
        * 
        * @param integer $id
        * @return mixed
        */
        public function get_by_id($id)
        {
            if( !is_numeric($id) ) {
                return false;
            }
            
            if( isset($this->cache[ $id ]) ) {
                return $this->cache[ $id ];
            }
            
            $user = $this->db->prepare(
                "SELECT `u`.`id`, `u`.`email`, `u`.`password`, `u`.`salt`, `u`.`registred`, `u`.`timestamp`,
                        `u`.`group_id`, `u`.`activated`, `u`.`suspended`, `u`.`ipaddr`, `u`.`session`,
                        `u`.`hash`, `p`.`nicname`, `p`.`first_name`, `p`.`middle_name`, `p`.`last_name`,
                        `p`.`birthday`, `p`.`gender`, `p`.`phone`, `p`.`avatar`
                 FROM `sb_core_users` AS `u`
                 LEFT JOIN `sb_core_users_profiles` AS `p` USING(`id`)
                 WHERE `u`.`id` = {$id}
                 LIMIT 1" 
            )->execute()->fetch();

            if( empty($user['id']) ) {
                return false;
            }
            
            $group_code    = $this->groups[ $user['group_id'] ]['code'];
            $user['group'] = $this->i18n->$group_code;
            
            return $this->cache[ $id ] = $user;
        }
// -----------------------------------------------------------------------------
        /**
        * get user by email
        * 
        * @param string $email
        * @return mixed
        */
        public function get_by_email($email)
        {
            if( !\SB\Match::is_email($email) ) {
                return false;
            }
            
            if( ($id = $this->_search_into_cache('email', $email)) !== false ) {
                return $this->cache[ $id ];
            }
            
            $user = $this->db->prepare(
                "SELECT `u`.`id`, `u`.`email`, `u`.`password`, `u`.`salt`, `u`.`registred`, `u`.`timestamp`,
                        `u`.`group_id`, `u`.`activated`, `u`.`suspended`, `u`.`ipaddr`, `u`.`session`,
                        `u`.`hash`, `p`.`nicname`, `p`.`first_name`, `p`.`middle_name`, `p`.`last_name`,
                        `p`.`birthday`, `p`.`gender`, `p`.`phone`, `p`.`avatar`
                 FROM `sb_core_users` AS `u`
                 LEFT JOIN `sb_core_users_profiles` AS `p` USING(`id`)
                 WHERE `u`.`email` = '{$email}'
                 LIMIT 1" 
            )->execute()->fetch();

            if( empty($user['id']) ) {
                return false;
            }
            
            $group_code    = $this->groups[ $user['group_id'] ]['code'];
            $user['group'] = $this->i18n->$group_code;

            return $this->cache[ (int)$user['id'] ] = $user;
        }
// -----------------------------------------------------------------------------
        /**
        * Get user by session id
        * 
        * @param string $session_id
        * @return mixed
        */
        public function get_by_session_id($session_id)
        {
            if( !is_uuid($session_id) ) {
                return false;
            }
            
            if( ($id = $this->_search_into_cache('session', $session_id)) !== false ) {
                return $this->cache[ $id ];
            }

            $user = $this->db->prepare(
                "SELECT `u`.`id`, `u`.`email`, `u`.`password`, `u`.`salt`, `u`.`registred`, `u`.`timestamp`,
                        `u`.`group_id`, `u`.`activated`, `u`.`suspended`, `u`.`ipaddr`, `u`.`session`,
                        `u`.`hash`, `p`.`nicname`, `p`.`first_name`, `p`.`middle_name`, `p`.`last_name`,
                        `p`.`birthday`, `p`.`gender`, `p`.`phone`, `p`.`avatar`
                 FROM `sb_core_users` AS `u`
                 LEFT JOIN `sb_core_users_profiles` AS `p` USING(`id`)
                 WHERE `u`.`session` = '{$session_id}'
                 LIMIT 1" 
            )->execute()->fetch();

            if( empty($user['id']) ) {
                return false;
            }
            
            $group_code    = $this->groups[ $user['group_id'] ]['code'];
            $user['group'] = $this->i18n->$group_code;

            return $this->cache[ (int)$user['id'] ] = $user;
        }
// -----------------------------------------------------------------------------
        /**
        * Checking for user by ID, email or session
        * 
        * @param mixed $identifier
        * @return bool
        */
        public function is_exists( $identifier )
        {
            if( is_numeric($identifier) ) {
                $user = $this->get_by_id( $identifier );

                if( !empty($user) ) {
                    return true;
                }
                
            } else if( \SB\Match::is_email($identifier) ) {
                $user = $this->get_by_email( $identifier );

                if( !empty($user) ) {
                    return true;
                }
                
            } else if( is_uuid($identifier) ) {
                $user = $this->get_by_session_id( $identifier );

                if( !empty($user) ) {
                    return true;
                }
                
            }
            
            return false;
        }
// -----------------------------------------------------------------------------
        /**
        * Return instance of class
        */
        public static function Instance()
        {
            if( null === self::$_instance ) {
                self::$_instance = new self();
            }

            return self::$_instance;
        }
    }    
}

