<?php namespace SB {
/**     
* @author        Tishchenko Alexander
* @copyright     Copyright (c) 2014 All rights to Tishchenko A.
* @package		 Second-Breath PHP5 framework
* @filesource	 Profiler.php
* @name			 \SB\Profiler
*/

  final class Profiler
  {
      private static $_timer_start;
      private static $_timer_end;
      private static $_messages;
// -------------------------------------------------------------------------------------
      public function __destruct()
      {
		  self::stop();
		  
		  if( defined('DEVELOP_MODE') && DEVELOP_MODE ) {
		  	  self::save( LOGS_DIR );
		  }                
      }
// -------------------------------------------------------------------------------------
      public static function start()
      {
          if( self::$_messages === null ) {
              self::$_messages = array();
          }

          self::$_timer_start = microtime( true );
          self::set("Profiler started");
      }
// -------------------------------------------------------------------------------------
      public static function stop()
      {
          self::$_timer_end = microtime( true );
          self::set("Profiler stoped");
          
          return self::get_execute_time();
      }
// -------------------------------------------------------------------------------------
      public static function set($msg)
      {
          self::$_messages[] = array (
              'pointer' => microtime( true ),
              'message' => $msg,
          );
      }
// -------------------------------------------------------------------------------------
      public static function get_time()
      {
		  return round( microtime( true ) - self::$_timer_start, 3);
      }
// -------------------------------------------------------------------------------------
      public static function get_execute_time()
      {
      	  if( self::$_timer_end === null ) {
			  self::stop();
      	  }
      	  
          return round( self::$_timer_end - self::$_timer_start, 3);
      }
// -------------------------------------------------------------------------------------
      public static function save($path)
      {
          if( !is_dir($path) ) {
              return false;
          }
          
          $out = '';
          
          foreach(self::$_messages as $val) {
              $out .= round( $val['pointer'] - self::$_timer_start, 3 ) . ": \t" . $val['message'] . "\n";
          }
          
          $tmp = '';
          
          if( is_file($path . DIRECTORY_SEPARATOR . 'profiler.log') ) {
              $tmp = file_get_contents($path . DIRECTORY_SEPARATOR . 'profiler.log');
          }
          
          file_put_contents( $path . DIRECTORY_SEPARATOR . 'profiler.log', 
                   "< ---------------- " . date('d.m.Y H:i:s') . " ----------------- >\n" . 
                   $out . ">> Runtime " . self::get_execute_time() . "s\n\n" . $tmp );
      }
// -------------------------------------------------------------------------------------
  }
}