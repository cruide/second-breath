<?php namespace SB {
/**
* @author       Tishchenko Alexander
* @copyright    Copyright (c) 2014 All rights to Tishchenko A.
* @package      Second-Breath PHP5 framework
* @filesource   Access.php
* @name         Access library
*/

    final class Access extends \SB\Abstracts\Library
    {
        private static $_instance;
        private $access;
        
        /**
        * Контроллер библиотеки
        */
        public function _init()
        {
            if( !isset($this->db) ) {
                throw new \SB\Exception\Access(
                    'Access library require database'
                );
            }
            
            $this->get_module_access( $this->Router->get_module_name() );
        }
// -----------------------------------------------------------------------------
        /**
        * Получение правил для модуля
        * Возвращает список правил
        * или FALSE
        * 
        * @param string $module
        * @param bool $reload
        * @return mixed
        */
        public function get_module_access($module, $reload = false)
        {
            if( empty($module) || !is_module_exists($module) ) {
                return false;
            }
            
            if( !is_array($this->access) ) {
                $this->access = array();
            }

            if( array_key_isset($module, $this->access) && $reload == false ) {
                return $this->access[ $module ];
            }
            
            $access = $this->db->select('tab_core_access', [
                'where'  => '`module` = :module',
                'values' => ['module' => $module],
            ]);

            if( array_count($access) > 0 ) {
                foreach($access as $key=>$val) {
                    if( !array_key_isset($val['module'], $this->access) ) {
                        $this->access[ $val['module'] ] = [];
                    }
                    
                    if( !array_key_isset($val['controller'], $this->access[ $val['module'] ]) ) {
                        $this->access[ $val['controller'] ] = [];
                    }
                    
                    $this->access[ $val['module'] ][ $val['controller'] ][ $val['method'] ] = (int)$val['group'];
                }
                
                return $this->access[ $val['module'] ];
            }
            
            $this->access[ $module ] = null;
            unset( $this->access[ $module ] );
            
            return false;
        }
// -----------------------------------------------------------------------------
        /**
        * Проверяет доступ для группы
        * 
        * @param string $module
        * @param string $controller
        * @param string $method
        * @param integer $group
        * 
        * @return bool
        */
        public function access_check($module, $controller, $method, $group)
        {
            if( empty($module) || ($module != 'Plugins' && !is_module_exists($module)) ) {
                return false;
            }
            
            if( $module == 'Plugins' && !is_plugin_exists($controller) ) {
                return false;
            }
            
            if( empty($controller) || ($module != 'Plugins' && !is_controller_exists($module, $controller)) ) {
                return false;
            }
            
            if( empty($method) || !\SB\Match::is_method_name($method)) {
                return false;
            }
            
            if( !is_numeric($group) ) {
                return false;
            }
            
            if( empty($this->access[ $module ]) 
             || empty($this->access[ $module ][ $controller ])
             || !isset($this->access[ $module ][ $controller ][ $method ]) ) 
            {
                return true;
            }
            
            if( $group <= $this->access[ $module ][ $controller ][ $method ] ) {
                return true;
            }
            
            return false;
        }
// -----------------------------------------------------------------------------
        /**
        * Устанавливает правила для списка групп
        * 
        * @param string $module
        * @param string $controller
        * @param string $method
        * @param array $groups
        * 
        * @return bool
        */
        public function set($module, $controller, $method, $group)
        {
            if( empty($module) || !is_module_exists($module) ) {
                return false;
            }
            
            if( empty($controller) || !is_controller_exists($module, $controller) ) {
                return false;
            }
            
            if( empty($method) || !\SB\Match::is_method_name($method)) {
                return false;
            }
            
            if( !is_array($groups) ) {
                return false;
            }
            
            if( count($groups) == 0 ) {

                $this->db->prepare(
                    "DELETE FROM `tab_core_access` 
                     WHERE (
                           `module`     = :module 
                       AND `controller` = :controller 
                       AND `method`     = :method
                     )"
                )->execute([
                    'module'     => $module,
                    'controller' => $controller,
                    'method'     => $method,
                ]);
                
            } else {
                
                $access = new \SB\Base\tObject('tab_core_access', [
                    'module'     => $module,
                    'controller' => $controller,
                    'method'     => $method,
                ]);
                
                if( empty($access->id) ) {
                    $access->module     = $module;
                    $access->controller = $controller;
                    $access->method     = $method;
                }
                
                $access->groups = (int)$group;
                $access->save();
                
                unset($access);
            }
            
            return true;
        }
// -----------------------------------------------------------------------------
        public static function __callStatic($name, $params)
        {
            $_self = self::Instance();

            if( method_exists($_self, $name) ) {
                return call_user_func([$_self, $name], $params);
            }
            
            system_error('You have requested a non-existent method of ' . __CLASS__ . '::' . $name);
        }
// -----------------------------------------------------------------------------
        /**
        * Return instance of class
        */
        public static function Instance()
        {
            if( null === self::$_instance ) {
                self::$_instance = new self();
            }

            return self::$_instance;
        }
        
        
    }
    
}