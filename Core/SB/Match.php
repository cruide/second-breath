<?php namespace SB {
/**
* @author        Tishchenko Alexander
* @copyright     Copyright (c) 2014 All rights to Tishchenko A.
* @package		 Second-Breath PHP5 framework
* @filesource	 Match.php
*/

  class Match 
  {
      public static function is_int8($str)
      {
          if( !isset($str) || !is_scalar($str) || !preg_match('/^([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])$/', (string)$str) ) {
              return false;
          }
          
          return true;
      }
  // -------------------------------------------------------------------------------------
      public static function is_integer($str)
      {
          if( !is_numeric($str) || !preg_match('/^[0-9]+$/', (string)$str) ) {
              return false;
          }
          
          return true;
      }
  // -------------------------------------------------------------------------------------
      public static function is_float($str)
      {
          if( is_integer($str) || !preg_match('/^[0-9\.]+$/', (string)$str) ) {
              return false;
          }
          
          return true;
      }
  // -------------------------------------------------------------------------------------
      public static function is_ipaddress($str)
      {
          if( empty($str) || !is_scalar($str) || !preg_match('/^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)+$/i', (string)$str) ) {
              return false;
          }
          
          return true;
      }
  // -------------------------------------------------------------------------------------
      public static function is_email($str)
      {
          if( empty($str) || !is_scalar($str) || !preg_match('/^[a-z0-9_.-]+@([0-9a-z_-]+\.)+[a-z]{2,4}$/i', (string)$str) ) {
              return false;
          }
          
          return true;
      }
  // -------------------------------------------------------------------------------------
      public static function is_alpha($str)
      {
          if( !isset($str) || !is_scalar($str) || !preg_match('/^[a-zA-Zа-яА-ЯЁё]+$/u', (string)$str) ) {
              return false;
          }
          
          return true;
      }
  // -------------------------------------------------------------------------------------
      public static function is_latin_only($str)
      {
          if( !isset($str) || !is_scalar($str) || !preg_match('/^[a-z]+$/i', (string)$str) ) {
              return false;
          }
          
          return true;
      }
// -------------------------------------------------------------------------------------
      public static function is_words($str)
      {
          if( !isset($str) || !is_scalar($str) || !preg_match('/^[\w\s]+$/iu', (string)$str) ) {
              return false;
          }
          
          return true;
      }
// -------------------------------------------------------------------------------------
      public static function is_alpha_adv($str)
      {
          if( !isset($str) || !is_scalar($str) || !preg_match('/^[\_\*\w\s\-]+$/', (string)$str) ) {
              return false;
          }
          
          return true;
      }
  // -------------------------------------------------------------------------------------
      public static function is_alpha_adv_rus($str)
      {
          if( !isset($str) || !is_scalar($str) || !preg_match('/^[a-zA-Zа-яА-ЯЁё0-9\_\*\s\-\.]+$/u', (string)$str) ) {
              return false;
          }
          
          return true;
      }
  // -------------------------------------------------------------------------------------
      public static function is_alphanum($str)
      {
          if( !isset($str) || !is_scalar($str) || !preg_match('/^[a-zA-Zа-яА-ЯЁё0-9]+$/u', (string)$str) ) {
              return false;
          }
          
          return true;
      }
  // -------------------------------------------------------------------------------------
      public static function is_varible_name($str)
      {
          if( !isset($str) 
           || !is_scalar($str) 
           || !preg_match('/^[a-z0-9\_]+$/i', (string)$str) 
           || preg_match('/^[0-9]/', (string)$str) ) 
          {
              return false;
          }
          
          return true;
      }
  // -------------------------------------------------------------------------------------
      public static function is_cookie_varible_name($str)
      {
          if( !isset($str) 
           || !is_scalar($str) 
           || !preg_match('/^[a-z0-9\_\-]+$/i', (string)$str) 
           || preg_match('/^[0-9\-]/', (string)$str) ) 
          {
              return false;
          }
          
          return true;
      }
  // -------------------------------------------------------------------------------------
      public static function is_date($str)
      {
          if( empty($str) || !is_scalar($str) || !preg_match('%^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)[0-9]{2}+$%', (string)$str) ) {
              return false;
          }
          
          return true;
      }
  // -------------------------------------------------------------------------------------
      public static function is_date_us($str)
      {
          if( empty($str) || !is_scalar($str) || !preg_match('%^(19|20)[0-9]{2}[-/](0[1-9]|1[012])[-/](0[1-9]|[12][0-9]|3[01])$%', (string)$str) ) {
              return false;
          }
          
          return true;
      }
// -------------------------------------------------------------------------------------
      public static function is_sql_date($str)
      {
          if( empty($str) || !is_scalar($str) || !preg_match('%^(19|20)[0-9]{2}[-](0[1-9]|1[012])[-](0[1-9]|[12][0-9]|3[01])$%', (string)$str) ) {
              return false;
          }
          
          return true;
      }
// -------------------------------------------------------------------------------------
      public static function is_time($str)
      {
          if( empty($str) || !is_scalar($str) || !preg_match('/^(2[0-3]|[0-1][0-9]):[0-5][0-9]:[0-5][0-9]+$/', (string)$str) ) {
              return false;
          }
          
          return true;
      }
// -------------------------------------------------------------------------------------
      public static function is_time_m($str)
      {
          if( empty($str) || !is_scalar($str) || !preg_match('/^(2[0-3]|[0-1][0-9]):[0-5][0-9]$/', (string)$str) ) {
              return false;
          }
          
          return true;
      }
// -------------------------------------------------------------------------------------
      public static function is_url($str)
      {
          if( empty($str) || !is_scalar($str) || !preg_match('%^(https?://)+[-A-Z0-9+&@/?=~_|!:,.;]*[A-Z0-9+&@/=~_|]$%i', (string)$str) ) {
              return false;
          }
          
          return true;
      }
// -------------------------------------------------------------------------------------
      public static function is_webmoney($str)
      {
          if( empty($str) || !is_scalar($str) || !preg_match('/^(Z|R|E|U|B|G)+[0-9]{12,12}$/', (string)$str) ) {
              return false;
          }
          
          return true;
      }
  // -------------------------------------------------------------------------------------
      public static function is_hex($str)
      {
          if( !isset($str) || !is_scalar($str) || !preg_match('/^(0[x]|#)?[a-f0-9]+$/i', (string)$str) ) {
              return false;
          }
          
          return true;
      }
  // -------------------------------------------------------------------------------------
      public static function is_text($str)
      {
          if( !isset($str) || !is_scalar($str) || !preg_match('#^[\-a-zA-Zа-яА-ЯЁё0-9\@\+/\s\?\=\_\|\!\:\,\.\(\)\;\'\&\«\»\№"]+$#u', (string)$str) ) {
              return false;
          }
          
          return true;
      }
  // -------------------------------------------------------------------------------------
      public static function is_filename($str)
      {
          if( !isset($str) || !is_scalar($str) || !preg_match('#^[a-zA-Z0-9\_\.\-]+$#u', (string)$str) ) {
              return false;
          }
          
          return true;
      }
  // -------------------------------------------------------------------------------------
      public static function is_plaintext($str)
      {
          if( !isset($str) || !is_scalar($str) || !preg_match('/^[а-яА-ЯЁё\.\,\'\"\&\w\d\s\[\]\{\}\^\%\$\#\\\!\?\-\=\|\_\*\+\(\)\:\;\/\@\“\”\»\«\`\~]+\z$/u', (string)$str) ) {
              return false;
          }
          
          return true;
      }
  // -------------------------------------------------------------------------------------
      public static function is_method_name($str)
      {
          if( empty($str) || !is_scalar($str) || preg_match('/^[0-9]/', (string)$str) || !preg_match('/^[a-z0-9_]+$/i', (string)$str) ) {
              return false;
          }
          
          return true;
      }
  // -------------------------------------------------------------------------------------
      public static function is_uri($str)
      {
          if( !isset($str) || !is_scalar($str) || !preg_match('/^[a-zA-Z0-9_.\/]+$/', (string)$str) ) {
              return false;
          }
          
          return true;
      }
// -------------------------------------------------------------------------------------
      public static function is_name($str)
      {
          if( empty($str) || !preg_match('/^[a-zA-Zа-яА-ЯЁё\'\`]+$/u', $str) ) {
              return false;
          }
          
          return true;
      }
// -------------------------------------------------------------------------------------
      public static function is_skype($str)
      {
          if( empty($str) || !preg_match('/^[0-9a-z\,\.\-\_]+$/iu', $str) ) {
              return false;
          }
          
          return true;
      }
// -------------------------------------------------------------------------------------
      public static function is_phone($str)
      {
          if( !isset($str) || !is_scalar($str) || !preg_match('/^\+?[0-9]{2}+\s?+\([0-9]{3}\)+\s?+[0-9]{3}+\-?+[0-9]{2}+\-?+[0-9]{2}+$/', (string)$str) ) {
              return false;
          }
          
          return true;
      }
  // -------------------------------------------------------------------------------------
      public static function is_phone_easy($str)
      {
          if( !isset($str) || !is_scalar($str) || !preg_match('/^\+?[0-9\s\(\)\-]+$/', (string)$str) ) {
              return false;
          }
          
          return true;
      }
// -------------------------------------------------------------------------------------
      public static function is_zipcode_us($str)
      {
          if( !isset($str) || !is_scalar($str) || !preg_match('/^[0-9]{5}(?:-[0-9]{4})?+$/', $str) ) {
              return false;
          }
          
          return true;
      }
// -------------------------------------------------------------------------------------
      public static function is_credit_card($str)
      {
          if( empty($str) || !preg_match('/^(?:4\d{3}[ -]*\d{4}[ -]*\d{4}[ -]*\d(?:\d{3})?|5[1-5]\d{2}[ -]*\d{4}[ -]*\d{4}[ -]*\d{4}|6(?:011|5[0-9]{2})[ -]*\d{4}[ -]*\d{4}[ -]*\d{4}|3[47]\d{2}[ -]*\d{6}[ -]*\d{5}|3(?:0[0-5]|[68][0-9])\d[ -]*\d{6}[ -]*\d{4}|(?:2131|1800)[ -]*\d{6}[ -]*\d{5}|35\d{2}[ -]*\d{4}[ -]*\d{4}[ -]*\d{4})$/i', $str) ) {
              return false;
          }
          
          return true;
      }
// -------------------------------------------------------------------------------------
      public static function is_domain($str)
      {
          if( empty($str) || !preg_match('/^([a-z0-9]+(-[a-z0-9]+)*\.)+[a-z]{2,}+$/i', $str) ) {
              return false;
          }
          
          return true;
      }
// -------------------------------------------------------------------------------------
      public static function is_domain_strict($str)
      {
          if( empty($str) || !preg_match('/^((?=[a-z0-9-]{1,63}\.)[a-z0-9]+(-[a-z0-9]+)*\.)+[a-z]{2,63}+$/i', $str) ) {
              return false;
          }
          
          return true;
      }
// -------------------------------------------------------------------------------------
      public static function is_match($str, $method) 
      {
          $self_class = "\\SB\\Match";
        
	      if( method_exists($self_class, 'is_' . $method) ) {
              return call_user_func( 
                  array(
                      $self_class,
                      'is_' . $method,
                  ), $str 
              );
          } else if( function_exists('is_' . $method) ) {
              return call_user_func_array( 'is_' . $method, array($str) );
          } else {
              return false;
          }
      }
  }
}