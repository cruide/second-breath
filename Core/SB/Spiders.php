<?php namespace SB {
/**
* @author        Tishchenko Alexander
* @copyright     Copyright (c) 2014 All rights to Tishchenko A.
* @package       Second-Breath PHP5 framework
* @filesource    Browser.php
* @name          \SB\Spiders
*/

  class Spiders extends \stdClass
  {
      protected $_bot, $_is_bot = false;
      protected static $_instance;
// -------------------------------------------------------------------------------------
      public static function Instance()
      {
          if( null === self::$_instance ) {
              self::$_instance = new self();
          }
 
          return self::$_instance;
      }
// -------------------------------------------------------------------------------------
      function __construct()
      {
          global $_SERVER;

          if( isset($_SERVER['HTTP_USER_AGENT']) ) {
              if (preg_match("/(yandex|yadirectbot)/is", $_SERVER['HTTP_USER_AGENT'])){ $spider = 'Yandex robot';}
              else if (preg_match("/google/is", $_SERVER['HTTP_USER_AGENT'])){ $spider = 'GoogleBot';}
              else if (preg_match("/(yahoo|udisearch)/is", $_SERVER['HTTP_USER_AGENT'])){ $spider = 'Yahoo! Slurp';}
              else if (preg_match("/(msnbot|live|bingbot)/is", $_SERVER['HTTP_USER_AGENT'])){ $spider = 'MSN Bot';}
              else if (preg_match("/webcrawler/is", $_SERVER['HTTP_USER_AGENT'])){ $spider = 'Fast/Alltheweb crawler';}
              else if (preg_match("/zyborg/is", $_SERVER['HTTP_USER_AGENT'])){ $spider = 'ZyBorg';}
              else if (preg_match("/(stack|rambler)/is", $_SERVER['HTTP_USER_AGENT'])){ $spider = 'Rambler';}
              else if (preg_match("/aport/is", $_SERVER['HTTP_USER_AGENT'])){ $spider = 'Aport';}
              else if (preg_match("/lycos/is", $_SERVER['HTTP_USER_AGENT'])){ $spider = 'Lycos';}
              else if (preg_match("/accoona/is", $_SERVER['HTTP_USER_AGENT'])){ $spider = 'Accoona Search robot';}
              else if (preg_match("/acoon/is", $_SERVER['HTTP_USER_AGENT'])){ $spider = 'Acoon Robot';}
              else if (preg_match("/gaisbot/is", $_SERVER['HTTP_USER_AGENT'])){ $spider = 'GAIS spider';}
              else if (preg_match("/gigabot/is", $_SERVER['HTTP_USER_AGENT'])){ $spider = 'GigaBlast';}
              else if (preg_match("/icorus/is", $_SERVER['HTTP_USER_AGENT'])){ $spider = 'Webmasterworld.com';}
              else if (preg_match("/(infoseek|sidewinder|ultraseek)/is", $_SERVER['HTTP_USER_AGENT'])){ $spider = 'InfoSeek';}
              else if (preg_match("/iron33/is", $_SERVER['HTTP_USER_AGENT'])){ $spider = 'Verno';}
              else if (preg_match("/(mercator|scooter|crawler3|tarantula|trek17)/is", $_SERVER['HTTP_USER_AGENT'])){ $spider = 'Altavista robot';}
              else if (preg_match("/motor/is", $_SERVER['HTTP_USER_AGENT'])){ $spider = 'CyberCon';}
              else if (preg_match("/turtle/is", $_SERVER['HTTP_USER_AGENT'])){ $spider = 'TurtleScaner';}
              else if (preg_match("/technorat/is", $_SERVER['HTTP_USER_AGENT'])){ $spider = 'TechnoratiBot';}
              else if (preg_match("/vwbot/is", $_SERVER['HTTP_USER_AGENT'])){ $spider = 'SearchBC';}
              else if (preg_match("/twiceler/is", $_SERVER['HTTP_USER_AGENT'])){ $spider = 'Cuill bot (TwicelerBot)';}
              else if (preg_match("/mail\.ru/is", $_SERVER['HTTP_USER_AGENT'])){ $spider = 'Mail.Ru';}
              else if (preg_match("/webalta/is", $_SERVER['HTTP_USER_AGENT'])){ $spider = 'WebAlta';}
              else if (preg_match("/followsite/is", $_SERVER['HTTP_USER_AGENT'])){ $spider = 'Follow Site robot';}
              else if (preg_match("/abonti/is", $_SERVER['HTTP_USER_AGENT'])){ $spider = 'Abonti robot';}
              else if (preg_match("/unchaos/is", $_SERVER['HTTP_USER_AGENT'])){ $spider = 'UnCHAOS';}
              else if (preg_match("/sygol\.com/is", $_SERVER['HTTP_USER_AGENT'])){ $spider = 'Sygol Search robot';}
              else if (preg_match("/aladin/is", $_SERVER['HTTP_USER_AGENT'])){ $spider = 'Aladin robot';}
              else if (preg_match("/ah-ha/is", $_SERVER['HTTP_USER_AGENT'])){ $spider = 'ah-ha Bot';}
              else if (preg_match("/appie/is", $_SERVER['HTTP_USER_AGENT'])){ $spider = 'Walhello.com';}
              else if (preg_match("/zippp/is", $_SERVER['HTTP_USER_AGENT'])){ $spider = 'ZipppBot';}
              else if (preg_match("/libwww/is", $_SERVER['HTTP_USER_AGENT'])){ $spider = 'PuntoBot';}
              else if (preg_match("/webzip/is", $_SERVER['HTTP_USER_AGENT'])){ $spider = 'WebZIP Bot';}
              else if (preg_match("/dnabot/is", $_SERVER['HTTP_USER_AGENT'])){ $spider = 'DNABot';}
              else if (preg_match("/spiderman/is", $_SERVER['HTTP_USER_AGENT'])){ $spider = 'SpiderMan';}
              else if (preg_match("/mambot/is", $_SERVER['HTTP_USER_AGENT'])){ $spider = 'MamBot';}
              else if (preg_match("/abachobot/is", $_SERVER['HTTP_USER_AGENT'])){ $spider = 'AbachoBOT';}
              else if (preg_match("/aesop/is", $_SERVER['HTTP_USER_AGENT'])){ $spider = 'Aesop robot';}
              else if (preg_match("/atomz/is", $_SERVER['HTTP_USER_AGENT'])){ $spider = 'Atomz robot';}
              else if (preg_match("/dumbot/is", $_SERVER['HTTP_USER_AGENT'])){ $spider = 'DumbFind.com robot';}
              else if (preg_match("/mirago/is", $_SERVER['HTTP_USER_AGENT'])){ $spider = 'Mirago UK Robot';}
              else if (preg_match("/ftb-bot/is", $_SERVER['HTTP_USER_AGENT'])){ $spider = 'FBS robot';}
              else if (preg_match("/slurp/is", $_SERVER['HTTP_USER_AGENT'])){ $spider = 'SlurpBot';}
              else if (preg_match("/yodaobot/is", $_SERVER['HTTP_USER_AGENT'])){ $spider = 'YodaoBot (yodao.com)';}
              else if (preg_match("/voilabot/is", $_SERVER['HTTP_USER_AGENT'])){ $spider = 'VoilaBot BETA 1.2';}
              else if (preg_match("/vbseo/is", $_SERVER['HTTP_USER_AGENT'])){ $spider = 'vBSEO (vbseo.com)';}
              else if (preg_match("/sogou/is", $_SERVER['HTTP_USER_AGENT'])){ $spider = 'Sogou web spider';}
              else if (preg_match("/magpie/is", $_SERVER['HTTP_USER_AGENT'])){ $spider = 'Brandwatch';}
              else if (preg_match("/dotbot/is", $_SERVER['HTTP_USER_AGENT'])){ $spider = 'DotBot';}
              else if (preg_match("/exabot/is", $_SERVER['HTTP_USER_AGENT'])){ $spider = 'Exabot (exalead.com)';}
              else if (preg_match("/sitebot/is", $_SERVER['HTTP_USER_AGENT'])){ $spider = 'SiteBot (sitebot.org)';}
              else if (preg_match("/discobot/is", $_SERVER['HTTP_USER_AGENT'])){ $spider = 'DiscoBot (discoveryengine.com)';}
              else if (preg_match("/yeti/is", $_SERVER['HTTP_USER_AGENT'])){ $spider = 'NHN Corp. (help.naver.com)';}
              else if (preg_match("/plukkie/is", $_SERVER['HTTP_USER_AGENT'])){ $spider = 'Plukkie robot v1.4';}
              else if (preg_match("/[a-z0-9._-]bot[a-z0-9._-]/is", $_SERVER['HTTP_USER_AGENT'])){ $spider = $_SERVER['HTTP_USER_AGENT'];}
              else { $spider = false;}
          } else {
              $spider = false;
          }

          if( !empty($spider) ) {
              $this->_is_bot = true;
              
              $this->_bot = new \SB\stdObject( array (
                  'date'    => date("d.m.Y/H:i"),
                  'name'    => $spider,
                  'ip'      => get_ip_address(),
                  'request' => \SB\Router::Instance()->get_original_uri(),
              ));
              
              \SB\Native::assign_global('Spider', $this->_bot);
          } else {
              $this->_bot = array();
              \SB\Native::assign_global('Spider', false);
          }

          $cfg = load_ini('config')->application;
          
          if( !\SB\Router::Instance()->is_plugin() && isset($cfg['spiders_log']) && $cfg['spiders_log'] == true ) {
              $this->write_log();
          }
      }
// -------------------------------------------------------------------------------------
      public function write_log()
      {
          if( !$this->_is_bot ) {
              return true;
          }
          
          if( is_dir(LOGS_DIR) ) {
              if( is_file(LOGS_DIR . DIR_SEP . 'bots.log.txt') ) {
                  $_ = file_get_contents(LOGS_DIR . DIR_SEP . 'bots.log.txt');
              } else {
                  $_ = '';
              }
              
              if( $this->_bot->request == '' ) {
                  $this->_bot->request = '/';
              } else {
                  $this->_bot->request = '/' . $this->_bot->request;
              }
              
              file_put_contents(
                  LOGS_DIR . DIR_SEP . 'bots.log.txt',
                  "[{$this->_bot->date}] {$this->_bot->name} ({$this->_bot->ip}): {$this->_bot->request}\n" . $_
              );
              
              return true;
          }
          
          return false;
      }
// -------------------------------------------------------------------------------------
      public function read_log()
      {
          if( is_file(LOGS_DIR . DIR_SEP . 'bots.log.txt') ) {
              return file(LOGS_DIR . DIR_SEP . 'bots.log.txt');
          }
          
          return null;
      }
// -------------------------------------------------------------------------------------
      public function is_bot()
      {
          return $this->_is_bot;
      }
// -------------------------------------------------------------------------------------
      public function get_info()
      {
          return ($this->_is_bot) ? $this->_bot : null;
      }
// -------------------------------------------------------------------------------------
  }

}