<?php namespace SB { 

  class Log extends \stdClass
  {
     protected $_logs;
     protected static $_instance;
// -------------------------------------------------------------------------------
     function __construct()
     {
         $this->_logs   = array();
         $this->_writed = false;
     }
// -------------------------------------------------------------------------------
     /**
     * Добавление log-записи
     * 
     * @param string $method
     * @param string $message
     */
     public function add($message, $error = false)
     {
         if( !empty($message) ) {
             $this->_logs[] = [
                 'time'   => date('Y-m-d H:i:s'),
                 'msg'    => $message,
                 'error'  => $error,
             ];
         }
         
         return $this;
     }
// -------------------------------------------------------------------------------
     /**
     * Запись лога в файл
     * 
     */
     public function write()
     {
         $_tmp = '';

         if( !is_dir(LOGS_DIR) ) sb_mkdir(LOGS_DIR, 0775, true);
         
         if( !empty($this->_logs) ) {
             foreach($this->_logs as $key=>$val) {
                 $_tmp .= "[{$val['time']}] {$val['msg']}\n";
             }

             if( is_file(LOGS_DIR . DIR_SEP . 'app.log.txt') ) {
                 if( filesize(LOGS_DIR . DIR_SEP . 'app.log.txt') >= 614400 ) {
                     $logfile_out = LOGS_DIR . DIR_SEP . 'app-' . date('Ymd');
                     
                      $j = 1;
                      while(true) {
                          if( !is_file($logfile_out . "-{$j}.log.txt.gz") ) {
                              $logfile_out .= "-{$j}.log.txt.gz";
                              break;
                          }
                          $j++;
                      }
                     
                     gz_file_pack(LOGS_DIR . DIR_SEP . 'app.log.txt', $logfile_out);
                     @unlink(LOGS_DIR . DIR_SEP . 'app.log.txt');
                 }
                 
                 $_tmp .= file_get_contents( LOGS_DIR . DIR_SEP . 'app.log.txt' );
                 file_put_contents( LOGS_DIR . DIR_SEP . 'app.log.txt', $_tmp );

             } else {
                 @file_put_contents( LOGS_DIR . DIR_SEP . 'app.log.txt', $_tmp );
             }
             
             $this->_logs = array();
         }
     }
// -------------------------------------------------------------------------------------
      public static function Instance()
      {
          if( null === self::$_instance ) {
              self::$_instance = new self();
          }
 
          return self::$_instance;
      }
  }
  
}  

