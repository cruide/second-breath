<?php namespace SB {
/**
* @author        Tishchenko Alexander
* @copyright     Copyright (c) 2014 All rights to Tishchenko A.
* @package       Second-Breath PHP5 framework
* @filesource    Cookie.php
* @name          \SB\Cookie
*/

    class Cookie extends \stdClass
    {
        protected $cookies;
        private static $_instance, $_saved, $_need_save;
// -------------------------------------------------------------------------------------
        public function __construct()
        {
            if( self::$_saved === null ) self::$_saved = false;
            if( self::$_need_save === null ) self::$_need_save = false;

            $this->cookies = [];
            $tmp           = \SB\Input::Instance()->cookie();

            if( array_count($tmp) > 0 ) {
                if( array_key_isset('PHPSESSID', $tmp) || array_key_isset('SBSID', $tmp) || array_key_isset('SBLOCALE', $tmp) ) {
                    unset($tmp['PHPSESSID'], $tmp['SBSID'], $tmp['SBLOCALE']);
                }
                
                if( array_count($tmp) > 0 ) {
                    foreach($tmp as $key=>$val) {
                        if( \SB\Match::is_varible_name($key) ) {
                            if( $val != '' ) {
                                $this->cookies[ $key ] = unserialize(
                                    @gzuncompress( base64_decode($val) )
                                );
                            } else {
                                $this->cookies[ $key ] = '';
                            }
                        }
                    }
                }
            }
            
            unset($tmp);
        }
// -------------------------------------------------------------------------------------
        public function get($name = null)
        {
            if( empty($name) ) {
                return $this->cookies;
            }
            
            if( isset($this->cookies[ $name ]) ) {
                return $this->cookies[ $name ];
            }
            
            return null;
        }
// -------------------------------------------------------------------------------------
        public function set($name, $value = null)
        {
            if( \SB\Match::is_cookie_varible_name($name) && isset($value) ) {
                $this->cookies[ $name ] = $value;
                self::$_need_save = true;
            } else if( isset($this->cookies[ $name ]) && $value === null ) {
                unset($this->cookies[ $name ]);
                setcookie($name, '', time() - 3600, '/');
            } else {
                throw new \SB\Exception\SB_Exception(
                    "Can not set a cookie `{$name}` with a value of `{$value}`"
                );
            }
            
            return $this;
        }
// -------------------------------------------------------------------------------------
        public function save()
        {
            if( empty($this->cookies) || !self::$_need_save ) {
                return false;
            }
            
            foreach($this->cookies as $key=>$val) {
                setcookie($key, base64_encode( gzcompress(serialize($val), 9) ), time() + 86400, '/');
            }
                
            self::$_saved = true;
        }
// -------------------------------------------------------------------------------------
        public static function is_saved()
        {
            return self::$_saved;
        }
// -------------------------------------------------------------------------------------
        public function __get($name)
        {
            return $this->get($name);
        }
// -------------------------------------------------------------------------------------
        public function __set($name, $value)
        {
            $this->set($name, $value);
        }
// -------------------------------------------------------------------------------------
        public function __isset($name)
        {
            return isset( $this->cookies[ $name ] );
        }
// -------------------------------------------------------------------------------------
        public function __destruct()
        {
            if( !headers_sent() && !self::$_saved ) {
                $this->save();
            }
        }
// -------------------------------------------------------------------------------------
        public static function Instance()
        {
            if( null === self::$_instance ) {
                self::$_instance = new self();
            }
 
            return self::$_instance;
        }
    }
}