<?php namespace SB {
/**
* @author        Tishchenko Alexander
* @copyright     Copyright (c) 2014 All rights to Tishchenko A.
* @package		 Second-Breath PHP5 framework
* @filesource	 Native.php
* @name			 \SB\Native
*/

  class Native extends \stdClass
  {
      public static $correctXHTML = false;
      private static $globals;
      protected $_file, $properties, $template_dir;
      
      const XHTML_CRRECTION_ON  = true;
      const XHTML_CRRECTION_OFF = false;
      const EXTENSION           = 'phtml';
      
// -------------------------------------------------------------------------------------
      public function __construct($tplDirectory)
      {
          $this->properties = array();
          $this->setTemplateDir($tplDirectory);
      }
// -------------------------------------------------------------------------------------
      public function __destruct()
      {
          unset($this->properties);
      }
// -------------------------------------------------------------------------------------
      public function setTemplateDir($tplDir)
      {
		  if( !empty($tplDir) and is_dir($tplDir) ) {
			  $this->template_dir = correct_directory_path( $tplDir, true );
		  } else {
			  throw new \SB\Exception\Native("Incorrect template `{$tplDir}` directory");
		  }
      }

// -------------------------------------------------------------------------------------

      public function __get($name)
      {
          if( isset($this->properties[ $name ]) ) {
              return $this->properties[ $name ];
          }

          return null;
      }
      
// -------------------------------------------------------------------------------------
      public function __set($name, $value)
      {
          if( null === $value and isset($this->properties[ $name ]) ) {
              $this->properties[ $name ] = null;
              unset($this->properties[ $name ]);
          } else {
              $this->properties[ $name ] = $value;
          }
      }
// -------------------------------------------------------------------------------------
      public function __clone()
      {
          $this->properties = array();
      }
// -------------------------------------------------------------------------------------
      public function __isset($name)
      {
          if( isset($this->$name) ) {
              return true;
          } else {
              return isset( $this->properties[ $name ] );
          }
      }
// -------------------------------------------------------------------------------------
      public static function assign_global($name, $value = '')
      {
          if( !is_array(self::$globals) ) {
              self::$globals = array();
          }
          
          if( !is_numeric($name) and is_string($name) and !is_array($name)) {
              self::$globals[ $name ] = $value;
          } else if( !is_numeric($name) and is_string($name) and $value === null ) {
              unset( self::$globals[$name] );
          } else if( is_array($name) and $value === '') {
              foreach($name as $key=>$val) {
                  if( !is_numeric($key) ) {
                      self::$globals[ $key ] = $val;
                  }
              }
          }
      }
// -------------------------------------------------------------------------------------
      public function assign($name, $value = '')
      {
          if( !is_numeric($name) and is_string($name) and !is_array($name)) {
              $this->properties[ $name ] = $value;
          } else if( !is_numeric($name) and is_string($name) and $value === null ) {
              $this->remove($name);
          } else if( is_array($name) and $value === '') {
              foreach($name as $key=>$val) {
                  if( !is_numeric($key) ) {
                      $this->properties[ $key ] = $val;
                  }
              }
          }
          
          return $this;
      }
// -------------------------------------------------------------------------------------
      public function remove($name)
      {
          if( isset($this->properties[ $name ]) ) {
              $this->properties[ $name ] = null;
              unset($this->properties[ $name ]);
          }
          
          return $this;
      }
// -------------------------------------------------------------------------------------
      public function fetch( $file_name, $need_ext = true, $xhtml = Native::XHTML_CRRECTION_OFF )
      {
          if( $need_ext && !preg_match('#\.' . Native::EXTENSION . '$#', $file_name) ) {
              $file_name = $file_name . '.' . Native::EXTENSION;
          }
          
          $file_path = $this->template_dir . $file_name;
          
          if( !is_file($file_path) ) {
              throw new \SB\Exception\Native("Unknown template file {$file_path}");
          }

          if( $xhtml === Native::XHTML_CRRECTION_ON ) {
              $_ = $this->_validXHTML( $this->_exec( $file_path ) );
          } else {
          	  $_ = $this->_exec( $file_path );
		  }
          
          return $_;
      }
// -------------------------------------------------------------------------------------
      public function display( $file_name, $xhtml = Native::XHTML_CRRECTION_OFF )
      {
          echo $this->fetch( $file_name, true, $xhtml );
      }
// -------------------------------------------------------------------------------------
      public function clear()
      {
          $this->properties = array();
      }
// -------------------------------------------------------------------------------------
      public function parseText( $str )
      {
          if( empty($str) ) return $str;
          
          foreach($this->properties as $key=>$val) {
              if( is_scalar($val) ) {
                  $str = str_replace( '[$' . $key . ']', $val, $str );
              }
          }
          
          foreach(self::$globals as $key=>$val) {
              if( is_scalar($val) ) {
                  $str = str_replace( '[$' . $key . ']', $val, $str );
              }
          }

          if( preg_match_all('/\[plugin=([a-z0-9_]+)\]/is', $str, $plugins) ) {
              if( isset($plugins[1]) && array_count($plugins) > 0 ) {
                  foreach($plugins[0] as $key=>$val) {
                      $str = str_replace($val, sb_plugin($plugins[1][ $key ]), $str);
                  }
              }
          }
          
          unset($plugins, $key, $val);
          
          return $str;
      } 
// -------------------------------------------------------------------------------------
      protected function _exec( $_native_execute_prepared_file ) 
      {
          if( !is_file($_native_execute_prepared_file) ) {
              throw new \SB\Exception\Native(
                  "Prepared file {$_native_execute_prepared_file} not found"
              );
          }

          if( is_array(self::$globals) ) { 
          	  extract( self::$globals ); 
          }
          
          if( is_array($this->properties) ) { 
          	  extract( $this->properties ); 
          }

          ob_start();
          ob_implicit_flush(true);

		  include($_native_execute_prepared_file);

          return ob_get_clean();
      }
// -------------------------------------------------------------------------------------
      protected function _validXHTML($text)
      {
          if( !empty($text) ) {
              $ret = preg_replace("#\s*(cellspacing|cellpadding|border|width|height|colspan|rowspan)\s*=\s*(\d+)\s*((\%|px)?)(\s*)#si", " $1=\"$2$3\" ", $text);
              $ret = preg_replace("#\s*(align|valign)\s*=\s*(\w+)\s*#si", " $1=\"$2\"", $ret);
              $ret = preg_replace("#<(img|input|meta|link|base)\s*(.*?)\s*/?>#is", "<$1 $2 />", $ret);
              $ret = preg_replace("#<br\s*/?>#is", "<br />", $ret);
              $ret = preg_replace("#<hr(.*?)\s*/?>#is", "<hr$1 />", $ret);
              $ret = preg_replace("#\s+>#is", ">", $ret);
              $ret = preg_replace("#\s*=\s*#is", "=", $ret);
              return $ret;
          } else { 
              return '';
          }
      }
  }
}