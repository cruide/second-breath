<?php namespace SB {
/**
* @author        Tishchenko Alexander
* @copyright     Copyright (c) 2014 All rights to Tishchenko A.
* @package		 Second-Breath PHP5 framework
* @filesource	 Temp.php
* @name			 \SB\Temp
*/

  class Temp extends \stdClass
  {
      protected $_name, $_dir, $_content;
// ------------------------------------------------------------------------------
      /**
      * Constructor
      * 
      * @param string $name
      * @return Temp
      */
      public function __construct( $name, $directory = null )
      {
          if( empty($name) ) {
              throw new \SB\Exception\Temp( 
                  $this->className() . '::__construct - Undefined temp name' 
              );
          }
          
          $this->_dir  = ( !empty($directory) && is_dir($directory) ) 
                             ? correct_directory_path($directory, true) 
                                 : correct_directory_path(TEMP_DIR, true);
                             
          $this->_name = strtolower($name);
      }
// ------------------------------------------------------------------------------
      public function set_path( $dir )
      {
          if( is_dir($dir) ) {
              $this->_dir = correct_directory_path($dir, true);
          }
          
          return $this;
      }
// ------------------------------------------------------------------------------
      /**
      * Set temp content
      * 
      * @param mixed $content
      * @return Temp
      */
      public function set_content( $content )
      {
          if( is_scalar($content) ) {
              $this->_content = $content;
          }
          
          return $this;
      }
// ------------------------------------------------------------------------------
      /**
      * Get content
      * 
      */
      public function get_content()
      {
          return $this->_content;
      }
// ------------------------------------------------------------------------------
      /**
      * Write serialized content to file
      * 
      */
      public function write()
      {
          if( $this->_content == '' ) return false;
          if( !file_put_gz_content( $this->_dir . $this->_name, serialize($this->_content) ) ) {
              throw new \SB\Exception\Temp( 
                  $this->className() . '::write - Could not write a temporary file. Check the correctness of the path.' 
              );
              return false;
          }
          
          return true;
      }
// ------------------------------------------------------------------------------
      public function read()
      {
		  if( is_file($this->_dir . $this->_name) && is_readable($this->_dir . $this->_name) ) {
			  $this->_content = unserialize( file_get_gz_content($this->_dir . $this->_name) );
		  }
		  
		  return $this->_content;
      }
// ------------------------------------------------------------------------------
      /**
      * Delete temp file
      * 
      */
      public function delete()
      {
          if( is_file($this->_dir . $this->_name) ) {
              @unlink($this->_dir . $this->_name);
          }
      }
  }
}