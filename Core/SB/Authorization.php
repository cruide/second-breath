<?php namespace SB {
/**
* @author       Tishchenko Alexander
* @copyright    Copyright (c) 2014 All rights to Tishchenko A.
* @package      Second-Breath PHP5 framework
* @filesource   Authorization.php
* @name         Authorization library
*/

    final class Authorization extends \SB\Abstracts\Library
    {
        private static $_instance;
        
        private $session;
        private $_auth;
        private $_user;
        
// -----------------------------------------------------------------------------

        public function _init()
        {
            if( !isset($this->db) ) {
                throw new \SB\Exception\Authorization(
                    'Authorization library require database'
                );
            }
            
            $this->session = \SB\Session::Instance();
            $this->_auth   = null;
        }
// -----------------------------------------------------------------------------
        public function signout()
        {
            if( $this->is_auth() ) {
                $user = new \SB\Base\tObject('tab_core_users', [
                    'id' => $this->_user['id'],
                ]);
                
                $user->session = '';
                $user->save();

                $this->Session->destroy();
                
                return true;
            }
            
            return false;
        }
// -----------------------------------------------------------------------------
        public function signin($identifier)
        {
            $session_id = $this->session->id();
            $ip_address = get_ip_address();

            if( is_numeric($identifier) && $identifier > 0 ) {
                $user = new \SB\Base\tObject('tab_core_users', [
                    'id' => $identifier,
                ]);
            } else if( \SB\Match::is_email($identifier) ) {
                $user = new \SB\Base\tObject('tab_core_users', [
                    'email' => $identifier,
                ]);
            } else {
                return false;
            }

            if( empty($user->id) ) {
                return false;
            }
            
            $user->session   = $session_id;
            $user->ipaddr    = $ip_address;
            $user->timestamp = time();
            $user->save();
            
            $this->_auth = true;
            $this->_user = \SB\Users::Instance()->get_by_id( $user->id );

            unset($session_id, $ip_address, $user);
            
            \SB\Native::assign_global('auth_user', $this->_user);
            
            return $this->_auth;
        }
// -----------------------------------------------------------------------------
        public function is_auth()
        {
            if( $this->_auth === true || $this->_auth === false ) {
                return $this->_auth;
            }

            $session_id = $this->session->id();
            $ip_address = get_ip_address();
            
            $user = new \SB\Base\tObject('tab_core_users', [
                'session' => $session_id,
                'ipaddr'  => $ip_address,
            ]);
            
            if( empty($user->email) ) {
                return $this->_auth = false;
            }
            
            $this->_auth = true;
            $this->_user = \SB\Users::Instance()->get_by_id( $user->id );

            \SB\Native::assign_global('auth_user', $this->_user);
            
            unset($session_id, $ip_address, $user);
            
            return $this->_auth;
        }
// -----------------------------------------------------------------------------
        public function get_auth_user()
        {
            if( $this->is_auth() ) {
                return $this->_user;
            }
            
            return false;
        }
// -----------------------------------------------------------------------------
        public function get_auth_user_group()
        {
            if( $this->is_auth() ) {
                return $this->_user['group_id'];
            }
            
            return false;
        }
// -----------------------------------------------------------------------------
        /**
        * Return instance of class
        */
        public static function Instance()
        {
            if( null === self::$_instance ) {
                self::$_instance = new self();
            }

            return self::$_instance;
        }
    }
}