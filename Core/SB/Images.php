<?php namespace SB {
/**
* @author		Alexander Tishchenko
* @copyright	Copyright (c) 2013 Alexander Tishchenko
* @package		Second-Breath PHP5 framework
* @filesource	Images.php
* @name			\SB\Images
*/
    class Images extends \stdClass
    {
        public function is_imagick_support()
        {
            return class_exists('\\Imagick');
        } 
    // -------------------------------------------------------------------------------------
        protected function is_type_support(\Imagick $image)
        {
            $cur_type = $image->getImageFormat();
            
            if( $cur_type != 'JPEG' && $cur_type != 'PNG' && $cur_type != 'GIF' ) {
                return false;
            }
            
            return true;
        }
    // -------------------------------------------------------------------------------------
        public function imagick_combine($original_image, $imposition_image, $result_image, $x = null, $y = null)
        {
            if( !is_file($original_image) || !is_file($imposition_image) ) {
                return false;
            }
            
            $original   = new \Imagick($original_image);
            $imposition = new \Imagick($imposition_image);
            
            $original->addImage($imposition);
    //        $original->set
        }
    // -------------------------------------------------------------------------------------
        public function imagick_resize_scaled($originalImage, $imageOut, $toWidth = 96, $toHeight = 96, $toType = null)
        {
            if( !$this->is_imagick_support() || !is_file($originalImage) ) {
                return false;
            }

            $image = new \Imagick($originalImage);
            $x     = $image->getImageWidth();
            $y     = $image->getImageHeight();
            
            if( $x <= $toWidth && $y <= $toHeight ) {
                $image->writeImage($imageOut);
                return true;
            }
            
            $xscale = $x / $toWidth;
            $yscale = $y / $toHeight;

            if( $yscale > $xscale ) {
                $new_width  = round($x * (1/$yscale));
                $new_height = round($y * (1/$yscale));
            } else {
                $new_width  = round($x * (1/$xscale));
                $new_height = round($y * (1/$xscale));
            }
            
            $image->resizeImage($new_width, $new_height, \Imagick::FILTER_LANCZOS, 1);
            
            if( !empty($toType) ) {
                switch( strtolower($toType) ) {
                    case 'image/jpeg': $image->setImageFormat('JPEG'); break;
                    case 'image/gif':  $image->setImageFormat('GIF'); break;
                    case 'image/png':  $image->setImageFormat('PNG');
                }
            }
            
            return $image->writeImage($imageOut);
        }
    // -------------------------------------------------------------------------------------
        /**
        * Get image information
        * 
        * @param string $fImage
        */
	    public function image_info($fImage)
	    {
	        if( is_file($fImage) ) {
	            $ret =  getimagesize($fImage);
                
                if(empty($ret[0]) && empty($ret[1])) {
                    return false;
                }
              
                return array(
                    'x'    => $ret[0],
                    'y'    => $ret[1],
                    'type' => $ret['mime'],
                    'html' => $ret[3],
                );
	        } else { 
	            return false;
	        }
	    }
	    
    // -------------------------------------------------------------------------------------
        /**
        * Resizing pictures with aspect ratio
        * with the ability to maintain transparency
        * 
        * @param string $originalImage
        * @param string $imageOut
        * @param integer $toWidth
        * @param integer $toHeight
        * @param string $toType
        * @param boolian $alfa
        */
	    public function resize_image_scaled($originalImage, $imageOut, $toWidth = 115, $toHeight = 75, $toType = null, $alfa = false)
	    {
	      if( is_file($originalImage) ) {
	          $img    = $this->image_info($originalImage);
	          $xscale = $img['x'] / $toWidth;
	          $yscale = $img['y'] / $toHeight;

	          if( $yscale > $xscale ) {
	              $new_width  = round($img['x'] * (1/$yscale));
	              $new_height = round($img['y'] * (1/$yscale));
	          } else {
	              $new_width  = round($img['x'] * (1/$xscale));
	              $new_height = round($img['y'] * (1/$xscale));
	          }

	          $imageResized = imagecreatetruecolor($new_width, $new_height);
	          
	          switch( $img['type'] ) {
	              case 'image/jpeg': $imageTmp = imagecreatefromjpeg($originalImage); break;
	              case 'image/gif':  $imageTmp = imagecreatefromgif($originalImage); break;
	              case 'image/png':  $imageTmp = imagecreatefrompng($originalImage);
	          }

	          if( $alfa and ($img['type'] == 'image/gif' or $img['type'] == 'image/png') ) {
			      imageAlphaBlending($imageResized, false); 
			      imageSaveAlpha($imageResized, true);
			      imageAlphaBlending($imageTmp, false);
		      }
	          
	          imagecopyresampled($imageResized, $imageTmp, 0, 0, 0, 0, $new_width, $new_height, $img['x'], $img['y']);

	          if( empty($toType) ) {
	              $toType = $img['type'];
	          }
	      
	          switch($toType) {
	              case 'image/jpeg': imagejpeg($imageResized, $imageOut, 95); break;
	              case 'image/gif':  imagegif($imageResized, $imageOut); break;
	              case 'image/png':  imagepng($imageResized, $imageOut);
	          }
	      }
	    }
    // -------------------------------------------------------------------------------------
        /**
        * The union of two images with the ability to maintain transparency
        * 
        * @param string $original_image
        * @param string $imposition_image
        * @param string $result_image
        * @param integer $x
        * @param string $y
        */
	    public function images_combine($original_image, $imposition_image, $result_image, $x = null, $y = null)
	    {
	      if( !is_file($original_image) or !is_file($imposition_image) ) {
		      return false;
	      }
	      
	      $original   = $this->image_info($original_image);
	      $imposition = $this->image_info($imposition_image);
	      
	      if( $original == false || $imposition == false ) {
		      return false;
	      }
	      
	      $o_img = imagecreatefrompng($original_image);
	      $i_img = imagecreatefrompng($imposition_image);
	      $d_img = imagecreatetruecolor($original['x'], $original['y']);
	      
	      imagealphablending($o_img, true);
	      imagealphablending($i_img, true);	
	      imagesavealpha($i_img, true);
	      imagealphablending($d_img, true);	
	      
	      if( $x === null || $y === null ) {
		      $start_x = round( ($original['x'] - $imposition['x']) / 2 );
		      $start_y = round( ($original['y'] - $imposition['y']) / 2 );
	      } else {
		      $start_x = (int)$x;
		      $start_y = (int)$y;
	      }
	      
	      imagecopy($d_img, $o_img, 0, 0, 0, 0, $original['x'], $original['y']);
	      imagecopy($d_img, $i_img, $start_x, $start_y, 0, 0, $imposition['x'], $imposition['y']);
	      
	      imagepng($d_img, $result_image);
	      
	      imagedestroy($o_img);
	      imagedestroy($i_img);
	      imagedestroy($d_img);
	    }
    }
}