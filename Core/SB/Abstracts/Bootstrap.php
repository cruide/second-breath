<?php namespace SB\Abstracts {
/**
* @author        Tishchenko Alexander
* @copyright     Copyright (c) 2014 All rights to Tishchenko A.
* @package		 Second-Breath PHP5 framework
* @filesource	 Bootstrap.php
* @name			 \SB\Abstracts\Bootstrap
*/

    abstract class Bootstrap extends \stdClass
    {
	    protected $Config;
	    
	    public function __construct()
	    {
		    $this->Config = load_ini('config');
	    }
        
        public function __destruct()
        {
            unset($this->Config);
        }
	    
	    abstract public function run();
    }
}