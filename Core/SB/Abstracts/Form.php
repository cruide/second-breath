<?php namespace SB\Abstracts {
/**
* @author        Tishchenko Alexander
* @copyright     Copyright (c) 2014 All rights to Tishchenko A.
* @package		 Second-Breath PHP5 framework
* @filesource	 Form.php
* @name			 \SB\Abstracts\Form
*/

  abstract class Form extends \SB\stdObject
  {
      protected
          $_file_name,
          $_stdin,
          $_template,
          $_ui,
          $_restored,
          $_autoescape,
          $Cookie,
          $Session,
          $Router,
          $_fields,
          $_identifier,
          $_strings,
          $I18n,
          $_action;
// ---------------------------------------------------------------------------------
      public function __destruct()
      {
          unset($this->Session, $this->Cookie, $this->I18n, $this->_fields);
          memory_clear();
      }
// ---------------------------------------------------------------------------------
      public function __construct()
      {
          $this->_fields     = new \SB\stdObject();
          $this->_stdin      = \SB\Input::Instance()->post('form');
          $this->I18n        = \SB\I18n::Instance();
          $this->Cookie      = \SB\Cookie::Instance();
          $this->Session     = \SB\Session::Instance();
          $this->Router      = \SB\Router::Instance();
          $this->_restored   = true;
          $this->_autoescape = true;
          $this->_identifier = md5( get_class($this) );

          $this->_strings = array (
              'empty'      => 'Must not be empty',
              'minlenght'  => 'Allowed at least %d characters',
              'maxlenght'  => 'Allowed a maximum of %d characters',
              'format'     => 'Incorrect format of the field',
              'no_element' => 'Field not defined',
          );

          if( isset($this->I18n->form_empty) ) { $this->_strings['empty'] = $this->I18n->form_empty;}
          if( isset($this->I18n->form_minlenght) ) { $this->_strings['minlenght'] = $this->I18n->form_minlenght;}
          if( isset($this->I18n->form_maxlenght) ) { $this->_strings['maxlenght'] = $this->I18n->form_maxlenght;}
          if( isset($this->I18n->form_format) ) { $this->_strings['format'] = $this->I18n->form_format;}
          if( isset($this->I18n->form_no_element) ) { $this->_strings['no_element'] = $this->I18n->no_element;}

          $form_path    = correct_directory_path( ROOT . DIR_SEP . get_class($this) );
          $module_path  = preg_replace('/\/Forms\/([a-z0-9\-_]+)$/i', '', $form_path);

		  $module_views = correct_directory_path( 
		      $module_path . DIR_SEP . 'Forms' . DIR_SEP . 'Views', 
		      true
		  );
		  
		  $m_name = $this->Router->get_module_name();
		  
		  $inteface_views = correct_directory_path( 
			  \SB\Theme::Instance()->get_theme_path() . DIR_SEP . (($m_name == 'Plugins') ? 'Plugins' : 'Modules') . DIR_SEP .
			  (($m_name == 'Plugins') ? $this->Router->get_plugin_name() : $this->Router->get_module_name()) .
			  DIR_SEP . 'Forms',
			  true
		  );

          $filename = $this->className() . '.' . \SB\Native::EXTENSION;

          if( is_file($inteface_views . $filename) ) {
          	  $this->_ui = new \SB\Native($inteface_views);
	      } else if( is_file($module_views . $filename) ) {
	      	  $this->_ui = new \SB\Native($module_views);
          } else {
			  throw new \SB\Exception\Form( 
                  $this->className() . ' form template not found',
                  500
              );
          }

		  $this->_file_name = $filename;
		  
		  if( method_exists($this, '_init') ) {
			  $this->_init();
		  }
		  
          $this->restore_state();
      }                                       
// ---------------------------------------------------------------------------------
      public function __set($name, $value)
      {
          if( isset($this->_fields->$name) ) {
              $this->_fields->$name->value = $value;
          }
      }
// ---------------------------------------------------------------------------------
      public function __get($name)
      {
          if( !isset($this->_fields->$name) ) {
              return null;
          }
          
          return $this->_fields->$name;
      }
// ---------------------------------------------------------------------------------
      public function __isset($name)
      {
          return isset($this->_fields->$name);
      }
// ---------------------------------------------------------------------------------
      abstract protected function _init();
// ---------------------------------------------------------------------------------
      public function set_values($values)
      {
          if( !empty($values) && is_array($values) ) {
              foreach($values as $key=>$val) {
                  if( isset($this->_fields->$key) ) {
                      $this->_fields->$key->value = $val;
                  }
              }
          }
          
          return $this;
      }
// ---------------------------------------------------------------------------------
      public function get_values_as_array()
      {
          $result = array();
          $elements = $this->_fields->toArray();
          
          if( !empty($elements) ) {
              foreach($elements as $key=>$val) {
                  $result[ $key ] = $this->_fields->$key->value;
              }
          }
          
          return $result;
      }
// ---------------------------------------------------------------------------------
      public function set_action($action)
      {
          if( !empty($action) ) {
              $this->_action = $action;
          }
          
          return $this;
      }
// ---------------------------------------------------------------------------------
      public function fetch()
      {
          if( !empty($this->_stdin) ) {
              foreach($this->_stdin as $key=>$val) {
                  if( isset($this->_fields->$key) ) {
                      $this->_fields->$key->value = ($this->_fields->$key->escaped) ? escape($val) : $val;
                  }
              }
          }

          $form = new \SB\stdObject( array(
              'fields' => $this->_fields,
              'action' => make_url($this->_action),
              'id'     => $this->_identifier,
              'name'   => $this->className(),
          ));
          
          $this->_ui->assign('Form', $form);

          $render = $this->_ui->fetch( $this->_file_name );
          return preg_replace('/\<\/form\>/i', "<input type=\"hidden\" name=\"form_identifier\" value=\"{$this->_identifier}\" />\n</form>\n", $render);
      }
// ---------------------------------------------------------------------------------
      public function set_field($name, $validator, $minlen = 0, $maxlen = 254, $required = false, $def = '', $escaped = true)
      {
          if( !empty($name) && !empty($validator) ) {
              
              $element = new \SB\stdObject();
              $element->validator = $validator;
              $element->minlength = $minlen;
              $element->maxlength = $maxlen;
              $element->required  = $required;
              $element->message   = '';
              $element->default   = $def;
              $element->escaped   = $escaped;
              $element->value     = null;

              $this->_fields->$name = $element;
          }
          
          return $this;
      }
// ---------------------------------------------------------------------------------
      public function escaped_all( $auto = false )
      {
          if( $auto === true ) {
              foreach($this->_fields as $key=>$val ) {
                  $this->_fields->$key->escaped = true;
              }
          }
          
          return $this;
      }
// ---------------------------------------------------------------------------------
      public function set_escaped($name, $escaped = true)
      {
          if( isset($this->_fields->$name) && is_bool($escaped) ) {
              $this->_fields->$name->escaped = $escaped;
          }
      }
// ---------------------------------------------------------------------------------
      public function assign($key, $value = '')
      {
          $this->_ui->assign($key, $value);
          return $this;
      }
// ---------------------------------------------------------------------------------
      public function set_default_value($name, $str)
      {
          if( isset($this->_fields->$name) ) {
              $this->_fields->$name->default = $str;
          }
          
          return $this;
      }
// ---------------------------------------------------------------------------------
      public function get_default_value($name)
      {
          if( isset($this->_fields->$name) ) {
              return $this->_fields->$name->default;
          }
          
          return null;
      }
// ---------------------------------------------------------------------------------
      public function set_stdin_value($name, $value)
      {
          $this->_stdin[ $name ] = $value;
          return $this;
      }
// ---------------------------------------------------------------------------------
      public function get_stdin_value($name)
      {
          if( !isset($this->_stdin[$name]) ) {
              return null;
          }
          
          return $this->_stdin[$name];
      }
// ---------------------------------------------------------------------------------
      public function set_required($name, $required = false)
      {
          if( isset($this->_fields->$name) ) {
              return $this->_fields->$name->required = $required;
          }
          
          return $this;
      }
// ---------------------------------------------------------------------------------
      public function check_all()
      {
          if( empty($this->_stdin) ) {
              return false;
          }
          
          if( empty($this->_fields) ) {
              throw new \SB\Exception\Form(
                  $this->className() . '::check_all -> No declaration of the form elements'
              );
          }
          
          $identifier = \SB\Input::Instance()->post('form_identifier');
          if( $identifier != $this->_identifier ) {
              return false;
          }
          
          $result = true;
          $_array = $this->_fields->toArray();

          foreach($_array as $key=>$val) {
              if( !$this->check($key) ) {
                  $result = false;
              }
          }
          
          $this->_ui->assign( 'errors_exist', (!$result) ? true : false );
          
          return $result;
      }
// ---------------------------------------------------------------------------------
      public function check($name)
      {
          if( !isset($this->_fields->$name) ) {
              return false;
          }

          $result = true;
          $value  = (isset($this->_stdin[$name])) ? $this->_stdin[$name] : null;

          if( !isset($this->_fields->$name) ) {
              $this->_fields->$name->message = $this->_strings['no_element'];
              return false;
          }
          
          $element = $this->_fields->$name;

          if( ($value == '' or $value === null) && $element->required == true ) {
              $element->message = $this->_strings['empty'];
              $result = false;
          } else if( ($value == '' or $value === null) && $element->required != true ) {
              return true;
          } else {
              if( sb_strlen($value) < $element->minlength ) {
                  $element->message = sprintf($this->_strings['minlenght'], $element->minlength);
                  $result = false;
              } else if( sb_strlen($value) > $element->maxlength ) {
                  $element->message = sprintf($this->_strings['maxlenght'], $element->maxlength);
                  $result = false;
              } else {
                  if( !\SB\Match::is_match($value, $element->validator) ) {
                      $element->message = $this->_strings['format'];
                      $result = false;
                  }
              }
          }
          
          if( $result ) {
              $element->value = $value;
          }
          
          return $result;
      }
// ---------------------------------------------------------------------------------
      public function set_error_message($errName, $errMessage)
      {
          if( empty($errName) or empty($errMessage) or !in_array($name, $this->_strings) ) {
              return $this;
          }
          
          $this->_strings[ $name ] = $errMessage;
          return $this;
      }
// ---------------------------------------------------------------------------------
      public function save_state()
      {
          $state = array (
              'stdin'  => serialize($this->_stdin),
              'fields' => serialize($this->_fields),
          );

          if( $this->_restored ) {
      		  $session_id = $this->Session->id();
      		  $tmp_name   = str_replace('\\', '_', get_class($this)) . "_{$session_id}.tmp";
              $tmp        = new \SB\Temp($tmp_name, TEMP_DIR);
              
              $tmp->set_content($state)->write();
          }
      }
// ---------------------------------------------------------------------------------
      public function restore_state()
      {
      	  $session_id = $this->Session->id();
      	  $tmp_name   = str_replace('\\', '_', get_class($this)) . "_{$session_id}.tmp";
          $tmp        = new \SB\Temp($tmp_name, TEMP_DIR);
          $tmp_data   = $tmp->get_content();
          
          if( !empty($tmp_data) && is_array($tmp_data) ) {
              $this->_stdin  = unserialize($tmp_data['stdin']);
              $this->_fields = unserialize($tmp_data['fields']);
          }
          
          $tmp->delete();
      }
// ---------------------------------------------------------------------------------
      public function restored_off()
      {
          $this->_restored = false;
          return $this;
      }
// ---------------------------------------------------------------------------------
  }
}