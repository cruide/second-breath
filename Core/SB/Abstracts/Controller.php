<?php namespace SB\Abstracts {
/**
* @author        Tishchenko Alexander
* @copyright     Copyright (c) 2014 All rights to Tishchenko A.
* @package		 Second-Breath PHP5 framework
* @filesource	 Controller.php
* @name			 \SB\Abstracts\Controller
*/

  abstract class Controller extends \SB\stdObject
  {
  	  protected
  	      $Session,
          $Cookie,
          $Models,
  	      $Config,
  	      $I18n,
  	      $Router,
  	      $Input,
  	      $Layout,
  	      $Ui;
// ---------------------------------------------------------------------------------
	  public function __construct()
	  {
          $this->Config  = load_ini('config');
          $this->Session = \SB\Session::Instance();
          $this->Cookie  = \SB\Cookie::Instance();
		  $this->Router  = \SB\Router::Instance();
		  $this->Input   = \SB\Input::Instance();
		  $this->Layout  = \SB\Theme::Instance();
          
          $this->Models  = new \SB\stdObject();
          
		  $current_controller = 'Application\\Modules\\' . $this->Router->get_module_name() . 
                                '\\Controllers\\' . $this->Router->get_controller_name();
                                
		  if( $current_controller != get_class($this) ) {
			  throw new \SB\Exception\Controller('This distortion is unacceptable!', 500);
		  }
		  
		  $m_name       = $this->Router->get_module_name();
		  $module_views = correct_directory_path( 
		      MODULES_DIR . DIR_SEP . $m_name . DIR_SEP . 'Views'
		  );

		  $inteface_views = correct_directory_path( 
			  $this->Layout->get_theme_path() . DIR_SEP . 'Modules' . DIR_SEP . $m_name,
			  true
		  );
		  
          if( is_dir($inteface_views) ) {
          	  $this->Ui = new \SB\Native($inteface_views);
	      } else if( is_dir($module_views) ) {
	      	  $this->Ui = new \SB\Native($module_views);
          } else {
              if( !mkdir($module_views, 0755) ) {
                  throw new \SB\Exception\Controller( 
                      $this->className() . ' Unable to createiInterface directory', 
                      500
                  );
              }
              
              $this->Ui = new \SB\Native($module_views);
          }
		  
          $this->I18n = \SB\I18n::Instance();
          $module_url = BASE_URL . '/Application/Modules/' . $this->Router->get_module_name();
          
          if( is_object($this->Ui) ) {
              \SB\Native::assign_global('self_module',  new \SB\stdObject( array(
                  'name'           => $this->Router->get_module_name(),
                  'controller'     => $this->Router->get_controller_name(),
                  'method'         => $this->Router->get_method_name(),
                  'class'          => get_class($this),
                  'module_url'     => sb_strtolower( BASE_URL . '/' . $this->Router->get_module_name() ),
                  'controller_url' => sb_strtolower( BASE_URL . '/' . $this->Router->get_module_name() . '/' . $this->Router->get_controller_name() ),
                  'assets_url'     => $module_url . '/Assets',
                  'images_url'     => $module_url . '/Assets/Images',
                  'js_url'         => $module_url . '/Assets/Js',
              )));
          }

          if( isset($this->Config->application['models_preload']) && $this->Config->application['models_preload'] == false ) {
              return;
          }

          $models_dir   = MODULES_DIR . DIR_SEP . $this->Router->get_module_name() . DIR_SEP . 'Models';
          $class_prefix = "\\Application\\Modules\\" . $this->Router->get_module_name() . "\\Models\\";
          
          if( is_dir($models_dir) ) {
              $models = get_files_list($models_dir, array('php'));
              if( array_count($models) > 0 ) {
                  foreach($models as $key=>$val) {

                      $models_name = str_replace( '.php', '', $key );
                      $class_name  = $class_prefix . $models_name;

                      if( class_exists($class_name) ) {
                          $this->Models->$models_name = new $class_name();
                      } else {
                          throw new \SB\Exception\Controller(
                              "Incorrect model class name: {$class_name}"
                          );
                      }
                      
                  }
              }
          }
      }
// ---------------------------------------------------------------------------------
      public function getUi()
      {
          return $this->Ui;
      }
// ---------------------------------------------------------------------------------
      public function getLayout()
      {
          return $this->Layout;
      }
// ---------------------------------------------------------------------------------
      abstract public function default_Action();
  }
}