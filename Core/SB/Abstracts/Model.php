<?php namespace SB\Abstracts {
/**
* @author        Tishchenko Alexander
* @copyright     Copyright (c) 2014 All rights to Tishchenko A.
* @package		 Second-Breath PHP5 framework
* @filesource	 Model.php
* @name			 \SB\Abstracts\Model
*/

  abstract class Model extends \Illuminate\Database\Eloquent\Model
  {
      
  }
  
}