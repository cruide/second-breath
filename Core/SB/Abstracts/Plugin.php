<?php namespace SB\Abstracts {
/**
* @author        Tishchenko Alexander
* @copyright     Copyright (c) 2014 All rights to Tishchenko A.
* @package		 Second-Breath PHP5 framework
* @filesource	 Plugin.php
* @name			 \SB\Abstracts\Plugin
*/

  abstract class Plugin extends \SB\stdObject
  {
  	  protected static
  	      $plugins;
  	  
  	  protected
  	      $Name,
          $direct_request,
  	      $Session,
          $Cookie,
  	      $Config,
  	      $I18n,
  	      $Router,
  	      $Params,
  	      $Input,
  	      $Theme,
  	      $Ui;
// ---------------------------------------------------------------------------------
	  public function __construct( $params = array() )
	  {
	  	  $this->Name   = $this->className();
	  	  $this->Config = load_ini('config');
          $this->Cookie = \SB\Cookie::Instance();
	  	  
	  	  if( empty($this->Name) ) {
			  throw new \SB\Exception\Plugin('Incorrect plugin class name');
	  	  }

	  	  $this->Params = array();
	  	  if( array_count($params) > 0 ) {
			  foreach($params as $key=>$val) {
				  $this->Params[ $key ] = $val;
			  }
	  	  }
	  	  
	  	  if( self::$plugins === null ) {
			  self::$plugins = array();
	  	  }
	  	  
	  	  if( !in_array($this->Name, self::$plugins) ) {
			  if( \SB\I18n::Instance()->load_plugin_strings($this->Name) ) {
				  self::$plugins[] = $this->Name;
			  }
	  	  }
	  	  
          $this->Session   = \SB\Session::Instance();
		  $this->Router    = \SB\Router::Instance();
		  $this->Input     = \SB\Input::Instance();
          $this->I18n      = \SB\I18n::Instance();

          $plugin_views    = correct_directory_path(
              PLUGINS_DIR . DIR_SEP . $this->Name . DIR_SEP . 'Views'
          );

          $interface_views = correct_directory_path(
              \SB\Theme::Instance()->get_theme_path() . DIR_SEP . 'Plugins' . DIR_SEP . $this->Name
          );

          $this->Ui = new \SB\Native( 
              (is_dir($interface_views)) ? $interface_views : $plugin_views 
          );
		  
          $module = sb_strtolower( $this->Router->get_module_name() );
          $ctrl   = sb_strtolower( $this->Router->get_controller_name() );

          if( $module == 'plugins' and $ctrl == sb_strtolower( $this->Name ) ) {
              $this->Ui->assign('direct_request', true);
              $this->direct_request = true;
          } else {
              $this->Ui->assign('direct_request', false);
              $this->direct_request = false;
          }
          
          if( is_object($this->Ui) ) {
              $this->Ui->assign('self_plugin',  new \SB\stdObject( array(
                  'name'       => $this->Name,
                  'url'        => PLUGINS_URL . '/' . $this->className(),
                  'action'     => BASE_URL . '/plugins/' . sb_strtolower( $this->className() ),
                  'id'         => md5( $this->classId() . time() ),
                  'class'      => get_class($this),
                  'assets_url' => PLUGINS_URL . '/' . $this->className() . '/Assets',
                  'images_url' => PLUGINS_URL . '/' . $this->className() . '/Assets/Images',
                  'css_url'    => PLUGINS_URL . '/' . $this->className() . '/Assets/Css',
                  'js_url'     => PLUGINS_URL . '/' . $this->className() . '/Assets/Js',
              )));
          }
      }
// ---------------------------------------------------------------------------------
      public function get_plugin_name()
      {
		  return $this->Name;
      }
// ---------------------------------------------------------------------------------
      abstract public function content();
// ---------------------------------------------------------------------------------
      public function __destruct()
      {
          unset($this->Config, $this->Cookie, $this->I18n, $this->Input);
          unset($this->Router, $this->Session, $this->Theme, $this->Ui);
      }
  }
}