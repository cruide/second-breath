<?php namespace SB\Abstracts {

    abstract class Service extends \SB\stdObject 
    {
        protected $Config, $db, $Session, $Router, $Input;
// ---------------------------------------------------------------------------------
        public function __construct()
        {
            $this->Config  = load_ini('config');

            $this->Cookie  = \SB\Cookie::Instance();
            $this->Router  = \SB\Router::Instance();
            $this->Input   = \SB\Input::Instance();
            $this->Session = \SB\Session::Instance();
            $this->db      = \SB\Base\DB_Joint::Instance();
            
            if( method_exists($this, '_init') ) {
                $this->_init();
            } 
        }
// ---------------------------------------------------------------------------------
        abstract public function Execute();
// ---------------------------------------------------------------------------------
        public function __destruct()
        {
            if( isset($this->db) ) { unset($this->db);}
            unset($this->Config, $this->Cookie, $this->Session, $this->Input, $this->Router);
        }
    }
}