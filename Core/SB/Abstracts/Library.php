<?php namespace SB\Abstracts {

    class Library extends \SB\stdObject
    {
        protected $db, $Config, $Input, $Router, $Session, $Cookie;
        
        public function __construct()
        {
            $this->Config  = load_ini('config');
          
            $this->Router  = \SB\Router::Instance();
            $this->Input   = \SB\Input::Instance();
            $this->Cookie  = \SB\Cookie::Instance();
            $this->Session = \SB\Session::Instance();
            $this->db      = \SB\Base\DB_Joint::Instance();

            if( method_exists($this, '_init') ) {
                $this->_init();
            }
        }
        
        public function __destruct()
        {
            if( isset($this->db) ) { unset($this->db);}
            unset($this->Config, $this->Cookie, $this->Session, $this->Input, $this->Router);
        }
    }
}