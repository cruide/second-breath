<?php namespace SB {
/**
* @author        Tishchenko Alexander
* @copyright     Copyright (c) 2014 All rights to Tishchenko A.
* @package		 Second-Breath PHP5 framework
* @filesource	 Kernel.php
* @name			 \SB\Kernel
*/

  final class Kernel extends \stdClass
  {
      public static $controller;
      private static $_instance;
// -------------------------------------------------------------------------------------
     /**
     * Initailization App
     * 
     */
      public function __construct()
      {
          $router = \SB\Router::Instance();

      	  if( is_file(LIBRARY_DIR . DIR_SEP . 'Bootstrap.php') ) {
			  $bootstrap = new \Application\Library\Bootstrap();
			  $bootstrap->run();
			  unset($bootstrap);
      	  }
      	  
      	  $m_name = $router->get_module_name();
      	  $c_name = $router->get_controller_name();

      	  if( $m_name == 'Plugins' ) {
			  $className = "\\Application\\Plugins\\{$c_name}\\{$c_name}";

              try {
                  self::$controller = new $className();
              } catch( \SB\Exception\SB_Exception $e ) {
                  system_exception( $e );
              }

              if( !(self::$controller instanceof \SB\Abstracts\Plugin) ) {
                  throw new \SB\Exception\SB_Exception(
                      "Class `{$className}` not inherits from `\\SB\\Abstracts\\Plugin`"
                  );
              }
              
			  return;
      	  } else {
			  $className = "\\Application\\Modules\\{$m_name}\\Controllers\\{$c_name}";
      	  }
      	  
      	  if( !is_controller_exists($m_name, $c_name) ) {
			  show_error_404();
      	  }

          try {
      	      self::$controller = new $className();
          } catch( \SB\Exception\SB_Exception $e ) {
              system_exception( $e );
          }
          
          if( !(self::$controller instanceof \SB\Abstracts\Controller ) ) {
              throw new \SB\Exception\SB_Exception(
                  "Class `{$className}` not inherits from `\\SB\\Abstracts\\Controller`"
              );
          }
      }
// -------------------------------------------------------------------------------------
      public function Execute()
      {
          \SB\Spiders::Instance();
          $this->_execute_services();
          memory_clear();

          $auth   = \SB\Authorization::Instance();
          $access = \SB\Access::Instance();
          
          if( $auth->is_auth() ) {
              if( ($auth_user_group = $auth->get_auth_user_group()) === false ) {
                  $auth_user_group = 6;
              }
          } else {
              $auth_user_group = 6;
          }

          $module_name     = \SB\Router::Instance()->get_module_name();
          $controller_name = \SB\Router::Instance()->get_controller_name();
          $method_name     = \SB\Router::Instance()->get_method_name();
          $bootstrap       = "\\Application\\Modules\\{$module_name}\\Library\\Bootstrap";
          
          if( is_file(correct_directory_path(MODULES_DIR . DIR_SEP . $module_name . DIR_SEP . 'Library' , true) . 'Bootstrap.php') ) {
              $bs = new $bootstrap();
              $bs->run();
              unset($bs, $bootstrap);
          }

          $this->_set_global_varibles();
          
      	  if( self::$controller instanceof \SB\Abstracts\Plugin ) {
              if( !empty($access) && !$access->access_check('Plugins', $controller_name, 'content', $auth_user_group) ) {
                  show_error_403();
              }

              try {
                  if( method_exists(self::$controller, 'headers') && !headers_sent() ) {
                      self::$controller->headers();
                  }
                  
                  if( !\SB\Cookie::is_saved() ) {
                      \SB\Cookie::Instance()->save();
                  }
                  
                  if( method_exists(self::$controller, '_before') ) {
                      self::$controller->_before();
                  }
              
                  echo self::$controller->content();

                  if( method_exists(self::$controller, '_after') ) {
                      self::$controller->_after();
                  }
              
              } catch(\SB\Exception\SB_Exception $e) {
                  system_exception( $e );
              }
			  
			  return;
      	  }
      	  
      	  if( !method_exists(self::$controller, $method_name . '_Action')
           && !method_exists(self::$controller, $method_name . '_ActionPost')
           && !method_exists(self::$controller, $method_name . '_ActionAjax') ) 
          {
			  show_error_404();
      	  }

          $less_files_dir = self::$controller->getLayout()->get_theme_path() . DIR_SEP . 'Less';
          
          if( is_dir($less_files_dir) ) {
              $less_files = get_files_list( $less_files_dir, ['less'] );
              
              if( array_count($less_files) > 0 ) {
                  
                  require_once( SB_PATH . DIR_SEP . 'SB' . DIR_SEP . 'Thirdparty' . DIR_SEP . 'lessc.inc.php' );
                  
                  $less_obj = new \lessc();
                  $css_dir  = self::$controller->getLayout()->get_theme_path() . DIR_SEP . 'Css';
                  
                  if( !is_dir($css_dir) ) {
                      @mkdir($css_dir, 0777);
                  }
                  
                  foreach($less_files as $key=>$val) {
                      $less_obj->checkedCompile(
                          $less_files_dir . DIR_SEP . $key,
                          $css_dir . DIR_SEP . $key . '.css'
                      );
                  }
              }
          }

          if( !empty($access) && !$access->access_check($module_name, $controller_name, $method_name, $auth_user_group) ) {
              show_error_403();
          }
              
      	  try {
              
              if( method_exists(self::$controller, '_before') ) {
                  self::$controller->_before();
              }
              
              $method_name = $method_name . '_Action';
              
              if( \SB\Input::Instance()->is_post() && method_exists(self::$controller, $method_name . 'Post') ) {
                  $method_name = $method_name . 'Post';
              } else if( is_ajax() && method_exists(self::$controller, $method_name . 'Ajax') ) {
                  $method_name = $method_name . 'Ajax';
              }

              $content = call_user_func([
                  self::$controller,
                  $method_name 
              ]);
              
              if( method_exists(self::$controller, '_after') ) {
                  self::$controller->_after();
              }
              
              if( is_ajax() ) {
                  self::$controller->getLayout()->disable();
              }

			  self::$controller->getLayout()->display( $content );
              
      	  } catch( \Exception $e ) {
			  system_exception( $e );
      	  }
      }
// -------------------------------------------------------------------------------------
      private function _execute_services()
      {
          if( is_dir(SERVICES_DIR) ) {
              $services    = get_files_list( SERVICES_DIR, ['php'] );
              $services_ns = "\\Application\\Services\\";
              
              if( array_count($services) > 0 ) {
                  ksort( $services );

                  foreach($services as $key=>$val) {
                      $srv_name  = str_replace( '.php', '', $key );
                      $srv_class = $services_ns . $srv_name;

                      try {
                          $tmp = new $srv_class();
                      } catch( \SB\Exception\SB_Exception $e ) {
                          system_exception( $e );
                      }
                      
                      if( $tmp instanceof \SB\Abstracts\Service ) {
                          $tmp->Execute();
                      } else {
                          throw new \SB\Exception\Service(
                              'Incorrect instance of service class ' . $tmp->className()
                          );
                      }
                      
                      unset($tmp);
                  }
              }
          }
      }
// -------------------------------------------------------------------------------------
      private function _set_global_varibles()
      {
          \SB\Native::assign_global('base_url'     , BASE_URL);
          \SB\Native::assign_global('modules_url'  , MODULES_URL);
          \SB\Native::assign_global('plugins_url'  , PLUGINS_URL);
          \SB\Native::assign_global('content_url'  , CONTENT_URL);
          \SB\Native::assign_global('interface_url', INTERFACE_URL);
          \SB\Native::assign_global('theme_url'    , \SB\Theme::Instance()->get_theme_url());
          \SB\Native::assign_global('css_url'      , \SB\Theme::Instance()->get_theme_url().'/Css');
          \SB\Native::assign_global('js_url'       , \SB\Theme::Instance()->get_theme_url().'/Js');
          \SB\Native::assign_global('assets_url'   , \SB\Theme::Instance()->get_theme_url().'/Assets');
          \SB\Native::assign_global('images_url'   , \SB\Theme::Instance()->get_theme_url().'/Images');
          \SB\Native::assign_global('core_name'    , CORE_NAME);
          \SB\Native::assign_global('core_version' , CORE_VERSION);
          \SB\Native::assign_global('core_ver_name', CORE_VERSION_NAME);
          \SB\Native::assign_global('framework'    , FRAMEWORK);
          \SB\Native::assign_global('timer_varible', time());
          \SB\Native::assign_global('common_url'   , INTERFACE_COMMON_URL);
          \SB\Native::assign_global('current_uri'  , \SB\Router::Instance()->get_original_uri());

          $cfg = load_ini('config')->application;
          if( !empty($cfg['url_suffix']) ) {
              \SB\Native::assign_global('url_suffix', (string)$cfg['url_suffix']);
          } else {
              \SB\Native::assign_global('url_suffix', '.html');
          }
                    
		  $msg = pick_temp( 
		      make_temp_name_from_uri(
		          \SB\Router::Instance()->get_original_uri()
		  ));
		  
          if( null !== $msg ) {
	          \SB\Native::assign_global('redirector', new \SB\stdObject([
                  'message' => $msg['message'],
                  'error'   => $msg['error'],                 
              ]));
		  }
      }
// -------------------------------------------------------------------------------------
      public function getControllerUi()
      {
		  return self::$controller->getUi();
      }
// -------------------------------------------------------------------------------------
      public static function Terminate( $msg = null )
      {
          exit($msg);
      }
// -------------------------------------------------------------------------------------
      public function __destruct()
      {
          \SB\Log::Instance()->write();
          self::$controller = null;
      }
// -------------------------------------------------------------------------------------
      public static function Instance()
      {
          if( null === self::$_instance ) {
              self::$_instance = new self();
          }
 
          return self::$_instance;
      }
  }
}