<?php namespace SB {
/**
* @author        Alexander Tishchenko
* @copyright     Copyright (c) 2013 Tishchenko A.
* @package       Second-Breath PHP5 framework
* @filesource    Phpmail.php
* @name          \SB\Phpmail
*/

    class Phpmail extends \SB\Abstracts\Library
    {
        protected $phpmail, $settings, $Ui;
// -------------------------------------------------------------------------------------
        public function __construct($exceptions = false)
        {
            $this->settings = load_ini('phpmailer');
            
            if( !class_exists('\\PHPMailer') ) {
                require_once( 
                    SB_PATH . DIR_SEP . 'SB' . DIR_SEP . 'Thirdparty' . 
                    DIR_SEP . 'phpmailer' . DIR_SEP . 'class.phpmailer.php' 
                );
            }
            
            $this->phpmail = new \PHPMailer($exceptions);
            $this->_prepare();

            $mails_path = get_work_path() . DIR_SEP . 'Mails';
            $this->Ui   = new \SB\Native( $mails_path );
        }
// -------------------------------------------------------------------------------------
        public function Subject($subject)
        {
            $this->phpmail->Subject = htmlspecialchars( $subject );
            return $this;
        }
// -------------------------------------------------------------------------------------
        public function assign($tpl_var, $value = null, $nocache = false)
        {
            $this->Ui->assign($tpl_var, $value, $nocache);
            return $this;
        }
// -------------------------------------------------------------------------------------
        public function Send($template)
        {
            $this->phpmail->MsgHTML(
                $this->Ui->fetch($template)
            );

            try {
                $_ = $this->phpmail->send();
            } catch( \SB\Exception\Phpmail $e ) {
                system_exception( $e );
            }
            
            return $_;
        }
// -------------------------------------------------------------------------------------
        protected function _prepare()
        {
            if( !empty($this->settings->email_from) ) {
                $this->phpmail->SetFrom(
                    $this->settings->email_from,
                    (isset($this->settings->from_name)) ?
                        $this->settings->from_name : ''
                );
            }

            if( !empty($this->settings->replyto) ) {
                $this->phpmail->AddReplyTo( $this->settings->replyto );
            }

            if( !empty($this->settings->host) ) {
                $this->phpmail->Host = $this->settings->host;
            }
            
            if( !empty($this->settings->port) ) {
                $this->phpmail->Port = $this->settings->port;
            }
            
            if( !empty($this->settings->username) ) {
                $this->phpmail->Username = $this->settings->username;

                if( !empty($this->settings->password) ) {
                    $this->phpmail->Password = $this->settings->password;
                    $this->phpmail->SMTPAuth = true;
                }
            }
            
            if( !empty($this->settings->authtype) ) {
                $this->phpmail->AuthType = $this->settings->authtype;
            }
            
            if( !empty($this->settings->type) ) {
                switch($this->settings->type) {
                    case 'sendmail': 
                       if( isset($this->settings->sendmail) ) { 
                           $this->phpmail->Sendmail = (string)$this->settings->sendmail; 
                       }
                       
                       $this->phpmail->IsSendmail(); 
                       break;
                       
                    case 'smtp':     $this->phpmail->IsSMTP(); break;
                    case 'qmail':    $this->phpmail->IsQmail(); break;
                    default:         $this->phpmail->IsMail();
                }
            } else {
                $this->phpmail->IsMail();
            }
/*            
            if( DEVELOP_MODE ) {
                $this->phpmail->SMTPDebug = 2;
            }
*/            
            $this->phpmail->XMailer = FRAMEWORK;
            $this->phpmail->CharSet = 'utf-8';
        }
// -------------------------------------------------------------------------------------
        public function __call($name, $args)
        {
            if( method_exists($this->phpmail, $name) ) {
                return call_user_func_array(
                    array($this->phpmail, $name),
                    $args
                );
            } else {
                throw new \SB\Exception\Phpmail(
                    "Call to undefined method {$name}"
                );
            }
        }
    }
}