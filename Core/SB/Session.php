<?php namespace SB {
/**
* @author        Tishchenko Alexander
* @copyright     Copyright (c) 2014 All rights to Tishchenko A.
* @package		 Second-Breath PHP5 framework
* @filesource	 Session.php
* @name			 \SB\Session
*/

  class Session extends \stdClass
  {       
      protected $session, $properties, $session_time, $db;
      private static $_instance;
// -------------------------------------------------------------------------------------
      public function __construct()
      {
          $config   = load_ini('config')->application;
          $this->db = \SB\Base\DB_Joint::Instance();
          
          if( !empty($config['session_time']) ) {
              $this->session_time = (int)$config['session_time'];
          } else {
              $this->session_time = 1800;
          }
          
          $this->_kill_old_sessions();
          $this->start();
      }
// -------------------------------------------------------------------------------------
      protected function _kill_old_sessions()
      {
          $this->db->prepare(
              'DELETE FROM `tab_core_sessions` WHERE `timestamp` < ' . ( time() - $this->session_time )
          )->execute();
      }
// -------------------------------------------------------------------------------------
      public function start()
      {
          global $_COOKIE;
          
          $ipaddr        = get_ip_address();
          $this->session = new \SB\Base\tObject('tab_core_sessions');

          if( isset($_COOKIE['SBSID']) && is_uuid($_COOKIE['SBSID']) ) {
              $this->session->get( array(
                  'sid'    => $_COOKIE['SBSID'],
                  'ipaddr' => $ipaddr,
              ));
          }
          
          if( empty($this->session->sid) ) {
              $this->session->sid        = uuid();
              $this->session->ipaddr     = $ipaddr;
              $this->session->data       = serialize( array() );
              
              setcookie('SBSID', $this->session->sid, time() + 86400, '/');
          }
          
          $this->properties = unserialize( $this->session->data );
          
          $this->session->timestamp  = time();
          $this->session->save();
      }
// -------------------------------------------------------------------------------------
      protected function _save_session()
      {
          $this->session->data = serialize( $this->properties );
          $this->session->save();
      }
// -------------------------------------------------------------------------------------
      public function get($name)
      {
          return ( array_key_isset($name, $this->properties) ) ? $this->properties[ $name ] : null;
      }   
// -------------------------------------------------------------------------------------
      public function set($name, $value = null)
      {
          if( \SB\Match::is_varible_name($name) ) {
              if( $value === null ) {
                  $this->remove( $name );
              } else {
                  $this->properties[ $name ] = $value;
                  $this->_save_session();
              }
          }
          
          return $this;
      }
// -------------------------------------------------------------------------------------
      public function remove($name)
      {
          if( \SB\Match::is_varible_name($name) && array_key_isset($name, $this->properties) ) {
              unset( $this->properties[ $name ] );
              $this->_save_session();
          }
      }
// -------------------------------------------------------------------------------------
      public function id( $hash = false )
      {
          return ($hash) ? md5($this->session->sid) : $this->session->sid;
      }
// -------------------------------------------------------------------------------------
      public function __set($name, $value)
      {
          $this->set($name, $value);
      }
// -------------------------------------------------------------------------------------
      public function __get($name)
      {
      	  return $this->get($name);
      }
// -------------------------------------------------------------------------------------
      public function __isset($name)
      {
          return array_key_isset( $name, $this->properties );
      }
// -------------------------------------------------------------------------------------
      public function destroy()
      {
		  $this->db->prepare(
              "DELETE FROM `tab_core_sessions` WHERE `sid` = '{$this->session->sid}'"
          )->execute();
          
          $this->properties = array();
          $this->session    = null;
          
          setcookie('SBSID', '', time() - 86400, '/');
      }
// -------------------------------------------------------------------------------------
      public static function Instance()
      {
          if( null === self::$_instance ) {
              self::$_instance = new self();
          }
 
          return self::$_instance;
      }
  }
}