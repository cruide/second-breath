<?php namespace SB {
/**
* @author        Tishchenko Alexander
* @copyright     Copyright (c) 2014 All rights to Tishchenko A.
* @package		 Second-Breath PHP5 framework
* @filesource	 Uploads.php
* @name			 \SB\Uploads
*/

  class Uploads extends \stdClass
  {
      protected
          $_destination_dir,
          $_bad_uploads,
          $_result_uploads,
          $_max_file_size,
          $_max_image_width,
          $_max_image_height,
          $_images_count,
          $_audios_count,
          $_medias_count,
          $_files_count,
          $_files;
          
      public
          $check_inject = false;

      private static
          $_instance;
// -------------------------------------------------------------------------------------
      public function __construct()
      {
          $this->_files            = array();
          $this->_bad_uploads      = array();
          $this->_result_uploads   = array();

          $this->_destination_dir  = CONTENT_DIR;

          $this->_max_file_size    = (int)ini_get('upload_max_filesize');
          $this->_max_image_width  = 1280;
          $this->_max_image_height = 1024;
          
          $this->_images_count     = 0;
          $this->_medias_count     = 0;
          $this->_audios_count     = 0;
          $this->_files_count      = 0;

          if( !is_dir($this->_destination_dir) ) {
              if( !mkdir($this->_destination_dir, 0755, true) ) {
                  throw new \SB\Exception\Uploads( 
                      $this->className() . '::__construct - I can not create directory for uploads'
                  );
              }
          }
          
          $cfg = load_ini('config')->uploads;
          if( array_count($cfg) > 0 ) {
              foreach($cfg as $key=>$val) {
                  $varible = '_' . $key;
                  $this->$varible = $val;
              }
              
              unset( $key, $val, $varible );
          }
          
          unset( $cfg );

          $this->_check_uploads();
      }
// -------------------------------------------------------------------------------------
      protected function _check_uploads()
      {
          $files = \SB\Input::Instance()->files();
          
          if( empty($files) || !is_array($files) ) {
              return;
          }

          foreach($files as $key=>$val) {

              if( !is_uploaded_file($val['tmp_name']) || !$this->_checkSize($val['size']) ) {
                  $this->_bad_uploads[ $key ]            = $val;
                  $this->_bad_uploads[ $key ]['message'] = 'Exceeded file size';
                  $this->_bad_uploads[ $key ]['typeof']  = 'bad';
                  
                  continue;
              }

              if( !$this->_checkFileName($val['name']) ) {
                  $this->_bad_uploads[ $key ]            = $val;
                  $this->_bad_uploads[ $key ]['message'] = 'Incorrect file name';
                  $this->_bad_uploads[ $key ]['typeof']  = 'bad';
                  
                  continue;
              }

              $this->_files[ $key ]        = $val;
              $this->_files[ $key ]['ext'] = $this->_getExt( $val['name'] );

              if( $val['size'] > 0 && preg_match('/(php|phps|php5|php3|js|cgi|pl|css|htm|html|shtml|xhtml|dhtml|phtml)$/i', $this->_files[ $key ]['ext']) )
              {
                  $this->_bad_uploads[ $key ]            = $val;
                  $this->_bad_uploads[ $key ]['message'] = 'Prohibited file types';
                  $this->_bad_uploads[ $key ]['typeof']  = 'banned';

                  unset( $this->_files[ $key ] );
              }
              
              else if( $val['size'] > 0 && preg_match('/(gif|jpg|jpeg|png|bmp|tif)$/i', $this->_files[ $key ]['ext']) )
              {
                  $_image = $this->image_info( $val['tmp_name'] );

                  if( false === $_image ) {
                      $this->_bad_uploads[ $key ]            = $val;
                      $this->_bad_uploads[ $key ]['typeof']  = 'bad';
                      $this->_bad_uploads[ $key ]['message'] = 'Invalid image format';

                      unset( $this->_files[ $key ] );
                      
                  } else {
                      
                      if( $_image['x'] > $this->_max_image_width || $_image['y'] > $this->_max_image_height )
                      {
                          $this->_bad_uploads[ $key ]            = $val;
                          $this->_bad_uploads[ $key ]['typeof']  = 'bad';
                          $this->_bad_uploads[ $key ]['message'] = 'Exceeded the resolution of the image';

                          unset( $this->_files[ $key ] );
                      }
                      
                      else if ( $this->check_inject && !$this->_checkForInjection($val['tmp_name']) )
                      {
                          $this->_bad_uploads[ $key ]            = $val;
                          $this->_bad_uploads[ $key ]['typeof']  = 'bad';
                          $this->_bad_uploads[ $key ]['message'] = 'Image contains an injection';

                          unset($this->_files[$key]);
                      }
                      
                      else {
                          $this->_files[ $key ]['typeof'] = 'image';
                          $this->_files[ $key ]['width']  = $_image['x'];
                          $this->_files[ $key ]['height'] = $_image['y'];
                          
                          $this->_images_count++;
                      }
                  }
              }
              
              else if( $val['size'] > 0 && preg_match('/(mp3|wav|amr|amf|mid|midi|imf|imp|m1a|m4a|m2b|m4p|mmf|mod|mp1|mp2|m3u|mpa|oga|ogg|pcm|snd|voc|smf)$/i', $this->_files[ $key ]['ext']) )
              {
                  $this->_files[ $key ]['typeof'] = 'audio';
                  
                  $this->_audios_count++;
              }
              
              else if( $val['size'] > 0 && preg_match('/(avi|mpg|mpeg|mov|swf|flv|wmv|wma)$/i', $this->_files[ $key ]['ext']))
              {
                  $this->_files[ $key ]['typeof'] = 'media';
                  
                  $this->_medias_count++;
              }
              
              else
              {
                  $this->_files[ $key ]['typeof'] = 'file';
                  
                  $this->_files_count++;
              }
          }
      }
// -------------------------------------------------------------------------------------
      public function set_max_image_resolution($width, $height)
      {
          if( empty($width) || !is_numeric($width) || empty($height) || !is_numeric($height) ) {
              return $this;
          }
          
          $this->_max_image_width  = $width;
          $this->_max_image_height = $height;
          
          return $this;
      }
// -------------------------------------------------------------------------------------
      public function set_max_upload_size($size)
      {
          if( empty($size) || !is_numeric($size) ) {
              return $this;
          }
          
          $this->_max_file_size = $size;
          
          return $this;
      }
// -------------------------------------------------------------------------------------
      public function set_destination_path($_path)
      {
          if( is_dir($_path) ) {
              $this->_destination_dir = $_path;
          }
          
          return $this;
      }
// -------------------------------------------------------------------------------------
      public function upload_all($type = '')
      {
          if( empty($this->_files) ) {
              return $this;
          }
          
          foreach($this->_files as $key=>$val) {
              if($val['typeof'] == $type) {
                  $this->upload($key);
              }
          }
          
          return $this;
      }
// -------------------------------------------------------------------------------------
      public function upload($_name) 
      {
          if( empty($this->_files) ) {
              return $this;
          }

          if( empty($this->_destination_dir) || !is_dir($this->_destination_dir) ) {
              throw new \SB\Exception\Uploads(
                  get_class($this) . '::upload - Wrong destination path for uploads'
              );
          } else {
          
              if( !empty($_name) && !empty($this->_files[ $_name ]) && is_array($this->_files[ $_name ]) )
              {
                  if( move_uploaded_file($this->_files[ $_name ]['tmp_name'], $this->_destination_dir . DIR_SEP . $this->_files[ $_name ]['name']) ) 
                  {
                      $this->_result_uploads[ $_name ]['name']    = $this->_files[ $_name ]['name'];
                      $this->_result_uploads[ $_name ]['size']    = $this->_files[ $_name ]['size'];
                      $this->_result_uploads[ $_name ]['message'] = 'Successfully';
                      $this->_result_uploads[ $_name ]['path']    = $this->_destination_dir . DIR_SEP . $this->_files[ $_name ]['name'];
                      $this->_result_uploads[ $_name ]['type']    = $this->_files[ $_name] ['typeof'];

                      $this->_files[ $_name ] = null;
                      
                      unset( $this->_files[ $_name ] );
                      
                  } else {
                      $this->_bad_uploads[ $_name ]            = $this->_files[ $_name ];
                      $this->_bad_uploads[ $_name ]['message'] = 'Uploading file failed';

                      $this->_files[ $_name ] = null;
                      
                      unset($this->_files[ $_name]);
                  }
              }
          }
          
          return $this;
      }
// -------------------------------------------------------------------------------------
      public function exists_bads()
      {
          if( !empty($this->_bad_uploads) ) {
              return true;
          }
          
          return false;
      }
// -------------------------------------------------------------------------------------
      public function get_bad_uploads()
      {
          if( !empty($this->_bad_uploads) ) {
              return $this->_bad_uploads;
          }
          
          return false;
      }
// -------------------------------------------------------------------------------------
      public function get_result()
      {
          if( !empty($this->_result_uploads) ) {
              return $this->_result_uploads;
          }
          
          return false;
      }
// -------------------------------------------------------------------------------------
      public function get_images_count()
      {
          return $this->_images_count;
      }
// -------------------------------------------------------------------------------------
      public function get_media_count()
      {
          return $this->_medias_count;
      }
// -------------------------------------------------------------------------------------
      public function get_audio_count()
      {
          return $this->_audios_count;
      }
// -------------------------------------------------------------------------------------
      public function get_files_count()
      {
          return $this->_files_count;
      }
// -------------------------------------------------------------------------------------
      public function get_file_data($name)
      {
          if( !empty($this->_files[ $name ]) ) {
              return $this->_files[ $name ];
          }
          
          return false;
      }
// -------------------------------------------------------------------------------------
      protected function _getExt($filename)
      {
          return preg_replace('/(.*?)\.+([a-z0-9]{3,4})$/is', '$2', $filename);
      }
// -------------------------------------------------------------------------------------
      protected function _checkFileName($filename)
      {
          if( empty($filename) || !preg_match('/^[a-z0-9\_\.\-]+$/i', $filename) ) {
              return false;
          }
          
          return true;
      }
// -------------------------------------------------------------------------------------
      protected function _checkSize($size)
      {
          if( empty($size) ) {
              return false;
          }
          
          $_size_M = round( ($size/1024) / 1024 );
          
          if( $this->_max_file_size >= $_size_M ) {
              return true;
          }
          
          return false;
      }
// -------------------------------------------------------------------------------------
      protected function _checkForInjection($_file) 
      {
          if( empty($_file) || !is_file($_file) ) {
              return false;
          }

          $_tmp = file_get_contents($_file);
          
          if( empty($_tmp) ) {
              return false;
          }
          
          if( preg_match('/(\<\?php|\<\?\=)+/is', $_tmp) ) {
              return false;
          }
          
          return true;
      }
// -------------------------------------------------------------------------------------
  	  public function image_info($fImage)
	  {
	      if( is_file($fImage) && function_exists('getimagesize') ) {
	          $ret =  getimagesize($fImage);
            
              if( empty($ret[0]) && empty($ret[1]) ) {
                  return false;
              }
          
              return array(
                  'x'    => $ret[0],
                  'y'    => $ret[1],
                  'type' => $ret['mime'],
                  'html' => $ret[3],
              );
	      } else { 
	          return false;
	      }
	  }
// -------------------------------------------------------------------------------------
      public static function Instance()
      {
         if( null === self::$_instance ) {
              self::$_instance = new self();
         }
         
         return self::$_instance;
      }
  }
}