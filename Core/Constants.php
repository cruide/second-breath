<?php
/**
* @author       Tishchenko Alexander
* @copyright    Copyright (c) 2014 All rights to Tishchenko A.
* @package		Second-Breath PHP5 framework
* @filesource	Constants.php
*/

if( !defined('__CONSTANTS__') ) { 
  define('__CONSTANTS__', 'Constants.php');

// -------------------------------------------------------------------------------------
  define('DIR_SEP', DIRECTORY_SEPARATOR);
// -------------------------------------------------------------------------------------
  $_REQUEST_URI = preg_replace( '/^\/\?/', '/', $_SERVER['REQUEST_URI']);
  $_REQUEST_URI = preg_replace( '/^\/+/', '/', $_REQUEST_URI);
// -------------------------------------------------------------------------------------
  define('CONTENT_DIR'         , ROOT . DIR_SEP . 'Content');
  define('INTERFACE_DIR'       , ROOT . DIR_SEP . 'Interface');
  define('CONTENT_URL'         , BASE_URL . '/Content');
  define('INTERFACE_URL'       , BASE_URL . '/Interface');
  define('INTERFACE_COMMON_URL', BASE_URL . '/Interface/Common');
  define('PLUGINS_URL'         , BASE_URL . '/Application/Plugins');
  define('MODULES_URL'         , BASE_URL . '/Application/Modules');
// -------------------------------------------------------------------------------------
  define('CORE_NAME'         , 'Second-Breath');
  define('CORE_VERSION'      , '1.7.1');
  define('CORE_VERSION_NAME' , 'beta');
  define('FRAMEWORK'         , CORE_NAME . ' PHP5 framework v' . CORE_VERSION);
// -------------------------------------------------------------------------------------
  define('APPLICATION_DIR'   , ROOT . DIR_SEP . 'Application');
  
  if( !defined('TEMP_DIR') ) {
      define('TEMP_DIR', ROOT . DIR_SEP . 'Temporary');
  }
  
  define('LOGS_DIR'          , APPLICATION_DIR . DIR_SEP . 'Logs');
  define('LIBRARY_DIR'       , APPLICATION_DIR . DIR_SEP . 'Library');
  define('SETTINGS_DIR'      , APPLICATION_DIR . DIR_SEP . 'Settings');
  define('MODULES_DIR'       , APPLICATION_DIR . DIR_SEP . 'Modules');
  define('PLUGINS_DIR'       , APPLICATION_DIR . DIR_SEP . 'Plugins');
  define('SERVICES_DIR'      , APPLICATION_DIR . DIR_SEP . 'Services');
  define('LANGUAGES_DIR'     , APPLICATION_DIR . DIR_SEP . 'I18n');
// -------------------------------------------------------------------------------------
  define('DEFAULT_MODULE'    , 'Index');
  define('DEFAULT_CONTROLLER', 'Index');

  /**
  * (ACCESS_VIEW | ACCESS_EDIT) = 5
  * 
  */
  if( !defined('ACCESS_DELETE') ) {
      define('ACCESS_VIEW'  , 1 << 0); // 1
      define('ACCESS_CREATE', 1 << 1); // 2
      define('ACCESS_EDIT'  , 1 << 2); // 4
      define('ACCESS_DELETE', 1 << 3); // 8
      define('ACCESS_ALL'   , ACCESS_VIEW | ACCESS_CREATE | ACCESS_EDIT | ACCESS_DELETE); // 15
  }
}
